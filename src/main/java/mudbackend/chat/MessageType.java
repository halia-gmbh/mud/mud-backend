package mudbackend.chat;

public enum MessageType {
    ChatWhisper,
    ChatTalk,
    DmToPlayer,
    PlayerToDm,
    DmTalk,
    DmShout,
    Admin,
    CubeEvent
}
