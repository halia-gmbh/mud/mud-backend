package mudbackend.chat;


import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.services.DungeonMasterService;
import mudbackend.services.DungeonPlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;


@Component
public class MessageListener {

    private final SimpMessagingTemplate template;
    private final DungeonMasterService dungeonMasterService;
    private final DungeonPlayerService dungeonPlayerService;

    @Autowired
    public MessageListener(SimpMessagingTemplate template, DungeonMasterService dungeonMasterService, DungeonPlayerService dungeonPlayerService) {
        this.template = template;
        this.dungeonMasterService = dungeonMasterService;
        this.dungeonPlayerService = dungeonPlayerService;
    }

    /**
     * Listens to messages to the kafka cluster and forwards them to the users websocket(s).
     *
     * @param message message object that will be forwarded
     */
    @KafkaListener(
            topicPattern = KafkaConstants.KAFKA_TOPIC,
            containerFactory = "kafkaListenerContainerFactory"
    )
    public void listen(Message message) {
        String messageString;
        long senderID;
        long receiverID;
        switch (message.getType()) {
            case ChatWhisper:
                receiverID = getID(message, "receiver");
                if (receiverID == -1) {
                    sendErrorMessage(message);
                }
                messageString = String.format("%s fluestert dir: %s", message.getSenderName(), message.getContent());
                template.convertAndSend(String.format("/topic/player/%s", receiverID), messageString);
                break;
            case ChatTalk:
                List<PlayerCharacter> characters = dungeonPlayerService.getCharactersInRoom(message.getGameID(), message.getRoomID());
                for (PlayerCharacter character : characters) {
                    if (!character.getName().equals(message.getSenderName())) {
                        messageString = String.format("%s sagt: %s", message.getSenderName(), message.getContent());
                        template.convertAndSend(String.format("/topic/player/%s", character.getId()), messageString);
                    }
                }
                break;
            case DmToPlayer:
                receiverID = getID(message, "receiver");
                template.convertAndSend(String.format("/topic/dm/%s", receiverID), message.getContent());
                break;
            case PlayerToDm:
                int receiverId = dungeonMasterService.getActiveGames().get(message.getGameID()).getDungeonMaster();
                messageString = String.format("%s schreibt: %s", message.getSenderName(), message.getContent());
                template.convertAndSend(String.format("/topic/dm/%s", receiverId), messageString);
                break;
            case DmTalk:
                List<PlayerCharacter> chars = dungeonPlayerService.getCharactersInRoom(message.getGameID(), message.getRoomID());
                for (PlayerCharacter character : chars) {
                    template.convertAndSend(String.format("/topic/dm/%s", character.getId()), message.getContent());
                }
                break;
            case DmShout:
                List<PlayerCharacter> charas = dungeonMasterService.getActiveGames().get(message.getGameID()).getDungeon().getActiveCharacters();
                for (PlayerCharacter character : charas) {
                    receiverID = character.getId();
                    template.convertAndSend(String.format("/topic/dm/%s", receiverID), message.getContent());
                }
                break;
            case Admin:
                messageString = String.format("Admin-Nachricht: %s", message.getContent());
                template.convertAndSend("/topic/admin/", messageString);
                break;
            case CubeEvent:
                senderID = getID(message, "sender");
                receiverID = dungeonMasterService.getActiveGames().get(message.getGameID()).getDungeonMaster();

                String randomNum = Integer.toString(new Random().nextInt(100) + 1);
                String messageStringUser = String.format("Du hast eine %s gewürfelt.", randomNum);
                String messageStringDm = String.format("%s hat eine %s gewürfelt.", message.getSenderName(), randomNum);
                template.convertAndSend(String.format("/topic/cube/%s", senderID), messageStringUser);
                template.convertAndSend(String.format("/topic/cube/%s", receiverID), messageStringDm);
                break;
        }
    }

    /**
     * Sends error message back to sender if receiver wasn't reached.
     *
     * @param message message that was intended to be send
     */
    private void sendErrorMessage(Message message) {
        long senderID = getID(message, "sender");
        String errorMessage = "Could not send message, please check if username actually exists.";
        template.convertAndSend(String.format("topic/player/%s", senderID), errorMessage);
    }

    /**
     * Retrieves the account id in various ways.
     *
     * @param message Always needed.
     * @param type    Option 1: get id by name ("sender", "receiver")
     * @return account id
     */
    private long getID(Message message, String type) {
        List<PlayerCharacter> characters = dungeonMasterService.getActiveGames().get(message.getGameID()).getDungeon().getActiveCharacters();
        for (PlayerCharacter character : characters) {
            if (type.equals("receiver")) {
                if (character.getName().equals(message.getReceiverName())) {
                    return character.getId();
                }
            } else if (type.equals("sender")) {
                if (character.getName().equals(message.getSenderName())) {
                    return character.getId();
                }
            }
        }
        return -1;
    }
}
