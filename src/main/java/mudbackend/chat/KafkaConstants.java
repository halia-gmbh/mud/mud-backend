package mudbackend.chat;

public class KafkaConstants {
    public static final String KAFKA_TOPIC = "haliachat";
    public static final String ROOM_TOPIC = "RoomChange";
    public static final String GROUP_ID = "kafka-backend";
    public static final String KAFKA_BROKER = "127.0.0.1:9092";
}
