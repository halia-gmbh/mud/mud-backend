package mudbackend.chat;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Message {
    private String senderName;
    private String receiverName;
    private String content;
    private MessageType type;
    private long roomID;
    private long gameID;
}
