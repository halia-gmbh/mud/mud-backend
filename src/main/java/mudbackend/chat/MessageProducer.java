package mudbackend.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageProducer {

    private final KafkaTemplate<String, Message> kafkaTemplate;

    @Autowired
    public MessageProducer(KafkaTemplate<String, Message> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @CrossOrigin
    @PostMapping(value = "/api/send", consumes = "application/json", produces = "application/json")
    public void whisper(@RequestBody Message message) {
        kafkaTemplate.send(KafkaConstants.KAFKA_TOPIC, message);
    }

}

