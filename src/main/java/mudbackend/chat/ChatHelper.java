package mudbackend.chat;

import mudbackend.services.DungeonMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChatHelper {

    public static DungeonMasterService dungeonMasterService;


    @Autowired
    public void setDungeonMasterService(DungeonMasterService masterService) {
        ChatHelper.dungeonMasterService = masterService;
    }

}
