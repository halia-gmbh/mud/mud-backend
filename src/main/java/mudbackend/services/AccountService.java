package mudbackend.services;

import lombok.Getter;
import mudbackend.accountservice.Account;
import mudbackend.accountservice.TokenHelper;
import mudbackend.database.MongoHelper;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.dungeondesign.Room;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Getter
public class AccountService {

    private final List<Account> loggedIn;

    @Autowired
    private MongoHelper mongoHelper;
    private final DungeonMasterService dungeonMasterService;
    private final TokenHelper tokenHelper;

    @Autowired
    public AccountService(TokenHelper tokenHelper, MongoHelper mongoHelper, DungeonMasterService dungeonMasterService) {
        loggedIn = new ArrayList<>();
        this.tokenHelper = tokenHelper;
        this.mongoHelper = mongoHelper;
        this.dungeonMasterService = dungeonMasterService;
    }

    /**
     * Tries to log in the given user to the server.
     *
     * @param email    the email address, with which the user has signed up
     * @param password the (unencrypted) password, with which the user has signed up
     * @return the AccountID, if login is successful; -1 if not because of any reason
     */
    public Account login(String email, String password) {
        // check if account is registered and if password is right
        Account toFind;
        HashMap<String, Object> query = new HashMap<>();
        query.put("email", email);
        List<Account> result = mongoHelper.getFromDB(Account.class, query);
        if (result == null || result.size() == 0) return null;
        else toFind = result.get(0);
        String hashed = DigestUtils.md5Hex(password);
        if (hashed.equals(toFind.getPassword()) && toFind.isUnlocked()) {
            loggedIn.add(toFind);
            setTokenForAccount(toFind);
            return toFind;
        }
        return null;
    }

    /**
     * Tries to log out the given user off the server. If user was still in game all his data will be removed from the
     * process.
     *
     * @return true if logout is successful; otherwise false
     */
    public boolean logout(@PathVariable int accountID) {
        List<Account> users = this.getLoggedIn();
        for (Account user : users) {
            if (user.getId() == accountID) {
                for (long gameID : user.getJoinedGames()) {
                    if (user.getDungeonsMaster() != null && user.getDungeonsMaster().contains(gameID)) {
                        dungeonMasterService.endGame(gameID);
                    } else {
                        try {
                            List<PlayerCharacter> characters = dungeonMasterService.getActiveGames().get(gameID).getCharactersOfPlayer(accountID);
                            for (PlayerCharacter character : characters) {
                                if (character.isActive()) {
                                    character.setActive(false);
                                    Room room = dungeonMasterService.getActiveGames().get(gameID).getDungeon().getRoomWithPlayer(character.getId());
                                    room.getPresentCharacters().remove(character.getId());
                                    dungeonMasterService.getActiveGames().get(gameID).getDungeon().getActiveCharacters().remove(character);
                                }
                            }
                        }
                        catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }
                user.setJoinedGames(null);
                user.setToken(null);
                return this.loggedIn.remove(user);
            }
        }
        return false;
    }

    /**
     * Sets token for an account.
     *
     * @param account account that will receive a token
     */
    private void setTokenForAccount(Account account) {
        account.setToken(tokenHelper.generateToken(new User(account.getEmail(), account.getPassword(), new ArrayList<>())));
    }

    /**
     * Returns the number of logged in users.
     *
     * @return number of logged in users
     */
    public int getLoggedInNumber() {
        return loggedIn.size();
    }

    /**
     * Sets the mongo helper
     *
     * @param helper mongo helper instance
     */
    public void setMongoHelper(MongoHelper helper) {
        this.mongoHelper = helper;
    }

    /**
     * Tries to sign up a user with the given credentials
     *
     * @param email    the email address, with which the user wants to sign up
     * @param password the (unencrypted) password, with which the user wants to login
     * @return true, if registration is successful; false, if not because of any reason
     */
    public boolean register(String email, String password) {
        HashMap<String, Object> query = new HashMap<>();
        query.put("email", email);
        List<Account> result = mongoHelper.getFromDB(Account.class, query);
        if (result.size() > 0) return false;
        String hashedPassword = DigestUtils.md5Hex(password);
        Account acc = new Account(email, hashedPassword);
        acc.sendConfirmationMail();
        return mongoHelper.saveInDB(acc);
    }

    /**
     * Resets the password to the given accounts and sends the new one per mail
     *
     * @param email the account email address
     * @return true, if the reset succeeded; false, if not
     */
    public boolean resetPassword(String email) {
        HashMap<String, Object> query = new HashMap<>();
        query.put("email", email);
        List<Account> result = mongoHelper.getFromDB(Account.class, query);
        if (result == null || result.size() == 0) return false;
        boolean res = result.get(0).resetPassword();
        mongoHelper.saveInDB(result.get(0));
        return res;
    }

    /**
     * Confirms a newly created account with a token sent by mail
     *
     * @param token the token the user entered
     * @return true, if a corresponding account was found and confirmed; false, else
     */
    public boolean confirmAccount(String token) {
        HashMap<String, Object> query = new HashMap<>();
        query.put("confirmToken", token);
        List<Account> result = mongoHelper.getFromDB(Account.class, query);
        if (result == null || result.size() != 1) return false;
        result.get(0).confirmAccount();
        mongoHelper.saveInDB(result.get(0));
        return true;
    }

    /**
     * Retrieves UserDetails for a certain user.
     *
     * @param username username
     * @return UserDetails
     */
    public UserDetails getUserDetailsByUsername(String username) {
        HashMap<String, Object> query = new HashMap<>();
        query.put("email", username);
        List<Account> result = mongoHelper.getFromDB(Account.class, query);
        if (result != null && result.size() == 1) {
            return new User(result.get(0).getEmail(), result.get(0).getPassword(), new ArrayList<>());
        }
        return null;
    }

    public void addDungeonToJoined(long userID, long gameID) {
        for (Account account : loggedIn) {
            if (account.getId() == userID) {
                account.joinGame(gameID);
                mongoHelper.saveInDB(account);
            }
        }
    }
}