package mudbackend.services;

import lombok.Getter;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.dungeondesign.DirectionEnum;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.events.RoomEvent;
import mudbackend.entity.general.Dungeon;
import mudbackend.entity.general.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Getter
public class DungeonPlayerService {

    private final DungeonMasterService dungeonMasterService;
    private Map<String, Integer> watchedCharacters;

    private AutomaticPlayerLeaveService playerLeaveService;
    private Thread thread;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    public DungeonPlayerService(DungeonMasterService dungeonMasterService) {
        this.dungeonMasterService = dungeonMasterService;
        this.watchedCharacters = new HashMap<>();
        playerLeaveService = new AutomaticPlayerLeaveService(this);
        thread = new Thread(playerLeaveService);
        thread.start();
    }

    /**
     * Uses an item: Either consumes it, or equips it.
     *
     * @param characterID identifier of the player character
     * @param gameID      identifier of the game
     * @param itemID      identifier of the item
     * @return the player character with a modified inventory
     */
    public PlayerCharacter useItem(long characterID, long gameID, long itemID) {
        PlayerCharacter character =
                this.dungeonMasterService.getGameFromID(gameID).getDungeon().getPlayerCharacterByID(characterID);
        character.useItem(itemID);
        sendCharacterChange(gameID, character);
        return character;
    }

    public PlayerCharacter unequipItem(long characterID, long gameID, long itemID) {
        PlayerCharacter character =
                this.dungeonMasterService.getGameFromID(gameID).getDungeon().getPlayerCharacterByID(characterID);
        character.unequipItem(itemID);
        sendCharacterChange(gameID, character);
        return character;
    }

    /**
     * Removes an item from a room and adds it to a player's inventory.
     *
     * @param characterID identifier of the player character
     * @param gameID      identifier of the game
     * @param itemName    name of the item
     * @return the player character with a modified inventory
     */
    public PlayerCharacter pickUpItem(long characterID, long gameID, String itemName) {
        Game game = this.dungeonMasterService.getGameFromID(gameID);
        PlayerCharacter character = game.getDungeon().getPlayerCharacterByID(characterID);
        game.getDungeon().pickUpItem(characterID, itemName);

        sendCharacterChange(gameID, character);
        sendRoomChange(gameID, character.getLocation(), character.getLocation() );
        return character;
    }

    private void sendCharacterChange(long gameID, PlayerCharacter character) {
        template.convertAndSend("/topic/game/" + gameID + "/character/" + character.getId(), character);
    }

    /**
     * Removes an item from a player's inventory and adds it to a room.
     *
     * @param characterID identifier of the player character
     * @param gameID      identifier of the game
     * @param itemID      identifier of the item
     * @return the player character with a modified inventory
     */
    public PlayerCharacter discardItem(long characterID, long gameID, long itemID) {
        Game game = this.dungeonMasterService.getGameFromID(gameID);
        PlayerCharacter character = game.getDungeon().getPlayerCharacterByID(characterID);
        game.getDungeon().discardItem(characterID, itemID);
        sendCharacterChange(gameID, character);
        sendRoomChange(gameID, character.getLocation(), character.getLocation() );
        return character;
    }

    /**
     * Interacts with a given NPC
     *
     * @param gameID identifier of the game
     * @param npcID  identifier of the NPC
     * @param action action done towards the NPC
     * @return String containing the reaction of the NPC
     */
    public String interactWithNPC(long gameID, long npcID, String action) {
        return this.dungeonMasterService.getGameFromID(gameID).getDungeon().getNPCbyID(npcID).getReaction(action);
    }

    /**
     * Fetches a room event identified by a given ID.
     *
     * @param gameID      identifier of the game
     * @param roomID      identifier of the room the room event lies within
     * @param roomEventID identifier of the room event
     * @return the room event previously identified
     */
    public RoomEvent getRoomEvent(long gameID, long roomID, long roomEventID) {
        return this.dungeonMasterService.getGameFromID(gameID).getDungeon().getRoomByID(roomID).getRoomEvents().stream()
                .filter(roomEvent -> roomEvent.getId() == roomEventID).findFirst().orElse(null);
    }

    private void sendRoomChange(long gameID, Room oldRoom, Room room) {
        template.convertAndSend("/topic/game/" + gameID + "/room/" + oldRoom.getId(), oldRoom);
        template.convertAndSend("/topic/game/" + gameID + "/room/" + room.getId(), room);
    }

    /**
     * Moves a player character from one room to another following a path in the specified direction.
     *
     * @param characterID identifier of the character that will be moved
     * @param gameID      identifier of the game
     * @param direction   direction of the pathway
     * @return the room the character has been moved to
     */
    public Room move(long characterID, long gameID, DirectionEnum direction) {
        Dungeon dungeon = dungeonMasterService.getGameFromID(gameID).getDungeon();
        long currentRoomID = dungeonMasterService.getGameFromID(gameID).getDungeon().getRoomWithPlayer(characterID).getId();
        Room newRoom = dungeon.moveCharacter(characterID, direction);
        Room oldRoom = dungeon.getRoomByID(currentRoomID);
        sendRoomChange(gameID, oldRoom, newRoom);
        return newRoom;
    }

    /**
     * Function fetching all the player characters in a given room.
     *
     * @param gameID identifier of the game that contains the room
     * @param roomID identifier of the room containing the player characters
     * @return List of player characters currently residing in that room
     */
    public List<PlayerCharacter> getCharactersInRoom(long gameID, long roomID) {
        List<PlayerCharacter> returnList = new ArrayList<>();
        Dungeon dungeon = dungeonMasterService.getGameFromID(gameID).getDungeon();
        for (PlayerCharacter character : dungeon.getActiveCharacters()) {
            if (character.getLocation().getId() == roomID)
                returnList.add(character);
        }
        return returnList;
    }

    /**
     * Runs interaction with a Roomevent.
     *
     * @param gameID      gameID
     * @param characterID characterID
     * @param eventName   name of the event
     * @return Response-String if TextEvent, else null
     */
    public RoomEvent interactRoomevent(long gameID, long characterID, String eventName) {
        Dungeon dungeon = dungeonMasterService.getGameFromID(gameID).getDungeon();
        RoomEvent roomEvent =  dungeon.interactRoomevent(characterID, eventName);
        Room room = dungeon.getPlayerCharacterByID(characterID).getLocation();
        sendRoomChange(gameID, room, room);
        return roomEvent;
    }

    /**
     * Removes a specified character from the active characters attribute within the dungeon class.
     *
     * @param gameID      identifier of the game containing the character
     * @param characterID identifier of the character leaving
     * @return true if the removal was successful
     */
    public boolean quitGame(long gameID, long characterID) {
        Dungeon dungeon = dungeonMasterService.getGameFromID(gameID).getDungeon();
        PlayerCharacter playerCharacter = dungeon.getPlayerCharacterByID(characterID);
        boolean success = dungeon.getActiveCharacters().remove(playerCharacter);
        if (success) {
            notifyQuit(gameID, characterID);
            return true;
        }
        return false;
    }

    private void notifyQuit(long gameID, long characterID) {
        template.convertAndSend("/topic/game/" + gameID + "/delChar/", characterID);
    }

    public void resetDeathWatch(Long gameID, Long characterID) {
        String id = gameID.toString() + "_" + characterID.toString();
        watchedCharacters.put(id,0);
        if (!thread.isAlive()) {
            this.playerLeaveService = new AutomaticPlayerLeaveService(this);
            this.thread = new Thread(playerLeaveService);
            thread.start();
        }
    }

    public void setDeathWatch(long gameID, long characterID) {
       /* watchedCharacters.put(characterID, false);
        System.out.println("Death Watch Char");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!getFromWatchedGames(characterID))
            quitGame(gameID, characterID);
        removeWatchedGame(characterID); */
    }


}
