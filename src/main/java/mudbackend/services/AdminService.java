package mudbackend.services;

import lombok.Getter;
import mudbackend.accountservice.Account;
import mudbackend.accountservice.Admin;
import mudbackend.accountservice.AuthenticationHelper;
import mudbackend.accountservice.TokenHelper;
import mudbackend.database.MongoHelper;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Getter
public class AdminService {

    private final AuthenticationHelper authenticationHelper;
    private final MongoHelper mongoHelper;
    private final AccountService accountService;
    private final TokenHelper tokenHelper;

    private final List<Admin> loggedIn;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    public AdminService(TokenHelper tokenHelper, AuthenticationHelper authenticationHelper, MongoHelper mongoHelper, AccountService accountService) {
        loggedIn = new ArrayList<>();
        this.tokenHelper = tokenHelper;
        this.authenticationHelper = authenticationHelper;
        this.accountService = accountService;
        this.mongoHelper = mongoHelper;
    }

    /**
     * Tries to log in the given user to the server.
     *
     * @param email    the email address, with which the user has signed up
     * @param password the (unencrypted) password, with which the user has signed up
     * @return the AccountID, if login is successful; -1 if not because of any reason
     */
    public Admin adminLogin(String email, String password) {
        Admin toFind;
        HashMap<String, Object> query = new HashMap<>();
        query.put("email", email);
        List<Admin> result = mongoHelper.getFromDB(Admin.class, query);
        if (result == null || result.size() == 0) return null;
        else toFind = result.get(0);
        String hashed = DigestUtils.md5Hex(password);
        if (hashed.equals(toFind.getPassword())) {
            loggedIn.add(toFind);
            setTokenForAdmin(toFind);
            return toFind;
        }
        return null;
    }

    /**
     * Logs the given user out of the server.
     *
     * @return true if logout was successful, otherwise false
     */
    public boolean adminLogut(int adminId) {
        List<Admin> users = this.getLoggedIn();
        for (Admin user : users) {
            if (user.getId() == adminId) {
                user.setToken(null);
                return this.loggedIn.remove(user);
            }
        }
        return false;
    }

    /**
     * Retrieves all accounts that are registered to the server.
     *
     * @return list of accounts
     */
    public List<Account> getAllAccounts() {
        return mongoHelper.getFromDB(Account.class, null);
    }

    /**
     * Retrieves all accounts that are currently active on the server.
     *
     * @return list of accounts
     */
    public List<Account> getAllPlayingAccounts() {
        return accountService.getLoggedIn();
    }

    /**
     * Verifies an account so that the user can use the login.
     *
     * @param accountID id of the account that will be verified
     * @return true if verification was successful, otherwise false
     */
    public boolean verify(long accountID) {
        HashMap<String, Object> query = new HashMap<>();
        query.put("id", accountID);
        Account acc = mongoHelper.getFromDB(Account.class, query).get(0);
        boolean result = acc.confirmAccount();
        mongoHelper.saveInDB(acc);
        return result;
    }

    /**
     * Deletes an account from the server.
     *
     * @param accountID id of the account that will be deleted
     * @return true of deletion was successful, otherwise false
     */
    public boolean delete(int accountID) {
        accountService.logout(accountID);
        Map<String, Object> query = new HashMap<>();
        query.put("id", accountID);
        int errorCode = mongoHelper.deleteFromDB(Account.class, query);
        return errorCode != -1;
    }

    private void setTokenForAdmin(Admin admin) {
        admin.setToken(tokenHelper.generateToken(new User(admin.getEmail(), admin.getPassword(), new ArrayList<>())));
    }

    /**
     * Changes the password of an account.
     *
     * @param accountID id of the account
     * @param password  password that will be set
     * @return true if change was successful, otherwise false
     */
    public boolean changePassword(Integer accountID, String password) {
        Account acc = Objects.requireNonNull(accountService.getLoggedIn().stream().filter(item -> item.getId() == accountID).findFirst().orElse(null));
        acc.setPassword(DigestUtils.md5Hex(password));

        return mongoHelper.saveInDB(acc);
    }

    /**
     * Changes the email of an account.
     *
     * @param accountID id of the account
     * @param email     email that will be set
     * @return true if change was successful, otherwise false
     */
    public boolean changeEmail(Integer accountID, String email) {
        Account acc = Objects.requireNonNull(accountService.getLoggedIn().stream().filter(item -> item.getId() == accountID).findFirst().orElse(null));
        acc.setEmail(email);

        return mongoHelper.saveInDB(acc);
    }

    public boolean adminMessage(String msg) {
        template.convertAndSend("/topic/admin/", msg);
        return true;
    }
}
