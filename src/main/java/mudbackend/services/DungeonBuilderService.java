package mudbackend.services;

import lombok.Getter;
import mudbackend.database.MongoHelper;
import mudbackend.entity.characterdesign.CharacterClass;
import mudbackend.entity.characterdesign.CharacterRace;
import mudbackend.entity.general.DungeonStatusEnum;
import mudbackend.entity.general.Game;
import mudbackend.entity.general.MatrixInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Service
public class DungeonBuilderService {
    private final AccountService accountService;
    private final DungeonBuilderItemService itemService;
    private final DungeonBuilderRoomService roomService;
    private final DungeonBuilderRoomEventService roomEventService;
    private final DungeonBuilderNPCService npcService;
    private final MongoHelper mongoHelper;
    private final DungeonMasterService dungeonMasterService;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    public DungeonBuilderService(DungeonMasterService dungeonMasterService, MongoHelper mongoHelper, AccountService accountService, DungeonBuilderItemService dungeonBuilderItemService, DungeonBuilderRoomService dungeonBuilderRoomService, DungeonBuilderRoomEventService dungeonBuilderRoomEventService, DungeonBuilderNPCService dungeonBuilderNPCService) {
        this.accountService = accountService;
        this.itemService = dungeonBuilderItemService;
        this.roomService = dungeonBuilderRoomService;
        this.roomEventService = dungeonBuilderRoomEventService;
        this.npcService = dungeonBuilderNPCService;
        this.mongoHelper = mongoHelper;
        this.dungeonMasterService = dungeonMasterService;
    }

    private final Map<Long, Game> dungeonsWIP = new HashMap<>();

    /**
     * Creates a new Game and adds id to dungeonsWIP
     *
     * @param name       name of the Game
     * @param maxPlayers max Amount of Players
     * @param builderID  userID of the builder
     * @return the game
     */
    public Game createGame(String name, int maxPlayers, int builderID) {
        Game game = new Game(maxPlayers, name, builderID);
        game.setDungeonMaster(builderID);
        dungeonsWIP.put(game.getId(), game);
        return game;
    }

    /**
     * Returns a Dungeonmatrix
     *
     * @param gameID gameID
     * @return the matrix
     */
    public MatrixInformation[][] getDungeonMatrix(long gameID) {
        Game game = dungeonsWIP.get(gameID);
        if (game == null)
            return dungeonMasterService.getGameFromID(gameID).getDungeon().generateMatrix();
        return dungeonsWIP.get(gameID).getDungeon().generateMatrix();
    }

    public long alterGame(long gameID) {
        Map<String, Object> query = new HashMap<>();
        query.put("id", gameID);
        List<Game> games = mongoHelper.getFromDB(Game.class, query);
        if (games == null || games.size() != 1) {
            return -1;
        }
        dungeonsWIP.put(games.get(0).getId(), games.get(0));
        return games.get(0).getId();
    }

    /**
     * Publishes the dungeon by setting its status to 'public'.
     *
     * @param game instance of a game
     * @return true if successful, otherwise false
     */
    public boolean publishDungeon(Game game) {
        dungeonsWIP.remove(game.getId());
        game.changeDungeonStatus(DungeonStatusEnum.PUBLIC);
        if (game.getDungeon().getRooms().isEmpty() || game.getClasses().isEmpty() || game.getRaces().isEmpty()) {
            return false;
        }
        notifyAboutNewOverview(game);
        return mongoHelper.saveInDB(game);
    }

    private void notifyAboutNewOverview(Game game) {
        for (long x : game.getPlayerCharacters().keySet()) {
            template.convertAndSend("/topic/game/" + game.getId() + "/overviewRefresh/" + x, "");
        }
    }

    public boolean saveDungeon(long gameID) {
        Game game = dungeonsWIP.get(gameID);
        dungeonsWIP.remove(gameID);
        return mongoHelper.saveInDB(game);
    }

    /**
     * Adds a class option to the dungeon.
     *
     * @param characterClass option that will be added
     * @param gameId         id of the game
     * @return true if addition was successful, otherwise false
     */
    public Game addCharacterClass(long gameId, CharacterClass characterClass) {
        dungeonsWIP.get(gameId).addClass(characterClass);
        return dungeonsWIP.get(gameId);
    }

    /**
     * Removes a class option from the dungeon.
     *
     * @param classId id of the option that will be removed
     * @param gameId  id of the game
     * @return true if deletion was successful, otherwise false
     */
    public boolean removeCharacterClass(long gameId, long classId) {
        return dungeonsWIP.get(gameId).removeClass(classId);
    }

    /**
     * Adds a race option to the dungeon.
     *
     * @param race   option that will be added
     * @param gameId id of the game
     * @return true if addition was successful, otherwise false
     */
    public Game addCharacterRace(long gameId, CharacterRace race) {
        dungeonsWIP.get(gameId).addRace(race);
        return dungeonsWIP.get(gameId);
    }

    /**
     * Removes a race option from the dungeon.
     *
     * @param raceId id of the option that will be removed
     * @param gameId id of the game
     * @return true if deletion was successful, otherwise false
     */
    public boolean removeCharacterRace(long gameId, long raceId) {
        return dungeonsWIP.get(gameId).removeRace(raceId);
    }


}
