package mudbackend.services;

public class AutomaticPlayerLeaveService implements Runnable{

    public DungeonPlayerService service;

    public AutomaticPlayerLeaveService(DungeonPlayerService playerService) {
        this.service = playerService;
    }
    @Override
    public void run() {
        while(true) {
            try{
                Thread.sleep(20000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            for(String x : service.getWatchedCharacters().keySet()) {
                Integer val = service.getWatchedCharacters().get(x);
                if(val > 0) {
                    String[] parts = x.split("_");
                    long gameID = Long.parseLong(parts[0]);
                    long characterID = Long.parseLong(parts[1]);
                    service.quitGame(gameID,characterID);
                    service.getWatchedCharacters().remove(x);
                }else{
                    service.getWatchedCharacters().put(x, val + 1);
                }
            }
        }
    }
}
