package mudbackend.services;

import mudbackend.database.MongoHelper;
import mudbackend.entity.general.Game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Helper class to save running games every five minutes.
 *
 * @author Dominik Sauerer
 */
public class AutomaticSaveService implements Runnable {

    private DungeonMasterService dungeonMasterService;
    ExecutorService executorService;
    MongoHelper mongoHelper;


    public AutomaticSaveService(DungeonMasterService service, MongoHelper mongoHelper) {
        this.dungeonMasterService = service;
        this.executorService = Executors.newFixedThreadPool(5);
        this.mongoHelper = mongoHelper;

    }


    @Override
    public void run() {
        while (dungeonMasterService.getActiveGames().size() != 0) {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Set<Long> toSave = new HashSet<>(dungeonMasterService.getActiveGames().keySet());
            List<Future<Boolean>> results = new ArrayList<>();
            for(long x : dungeonMasterService.getWatchedGames().keySet()) {
                Integer val = dungeonMasterService.getWatchedGames().get(x);
                if(val > 2) {
                    dungeonMasterService.killGame(x);
                }else{
                    dungeonMasterService.getWatchedGames().put(x, val + 1);
                }
            }
            for (long gameID : toSave) {
                Callable<Boolean> saveWork = () -> {
                    Game game = dungeonMasterService.getActiveGames().get(gameID);
                    if (game != null) {
                        return this.mongoHelper.saveInDB(game);
                    }
                    return false;
                };
                results.add(executorService.submit(saveWork));
            }
            for (Future<Boolean> res : results) {
                try {
                    res.get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
