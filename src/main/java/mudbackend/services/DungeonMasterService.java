package mudbackend.services;

import lombok.Getter;
import mudbackend.accountservice.Account;
import mudbackend.database.MongoHelper;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.general.Dungeon;
import mudbackend.entity.general.Game;
import mudbackend.entity.items.ItemForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Service
public class DungeonMasterService {
    private final Map<Long, Game> activeGames = new HashMap<>();
    private MongoHelper mongoHelper;
    private AutomaticSaveService automaticSaveService;
    private Thread automaticSaveThread;

    private Map<Long, Integer> watchedGames;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    public DungeonMasterService(MongoHelper mongoHelper) {
        this.mongoHelper = mongoHelper;
        this.automaticSaveService = new AutomaticSaveService(this, mongoHelper);
        this.automaticSaveThread = new Thread(automaticSaveService);
        watchedGames = new HashMap<>();
    }

    /**
     * Launches a game and corresponding dungeon.
     *
     * @param game game that will be launched
     * @return true if the launch was successful
     */
    public boolean startDungeon(Game game) {
        this.activeGames.put(game.getId(), game);
        if (!automaticSaveThread.isAlive()) {
            automaticSaveThread.start();
        }
        return true;
    }

    public void startSaveThread() {
        if (!automaticSaveThread.isAlive()) {
            this.automaticSaveService = new AutomaticSaveService(this, mongoHelper);
            this.automaticSaveThread = new Thread(automaticSaveService);
            automaticSaveThread.start();
        }
    }

    /**
     * Function used to fetch a game object from an ID.
     *
     * @param gameID identifier of the game
     * @return Game object corresponding to the ID
     */
    public Game getGameFromID(long gameID) {
        return this.activeGames.get(gameID);
    }

    private void sendCharacterChange(long gameID, PlayerCharacter character) {
        template.convertAndSend("/topic/game/" + gameID + "/character/" + character.getId(), character);
    }

    /**
     * Adds an item to a player's inventory.
     *
     * @param gameID      identifier of the game
     * @param characterID identifier of the character whose inventory will be modified
     * @param itemForm    item that will be added
     * @return the modified character
     */
    public PlayerCharacter givePlayerItem(long gameID, long characterID, ItemForm itemForm) {
        PlayerCharacter character = getGameFromID(gameID).getDungeon().getPlayerCharacterByID(characterID);
        character.addItem(itemForm.toItem());
        sendCharacterChange(gameID, character);
        return character;
    }

    /**
     * Removes an item from a player's inventory.
     *
     * @param gameID      identifier of the game
     * @param characterID identifier of the character whose inventory will be modified
     * @param itemID      identifier of the item that will be removed
     * @return the modified character
     */
    public PlayerCharacter deletePlayerItem(long gameID, long characterID, long itemID) {
        PlayerCharacter character = getGameFromID(gameID).getDungeon().getPlayerCharacterByID(characterID);
        character.deleteItem(itemID);
        sendCharacterChange(gameID, character);
        return character;
    }

    /**
     * Sets an attribute of a player to a specific value
     *
     * @param gameID      gameID
     * @param characterID characterID
     * @param index       index of the changed Attribute. -1 if new Attribute
     * @param attribute   attriuteName
     * @param value       new value
     * @return the Character
     */
    public PlayerCharacter setPlayerAttribute(long gameID, long characterID, int index, String attribute, int value) {
        PlayerCharacter character = getGameFromID(gameID).getDungeon().getPlayerCharacterByID(characterID);
        character.setPlayerAttribute(index, attribute, value);
        sendCharacterChange(gameID, character);
        return character;
    }

    /**
     * Deletes an Attribute from a Characters AttributeSet
     *
     * @param gameID      gameID
     * @param characterID characterID
     * @param attribute   Attribute-Name
     * @return the Character
     */
    public PlayerCharacter deletePlayerAttribute(long gameID, long characterID, String attribute) {
        PlayerCharacter character = getGameFromID(gameID).getDungeon().getPlayerCharacterByID(characterID);
        character.deletePlayerAttribute(attribute);
        sendCharacterChange(gameID, character);
        return character;
    }

    /**
     * Ends a GameSession. Kicks all Players and saves Game in DB
     *
     * @param gameID gameID
     * @return true, if game was ended
     */
    public boolean endGame(long gameID) {
        Game game = getGameFromID(gameID);
        game.setRunning(false);
        if (mongoHelper.saveInDB(game)) {
            activeGames.remove(gameID);
            notifyAboutNewOverview(game);
            notifyAboutEnding(gameID);
            return true;
        }
        return false;
    }

    private void notifyAboutEnding(long gameID) {
        template.convertAndSend("/topic/endGame/" + gameID, "");
    }

    private void notifyAboutNewOverview(Game game) {
        for (long x : game.getPlayerCharacters().keySet()) {
            template.convertAndSend("/topic/overviewRefresh/" + x, "");
        }
    }

    /**
     * Places a player character inside a specified room.
     *
     * @param gameID      identifier of the game that contains the character
     * @param characterID identifier of the character that wil be moved
     * @param roomID      identifier of the room the character will be moved to
     * @return the player character inside the new room
     */
    public PlayerCharacter placeCharacter(long gameID, long characterID, long roomID) {
        long oldRoomID = getGameFromID(gameID).getDungeon().getPlayerCharacterByID(characterID).getLocation().getId();
        PlayerCharacter player = getGameFromID(gameID).getDungeon().placeCharacter(characterID, roomID);
        sendRoomChange(gameID, oldRoomID, player.getLocation());
        return player;
    }

    private void sendRoomChange(long gameID, long oldRoomID, Room room) {
        template.convertAndSend("/topic/game/" + gameID + "/room/" + oldRoomID, room);
    }

    /**
     * Starts a death timer when a dungeon master leaves the webpage without properly closing the dungeon.
     * If the time runs out without the dungeon master rejoining the games is ended
     *
     * @param gameID the game for which the timer should be started
     */
    public void setDeathWatch(long gameID) {
        watchedGames.put(gameID, 0);
        System.out.println("Death Watch");
        //new Thread(new DeathService(gameID)).start();

    }

    public void killGame(long gameID) {
        if(activeGames.get(gameID) != null) {
            endGame(gameID);
        }
        watchedGames.remove(gameID);
    }

    /**
     * Stops the death timer when the dungeon master rejoins again
     *
     * @param gameID the game for which the timer should be stopped
     */
    public void resetDeathWatch(long gameID) {
        watchedGames.put(gameID, 0);
    }


    public boolean changeDungeonMaster(long gameID, long characterID) {
        Game game = activeGames.get(gameID);
        for(long g : game.getPlayerCharacters().keySet()) {
            List<PlayerCharacter> characters = game.getPlayerCharacters().get(g);
            for(PlayerCharacter chara : characters) {
                if(chara.getId() == characterID) {
                    game.changeDungeonMaster((int) g);
                    playerQuitGame(gameID, characterID);
                    template.convertAndSend("/topic/game/" + gameID + "/changeDM/" + characterID, "");
                    return true;
                }
            }
        }
        return false;
    }

    private boolean playerQuitGame(long gameID, long characterID) {
        Dungeon dungeon = getGameFromID(gameID).getDungeon();
        PlayerCharacter playerCharacter = dungeon.getPlayerCharacterByID(characterID);
        boolean success = dungeon.getActiveCharacters().remove(playerCharacter);
        if(success) {
            template.convertAndSend("/topic/game/" + gameID + "/delChar/", characterID);
            return true;
        }
        return false;
    }

   // private synchronized Boolean getFromWatchedGames(long gameID) {
   //     return watchedGames.get(gameID);
   // }

  //  private synchronized void setWatchedGameTrue(long gameID) {
   //     watchedGames.replace(gameID, true);
 //   }

    private synchronized void removeWatchedGame(long gameID) {
        watchedGames.remove(gameID);
    }

}
