package mudbackend.services;

import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.events.RoomEvent;
import mudbackend.entity.general.Dungeon;
import mudbackend.entity.general.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class DungeonBuilderRoomEventService {

    DungeonMasterService dungeonMasterService;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    public DungeonBuilderRoomEventService(DungeonMasterService masterService) {
        this.dungeonMasterService = masterService;
    }

    /**
     * Adds a template of a room event to a dungeon.
     *
     * @param game  game that will be modified
     * @param event event that will be added
     * @return modified game
     */
    public Game addRoomEventTemplate(Game game, RoomEvent event) {
        game.getDungeon().addRoomEventTemplate(event);
        return game;
    }

    private void sendRoomChange(long gameID, Room room) {
        template.convertAndSend("/topic/game/" + gameID + "/room/" + room.getId(), room);
    }

    /**
     * Adds a room event to a room in a dungeon.
     *
     * @param game   game that will be modified
     * @param roomID id of the room in which the room event will be set
     * @param event  event that will be added
     * @return modified game
     */
    public Game addRoomEvent(Game game, long roomID, RoomEvent event) {
        Dungeon dungeon = game.getDungeon();
        dungeon.addRoomEvent(roomID, event);
        if (dungeonMasterService.getActiveGames().get(game.getId()) != null) {
            sendRoomChange(game.getId(), dungeon.getRoomByID(roomID));
        }
        return game;
    }

    /**
     * Deletes a room event from the dungeon.
     *
     * @param game    game that will be modified
     * @param roomID  id of the room from which the event will be deleted
     * @param eventID id of the event that will be deleted
     * @return modified game
     */
    public Game deleteRoomEvent(Game game, long roomID, long eventID) {
        Dungeon dungeon = game.getDungeon();
        dungeon.deleteRoomEvent(roomID, eventID);
        if (dungeonMasterService.getActiveGames().get(game.getId()) != null) {
            sendRoomChange(game.getId(), dungeon.getRoomByID(roomID));
        }
        return game;
    }

    /**
     * Deletes the template of a room event from the dungeon.
     *
     * @param game    game that will be modified
     * @param eventID id of the event that will be deleted
     * @return modified game
     */
    public Game deleteRoomEventTemplate(Game game, long eventID) {
        game.getDungeon().deleteRoomEventTemplate(eventID);
        return game;
    }
}
