package mudbackend.services;

import mudbackend.entity.dungeondesign.NPC;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.general.Dungeon;
import mudbackend.entity.general.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class DungeonBuilderNPCService {

    DungeonMasterService dungeonMasterService;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    public DungeonBuilderNPCService(DungeonMasterService masterService) {
        this.dungeonMasterService = masterService;
    }


    /**
     * Adds an NPC template to a dungeon.
     *
     * @param game game that will be modified
     * @param npc  npc that will be added
     * @return modified game
     */
    public Game addNPCTemplate(Game game, NPC npc) {
        game.getDungeon().addNPCTemplate(npc);
        return game;
    }

    private void sendRoomChange(long gameID, Room room) {
        template.convertAndSend("/topic/game/" + gameID + "/room/" + room.getId(), room);
    }

    /**
     * Adds an NPC to a dungeon.
     *
     * @param game   game that will be modified
     * @param roomID id of the room in which the npc will be put
     * @param npc    npc that will be added
     * @return modified game
     */
    public Game addNPC(Game game, long roomID, NPC npc) {
        Dungeon dungeon = game.getDungeon();
        dungeon.addNPC(roomID, npc);
        if (dungeonMasterService.getActiveGames().get(game.getId()) != null) {
            sendRoomChange(game.getId(), dungeon.getRoomByID(roomID));
        }
        return game;
    }

    /**
     * Removes an NPC from a dungeon.
     *
     * @param game   game that will be modified
     * @param roomID id of the room in which the npc is
     * @param npcID  id of the npc that will be deleted
     * @return modified game
     */
    public Game removeNPC(Game game, long roomID, long npcID) {
        Dungeon dungeon = game.getDungeon();
        dungeon.deleteNPC(roomID, npcID);
        if (dungeonMasterService.getActiveGames().get(game.getId()) != null) {
            sendRoomChange(game.getId(), dungeon.getRoomByID(roomID));
        }
        return game;
    }

    /**
     * Removes an NPC template from a dungeon.
     *
     * @param game  game that will be modified
     * @param npcID id of the npc that will be deleted
     * @return modified game
     */
    public Game removeNPCTemplate(Game game, long npcID) {
        game.getDungeon().deleteNPCTemplate(npcID);
        return game;
    }
}
