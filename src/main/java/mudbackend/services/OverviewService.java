package mudbackend.services;

import mudbackend.accountservice.Account;
import mudbackend.database.MongoHelper;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.general.DungeonStatusEnum;
import mudbackend.entity.general.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OverviewService {

    @Autowired
    private MongoHelper mongoHelper;

    @Autowired
    private AccountService accountService;

    @Autowired
    private DungeonPlayerService dungeonPlayerService;

    @Autowired
    private DungeonBuilderService dungeonBuilderService;

    @Autowired
    private DungeonMasterService dungeonMasterService;

    @Autowired
    SimpMessagingTemplate template;

    /**
     * Retrieves all running games.
     *
     * @return list of all running games
     */
    public List<Game> getActiveGames(int userID) {
        List<Long> gameIDList =
                Objects.requireNonNull(accountService.getLoggedIn().stream().filter(item -> item.getId() == userID).findFirst().orElse(null)).getJoinedGames();
        if (dungeonMasterService.getActiveGames() != null) {
            List<Game> games = new ArrayList<>();
            for (Game game : dungeonMasterService.getActiveGames().values()) {
                if (gameIDList.contains(game.getId())) games.add(game);
                else if (game.getDungeonMaster() == userID) games.add(game);
            }
            return games;
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves all games that are not running at the moment.
     *
     * @return list of all inactive games
     */
    public List<Game> getInactiveGames(int userID) {
        List<Long> gameIDList =
                Objects.requireNonNull(accountService.getLoggedIn().stream().filter(item -> item.getId() == userID).findFirst().orElse(null)).getJoinedGames();
        Map<String, Object> attr = new HashMap<>();
        attr.put("running", false);
        attr.put("dungeonStatus", 1);
        List<Game> inactiveGames = mongoHelper.getFromDB(Game.class, attr);
        if (inactiveGames != null) {
            List<Game> games = new ArrayList<>();
            for (Game game : inactiveGames) {
                if (gameIDList.contains(game.getId()) || game.getDungeonMaster() == userID) games.add(game);
            }
            return games;
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves all games built by a certain player.
     *
     * @param accountId id of the player for which all built games shall be retrieved
     * @return list of all built games
     */
    public List<Game> getOwnBuiltGames(int accountId) {
        Map<String, Object> attr = new HashMap<>();
        attr.put("builder", accountId);
        return mongoHelper.getFromDB(Game.class, attr);
    }

    /**
     * Retrieves all games in which the player is currently active.
     *
     * @param accountID id of the player
     * @return list of all games in which the player is active
     */
    public List<Game> getOwnPlayingGames(int accountID) {
        List<Long> gameIDList =
                Objects.requireNonNull(accountService.getLoggedIn().stream().filter(item -> item.getId() == accountID).findFirst().orElse(null)).getJoinedGames();
        Map<String, Object> attr = new HashMap<>();
        for (long gameID : gameIDList) {
            attr.put("id", gameID);
        }
        return mongoHelper.getFromDB(Game.class, attr);
    }

    /**
     * Retrieves a dungeon by its id.
     *
     * @param dungeonID id of the dungeon
     * @return instance of game
     */
    public Game searchDungeon(long dungeonID) {
        Map<String, Object> attr = new HashMap<>();
        attr.put("id", dungeonID);
        return mongoHelper.getFromDB(Game.class, attr).get(0);
    }

    /**
     * Rolls back a dungeon. Removes all Character-Information
     */
    public Game rollBackDungeon(long gameID) {
        Map<String, Object> attr = new HashMap<>();
        attr.put("id", gameID);
        List<Game> games = mongoHelper.getFromDB(Game.class, attr);
        if (games.isEmpty()) {
            return null;
        }
        Game game = games.get(0);
        game.changeDungeonStatus(DungeonStatusEnum.WIP);
        notifyAboutNewOverview(game);
        for(long x : game.getPlayerCharacters().keySet()) {
            Map<String, Object> query = new HashMap<>();
            query.put("id", x);
            List<Account> accs = mongoHelper.getFromDB(Account.class, query);
            accs.get(0).deleteLoggedIn(gameID);
            mongoHelper.saveInDB(accs.get(0));
        }
        game.rollback();
        mongoHelper.saveInDB(game);
        return game;
    }

    /**
     * Deletes a game if its not public
     *
     * @param gameID gameID
     * @return true if deleted
     */
    public int deleteDungeon(long gameID) {
        if (dungeonMasterService.getActiveGames().containsKey(gameID)) {
            return 0;
        }

        Map<String, Object> attr = new HashMap<>();
        attr.put("id", gameID);
        List<Game> dbResult = mongoHelper.getFromDB(Game.class, attr);
        if (dbResult == null || dbResult.size() != 1) return -1;
        Game game = dbResult.get(0);
        if (game.getDungeonStatus() == DungeonStatusEnum.PUBLIC) {
            return 0;
        }

        dungeonBuilderService.getDungeonsWIP().remove(gameID);
        return mongoHelper.deleteFromDB(Game.class, attr);
    }

    /**
     * Starts an inactive Dungeon. The starting user becomes the DungeonMaster
     *
     * @param gameID    gameID
     * @param accountID accountID
     * @return true if game was started
     */
    public boolean startDungeon(long gameID, int accountID) {
        if (dungeonMasterService.getActiveGames().containsKey(gameID)) {
            return false;
        }
        Map<String, Object> attr = new HashMap<>();
        attr.put("id", gameID);
        List<Game> dbResult = mongoHelper.getFromDB(Game.class, attr);
        if (dbResult == null || dbResult.size() != 1) return false;
        Game game = dbResult.get(0);
        if (game.getDungeonStatus().equals(DungeonStatusEnum.WIP)) return false;
        game.setRunning(true);
        game.setDungeonMaster(accountID);
        dungeonMasterService.startSaveThread();
        dungeonMasterService.getActiveGames().put(gameID, game);
        mongoHelper.saveInDB(game);
        notifyAboutNewOverview(game);
        return true;
    }


    /**
     * Creates a character and saves it in the given game
     *
     * @param gameID    the game to which the new character belongs
     * @param userID    the user to which the new character belongs
     * @param character the new character
     * @return true if successful
     */
    public boolean createCharacter(long userID, long gameID, PlayerCharacter character) {
        character.startAttributes();
        if (dungeonMasterService.getActiveGames().containsKey(gameID)) {
            accountService.addDungeonToJoined(userID, gameID);
            dungeonMasterService.getGameFromID(gameID).addCharacter(userID, character);
        }
        Map<String, Object> query = new HashMap<>();
        query.put("id", gameID);
        List<Game> dbResult = mongoHelper.getFromDB(Game.class, query);
        if (dbResult == null || dbResult.size() != 1) return false;
        accountService.addDungeonToJoined(userID, gameID);
        Game game = dbResult.get(0);
        boolean result = game.addCharacter(userID, character);
        mongoHelper.saveInDB(game);
        return result;
    }

    /**
     * Deletes an existing character
     *
     * @param gameID      the game to which the character belongs
     * @param characterID the character to delete
     * @return true if successful
     */
    public List<PlayerCharacter> deleteCharacter(long gameID, long characterID, long userID) {
        if (dungeonMasterService.getActiveGames().containsKey(gameID)) {
            dungeonMasterService.getGameFromID(gameID).deleteCharacter(characterID);
        }
        Map<String, Object> query = new HashMap<>();
        query.put("id", gameID);
        List<Game> dbResult = mongoHelper.getFromDB(Game.class, query);
        if (dbResult == null || dbResult.size() != 1) return null;
        Game game = dbResult.get(0);
        game.deleteCharacter(characterID);

        mongoHelper.saveInDB(game);
        return game.getCharactersOfPlayer(userID);
    }

    public List<PlayerCharacter> getCharactersOfDungeon(long userID, long gameID) {
        Map<String, Object> query = new HashMap<>();
        query.put("id", gameID);
        List<Game> dbResult = mongoHelper.getFromDB(Game.class, query);
        if (dbResult == null || dbResult.size() != 1) return null;
        Game game = dbResult.get(0);
        return game.getCharactersOfPlayer(userID);
    }

    /**
     * Allows a player to join an active dungeon and updates their location
     *
     * @param gameID      identifier of the game
     * @param accountID   identifier of the account holding the character
     * @param characterID identifier of the character
     * @return the modified game state
     */
    public Game joinDungeon(long gameID, long accountID, long characterID) {
        Game game = this.dungeonMasterService.getGameFromID(gameID);
        PlayerCharacter playerCharacter =
                game.getPlayerCharacters().get(accountID).stream().filter(character -> character.getId() == characterID).findFirst().orElse(null);
        assert playerCharacter != null;
        game.getDungeon().getActiveCharacters().add(playerCharacter);
        if (playerCharacter.getLocation() == null) {
            Room startRoom = game.getDungeon().getStartRoom();
            startRoom.getPresentCharacters().add(playerCharacter.getId());
            playerCharacter.updateLocation(startRoom);
        }
        notifyAboutNewCharacter(gameID, playerCharacter);
        return game;
    }

    /**
     * Sets the password of a game so when players try to enter, they have to authorize.
     *
     * @param gameID   identifier of the game
     * @param password password that will be set
     */
    public void setPassword(long gameID, String password) {
        Game game = this.dungeonMasterService.getGameFromID(gameID);
        if (game == null) {
            game = this.getGameFromDB(gameID);
            assert game != null;
            if(password == null){
                game.setPassword(null);
            }else{
                game.setPassword(password);
            }
            mongoHelper.saveInDB(game);
        }
        else {
            if(password == null){
                game.setPassword(null);
            }else{
                game.setPassword(password);
            }
        }
    }

    /**
     * Checks if the password a player entered is correct.
     *
     * @param gameID   identifier of the game
     * @param password password that will be set
     */
    public boolean checkPassword(long gameID, String password) {
        Game game = this.dungeonMasterService.getGameFromID(gameID);
        if (game == null) {
            game = this.getGameFromDB(gameID);
        }
        assert game != null;
        return game.getPassword().equals(password);
    }

    /**
     * Checks if the game has a password set or not
     *
     * @param gameID identifier of the game
     */
    public boolean getGamePassword(long gameID) {
        Game game = this.dungeonMasterService.getGameFromID(gameID);
        if (game == null) {
            game = this.getGameFromDB(gameID);
        }
        assert game != null;
        String password = game.getPassword();
        return (password == null);
    }

    private Game getGameFromDB(long gameID) {
        Map<String, Object> query = new HashMap<>();
        query.put("id", gameID);
        List<Game> dbResult = mongoHelper.getFromDB(Game.class, query);
        if (dbResult == null || dbResult.size() != 1) return null;
        return dbResult.get(0);
    }

    /**
     * Retrieves a list of all dungeons that are public.
     *
     * @return list of all public game instances
     */
    public List<Game> getPublicDungeons() {
        Map<String, Object> query = new HashMap<>();
        query.put("dungeonStatus", 1);
        return mongoHelper.getFromDB(Game.class, query);
    }


    private void notifyAboutNewOverview(Game game) {
        for (long x : game.getPlayerCharacters().keySet()) {
            template.convertAndSend("/topic/overviewRefresh/" + x, "");
        }
        template.convertAndSend("/topic/overviewRefresh/" + game.getBuilder(), "");
        template.convertAndSend("/topic/overviewRefresh/" + game.getDungeonMaster(), "");
    }


    private void notifyAboutNewCharacter(long gameID, PlayerCharacter character) {
        template.convertAndSend("/topic/game/" + gameID + "/newChar/", character);
    }
}
