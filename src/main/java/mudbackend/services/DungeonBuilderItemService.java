package mudbackend.services;

import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.general.Dungeon;
import mudbackend.entity.general.Game;
import mudbackend.entity.items.ItemForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class DungeonBuilderItemService {

    DungeonMasterService dungeonMasterService;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    public DungeonBuilderItemService(DungeonMasterService masterService) {
        this.dungeonMasterService = masterService;
    }

    /**
     * Adds an item template to the dungeon that can be reused multiple times.
     *
     * @param game     game that will be modified
     * @param itemForm item in specialised front end class
     * @return modified game
     */
    public Game addItemTemplate(Game game, ItemForm itemForm) {
        game.getDungeon().addItemTemplate(itemForm.toItem());
        return game;
    }


    private void sendRoomChange(long gameID, Room room) {
        template.convertAndSend("/topic/game/" + gameID + "/room/" + room.getId(), room);
    }

    /**
     * Adds an item to a given room.
     *
     * @param game     game that will be modified
     * @param roomID   identifier of the room
     * @param itemForm item in specialised front end class
     * @return modified game
     */
    public Game addItem(Game game, long roomID, ItemForm itemForm) {
        Dungeon dungeon = game.getDungeon();
        dungeon.addItem(roomID, itemForm.toItem());
        if (dungeonMasterService.getActiveGames().get(game.getId()) != null) {
            sendRoomChange(game.getId(), dungeon.getRoomByID(roomID));
        }
        return game;
    }

    /**
     * Deletes a given item from a given room.
     *
     * @param game   game that will be modified
     * @param roomID identifier of the room
     * @param itemID identifier of the item
     * @return modified game
     */
    public Game removeItem(Game game, long roomID, long itemID) {
        Dungeon dungeon = game.getDungeon();
        dungeon.deleteItem(roomID, itemID);
        if (dungeonMasterService.getActiveGames().get(game.getId()) != null) {
            sendRoomChange(game.getId(), dungeon.getRoomByID(roomID));
        }
        return game;
    }

    /**
     * Deletes a previously added item template.
     *
     * @param game   game that will be modified
     * @param itemID identifier of the template that will be removed from the respective dungeon list
     * @return modified game
     */
    public Game removeItemTemplate(Game game, long itemID) {
        game.getDungeon().deleteItemTemplate(itemID);
        return game;
    }
}
