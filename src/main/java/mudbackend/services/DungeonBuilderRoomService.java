package mudbackend.services;

import mudbackend.entity.dungeondesign.DirectionEnum;
import mudbackend.entity.dungeondesign.GraphElement;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.general.Game;
import mudbackend.entity.general.MatrixInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class DungeonBuilderRoomService {

    @Autowired
    SimpMessagingTemplate template;

    private void sendRoomChange(long gameID, MatrixInformation[][] matrix) {
        template.convertAndSend("/topic/game/" + gameID + "/rooms", matrix);
    }

    private void sendRoomChangeWithoutMatrix(long gameID) {
        template.convertAndSend("/topic/game/" + gameID + "/roomsWM", "");
    }

    /**
     * Adds a room to the dungeon.
     *
     * @param game        game that will be modified
     * @param name        name of the room
     * @param description description of the room
     * @return modified game
     */
    public Room addRoom(Game game, String name, String description) {
        Room room = new Room(name, description);
        if (game.getDungeon().addRoom(room)) {
            sendRoomChangeWithoutMatrix(game.getId());
            return room;
        }
        return null;
    }

    /**
     * Adds a pathway between two rooms in a dungeon.
     *
     * @param game      game that will be modified
     * @param from      id of the first room
     * @param to        id of the second room
     * @param direction compass direction through the rooms will be connected
     * @return modified game
     */
    public Game addPathway(Game game, long from, long to, DirectionEnum direction) {
        game.getDungeon().addPathway(from, to, direction);
        sendRoomChangeWithoutMatrix(game.getId());
        return game;
    }

    /**
     * Deletes the path between two rooms in a dungeon.
     *
     * @param game game that will be modified
     * @param from id of the first room
     * @param to   id of the second room
     * @return modified game
     */
    public Game deletePathway(Game game, long from, long to) {
        DirectionEnum directionEnum = game.getDungeon().getDirectionOfPathway(from, to);
        game.getDungeon().deletePathway(from, to);
        if (isConnectedToStartroom(game, from) && isConnectedToStartroom(game, to)) {
            sendRoomChangeWithoutMatrix(game.getId());
            return game;
        }
        game.getDungeon().addPathway(from, to, directionEnum);
        return null;
    }

    /**
     * Removes a room from the dungeon and deletes all associated pathways.
     *
     * @param game   game that will be modified
     * @param roomID id of the room that will be removed
     * @return modified game
     */
    public Game removeRoom(Game game, long roomID) {
        Room r = game.getDungeon().getRoomByID(roomID);
        GraphElement graphElements = game.getDungeon().getRooms().get(r);
        game.getDungeon().deleteRoom(roomID);
        for (Room room : game.getDungeon().getRooms().keySet()) {
            if (!isConnectedToStartroom(game, room.getId())) {
                game.getDungeon().addRoom(r);

                Room directionRoom = graphElements.getRoomInDirection(DirectionEnum.NORTH);
                if (directionRoom != null)
                    game.getDungeon().addPathway(roomID, directionRoom.getId(), DirectionEnum.NORTH);

                directionRoom = graphElements.getRoomInDirection(DirectionEnum.EAST);
                if (directionRoom != null)
                    game.getDungeon().addPathway(roomID, directionRoom.getId(), DirectionEnum.EAST);

                directionRoom = graphElements.getRoomInDirection(DirectionEnum.WEST);
                if (directionRoom != null)
                    game.getDungeon().addPathway(roomID, directionRoom.getId(), DirectionEnum.WEST);

                directionRoom = graphElements.getRoomInDirection(DirectionEnum.SOUTH);
                if (directionRoom != null)
                    game.getDungeon().addPathway(roomID, directionRoom.getId(), DirectionEnum.SOUTH);

                return null;
            }
        }
        sendRoomChangeWithoutMatrix(game.getId());
        return game;
    }

    /**
     * Edits the name and description of a room
     *
     * @param game        game
     * @param roomID      roomID
     * @param name        new Name
     * @param description new Description
     * @return the room
     */
    public Room editNameAndDescription(Game game, long roomID, String name, String description) {
        game.getDungeon().getRoomByID(roomID).setName(name);
        game.getDungeon().getRoomByID(roomID).setDescription(description);
        sendRoomChangeWithoutMatrix(game.getId());
        return game.getDungeon().getRoomByID(roomID);
    }

    /**
     * Checks if a room is connected to the startroom of a dungeon
     *
     * @param game   the game
     * @param roomID identifier of the room
     * @return true if they are connected
     */
    public boolean isConnectedToStartroom(Game game, long roomID) {
        return game.getDungeon().isConnectedToStartroom(roomID, new ArrayList<>());
    }
}
