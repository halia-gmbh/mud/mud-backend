package mudbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MudBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MudBackendApplication.class, args);
    }

}
