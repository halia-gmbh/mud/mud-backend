package mudbackend.accountservice;

import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.MailerBuilder;

/**
 * Helper class to send mails to users.
 *
 * @author Dominik Sauerer
 * @version 1.0
 */
public class EmailHelper {

    Mailer mailer;

    /**
     * Constructs an EmailHelper and sets up the mailer from which the email are sent.
     */
    public EmailHelper() {
        this.mailer = MailerBuilder
                .withSMTPServerHost("in-v3.mailjet.com")
                .withSMTPServerUsername("faffa55cfd87f407a4eaa4d99d4eedb1")
                .withSMTPServerPassword("5888d405365b2d62f2f5488e5dee9dee")
                .withSMTPServerPort(587)
                .async()
                .buildMailer();
    }

    /**
     * Sends a password to the given email address.
     *
     * @param emailAdr the email address to which should be sent
     * @param password the (unencrypted) password which should be sent
     * @param send     true, if the mail should really be sent;
     *                 false, if it should only be controlled, if the email could be sent (debug mode)
     * @return true, if the email was specified right and is capable of sending; false, otherwise
     */
    public boolean sendPassword(String emailAdr, String password, boolean send) {
        Email email = EmailBuilder.startingBlank()
                .from("gruppe2swe@gmail.com")
                .to("user", emailAdr)
                .withSubject("Your new password for Halia's Dungeon")
                .withPlainText("You requested a new password for Halia's Dungeon!\n" +
                        "Your new password is " + password)
                .buildEmail();
        if (send) mailer.sendMail(email);
        try {
            return mailer.validate(email);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Sends a account confirmation mail including the activation token to the given email
     *
     * @param emailAdr the email address to which should be sent
     * @param token    the activation token generated from the account object
     * @param send     true, if the mail should really be sent;
     *                 false, if it should only be controlled, if the email could be sent (debug mode)
     * @return true, if the email was specified right and is capable of sending; false, otherwise
     */
    public boolean sendConfirmationMail(String emailAdr, String token, boolean send) {
        Email email = EmailBuilder.startingBlank()
                .from("gruppe2swe@gmail.com")
                .to("user", emailAdr)
                .withSubject("Welcome to Halia's Dungeon!")
                .withPlainText("Welcome to Halia's Dungeon!\n" +
                        "To activate your account please visit http://193.196.55.121/confirm and enter the following token.\n" +
                        "Your activation token is " + token)
                .buildEmail();
        if (send) mailer.sendMail(email);
        try {
            return mailer.validate(email);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Mailer getMailer() {
        return mailer;
    }
}
