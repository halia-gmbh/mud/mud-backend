package mudbackend.accountservice;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;

import java.util.UUID;

@Getter
@Setter
public class Admin implements DBSavable {

    @JsonIgnore
    private EmailHelper emailHelper;

    private long id;
    private String email;
    private String password;

    private String token;
    private String confirmToken;

    /**
     * Contructor.
     *
     * @param email    String - email corresponding to the account
     * @param password String - password corresponding to the account
     */
    public Admin(String email, String password) {
        this.id = UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE;
        this.email = email;
        this.password = password;
        this.emailHelper = new EmailHelper();
    }

    public Admin(DBObject object) {
        this.id = (long) object.get("id");
        this.email = (String) object.get("email");
        this.password = (String) object.get("password");
        this.confirmToken = (String) object.get("confirmToken");
        this.emailHelper = new EmailHelper();
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject("id", id)
                .append("email", email)
                .append("password", password)
                .append("confirmToken", confirmToken);
    }

    @Override
    public void toJavaObject(DBObject dbObject) {

    }
}
