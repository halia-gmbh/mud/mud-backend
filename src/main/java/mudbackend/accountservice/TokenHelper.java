package mudbackend.accountservice;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Manages Tokens for Authorization
 */
@Service
public class TokenHelper {

    @Value("${authorization.encryptionKey}")
    private String encryptCode;

    /**
     * Returns Username connected to a Token
     *
     * @param token the Token
     * @return username
     */
    public String getUsernameFromToken(String token) {
        try {
            return getClaimFromToken(token, Claims::getSubject);
        } catch (MalformedJwtException e) {
            return null;
        }
    }

    /**
     * Validates a token and a username
     *
     * @param token       token
     * @param userDetails userDetails
     * @return ValidationResult
     */
    public boolean validateToken(String token, UserDetails userDetails) {
        final String user = getUsernameFromToken(token);
        if (user != null) {
            return (user.equals(userDetails.getUsername()));
        }
        return false;
    }

    /**
     * Generates a Token for a User
     *
     * @param userDetails userDetails
     * @return Token
     */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return Jwts.builder().setClaims(claims).setSubject(userDetails.getUsername()).setIssuedAt(new Date(System.currentTimeMillis()))
                .signWith(SignatureAlgorithm.HS512, encryptCode).compact();
    }

    /**
     * Gets a username from a Token
     *
     * @param token          Token
     * @param claimsResolver claimsResolver
     * @return username
     */
    private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = Jwts.parser().setSigningKey(encryptCode).parseClaimsJws(token).getBody();
        return claimsResolver.apply(claims);
    }

}
