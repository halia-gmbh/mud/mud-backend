package mudbackend.accountservice;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationHelper {

    @Autowired
    private TokenHelper tokenHelper;

    @Autowired
    private UserDetailServiceImpl userDetailsService;

    /**
     * Authenticates a given token by frontend and checks, if it is correct
     *
     * @param jwtToken the given token
     * @throws RuntimeException if the token authentication fails
     */
    public void doFilterInternal(String jwtToken) {
        String username = null;

        if (jwtToken != null) {
            jwtToken = parseToken(jwtToken);
            try {
                username = tokenHelper.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
                throw e;
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
                throw e;
            }
        } else {
            throw new RuntimeException();
        }
        if (username != null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (!tokenHelper.validateToken(jwtToken, userDetails)) {
                throw new RuntimeException();
            }
        } else {
            throw new RuntimeException();
        }
    }

    /**
     * Removes Bearer-part from Bearer-Token
     *
     * @param bearerToken Bearer-Token
     * @return token
     */
    private String parseToken(String bearerToken) {
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }
}
