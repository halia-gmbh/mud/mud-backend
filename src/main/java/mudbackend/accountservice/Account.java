package mudbackend.accountservice;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class Account implements IAccount, DBSavable {

    @JsonIgnore
    private EmailHelper emailHelper;

    private long id;
    private String email;
    private String password;
    private List<Long> dungeonsBuilt;
    private List<Long> dungeonsMaster;
    private List<Long> joinedGames;
    private boolean unlocked;

    private String token;
    private String confirmToken;

    /**
     * Contructor.
     *
     * @param email    String - email corresponding to the account
     * @param password String - password corresponding to the account
     */
    public Account(String email, String password) {
        this.id = UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE;
        this.email = email;
        this.password = password;
        this.dungeonsBuilt = new ArrayList<>();
        this.dungeonsMaster = new ArrayList<>();
        this.joinedGames = new ArrayList<>();
        this.unlocked = false;
        this.emailHelper = new EmailHelper();
    }

    /**
     * @param object
     */
    public Account(DBObject object) {
        this.id = (long) object.get("id");
        this.email = (String) object.get("email");
        this.password = (String) object.get("password");
        this.dungeonsBuilt = (List<Long>) object.get("dungeonsBuilt");
        this.dungeonsMaster = (List<Long>) object.get("dungeonsMaster");
        this.joinedGames = (List<Long>) object.get("joinedGames");
        this.unlocked = (boolean) object.get("unlocked");
        this.emailHelper = new EmailHelper();
        this.confirmToken = (String) object.get("confirmToken");
    }

    public void deleteLoggedIn(long game) {
        for(int i = 0; i<joinedGames.size(); i++) {
            if(joinedGames.get(i) == game) {
                joinedGames.remove(i);
                break;
            }
        }
    }

    /**
     * Adds gameId to list of dungeons, which the user has created.
     *
     * @param gameId Long - identifier of a game
     */
    public void addCreatedDungeon(Long gameId) {
        dungeonsBuilt.add(gameId);
    }

    /**
     * Adds gameId to list of joined games.
     *
     * @param gameId Long - identifier of a game
     * @return boolean - true if game was added, otherwise false
     */
    public boolean joinGame(Long gameId) {
        for(long x : joinedGames) {
            if(gameId == x){
                return true;
            }
        }
        return joinedGames.add(gameId);
    }

    /**
     * Confirms account through setting the attribute "unlocked" to true.
     *
     * @return boolean - true if account confirmation was successful, otherwise false
     */
    public boolean confirmAccount() {
        return unlocked = true;
    }


    /**
     * Generates a new password and sends it per mail
     *
     * @return true, if successful; false, otherwise
     */
    public boolean resetPassword() {
        String newPassword = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
        this.password = DigestUtils.md5Hex(newPassword);
        return emailHelper.sendPassword(email, newPassword, true);
    }

    /**
     * Generates a confirmation token and sends it per mail
     */
    public void sendConfirmationMail() {
        this.emailHelper.sendConfirmationMail(email, createNewConfirmToken(), true);
    }

    /**
     *
     */
    public void getUserDetails() {
        // TODO: implement method
    }

    /**
     * Converts java object to an object that can used with the database.
     */
    @Override
    public DBObject toDBObject() {
        return new BasicDBObject("id", id)
                .append("email", email)
                .append("password", password)
                .append("dungeonsBuilt", dungeonsBuilt)
                .append("dungeonMaster", dungeonsMaster)
                .append("joinedGames", joinedGames)
                .append("unlocked", unlocked)
                .append("confirmToken", confirmToken);
    }

    /**
     * Converts database object to an object that can used within a java process.
     *
     * @param dbObject object to convert
     */
    @Override
    public void toJavaObject(DBObject dbObject) {

    }

    /**
     * Creates a new confirmation token which can be send per mail to activate the account.
     *
     * @return the generated token
     */
    private String createNewConfirmToken() {
        this.confirmToken = UUID.randomUUID().toString();
        return confirmToken;
    }
}
