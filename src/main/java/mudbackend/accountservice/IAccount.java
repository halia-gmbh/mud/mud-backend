package mudbackend.accountservice;

import java.util.List;

public interface IAccount {
    String getEmail();

    String getPassword();

    long getId();

    List<Long> getJoinedGames();
}
