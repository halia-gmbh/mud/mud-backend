package mudbackend.database;

import com.mongodb.*;
import com.mongodb.client.model.DBCollectionFindAndModifyOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Connects backend to the MongoDB database and provides all needed functions to interact with it.
 *
 * @author Dominik Sauerer
 */
@Service
public class MongoHelper {
    @Value("${mongoDB.IP}")
    private String mongoIP;

    @Value("${mongoDB.Port}")
    private int mongoPort;

    @Value("${mongoDB.DBName}")
    private String dbName;

    private MongoClient client;
    private DB database;

    /**
     * Switches the MongoHelper to test mode for unit and integration tests
     *
     * @throws UnknownHostException
     */
    public void switchToTest() throws UnknownHostException {
        this.client = new MongoClient(new ServerAddress(mongoIP, mongoPort));
        this.database = client.getDB("HaliaTest");
    }

    public void init() throws UnknownHostException {
        this.client = new MongoClient(new ServerAddress(mongoIP, mongoPort));
        this.database = client.getDB(dbName);
    }

    /**
     * Gets all DB documents from the database with the specified attributes and converts them to correct java objects
     *
     * @param coll The java class, to which the documents should be converted (class must implement DBSavable)
     * @param attr The attributes the documents to find should have. (String => attribute name; Object => attribute value)
     * @return A list of the fitting java objects; null, if the conversion to a java object fails at any point
     */
    public <T extends DBSavable> List<T> getFromDB(Class<T> coll, Map<String, Object> attr) {
        if (client == null) {
            try {
                init();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        String collName = getCollectionName(coll.getSimpleName());
        if (collName == null) return null;
        BasicDBObject query = transformToQuery(attr);
        DBCursor queryResult = database.getCollection(collName).find(query);
        List<T> resultList = new ArrayList<>();
        while (queryResult.hasNext()) {
            DBObject x = queryResult.next();
            try {
                T obj = coll.getDeclaredConstructor(DBObject.class).newInstance(x);
                resultList.add(obj);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }
        return resultList;
    }

    /**
     * Inserts or updates a DBSavable object as document in the respective collection. Used its id attribute to check,
     * if the object already exists.
     *
     * @param object the DBSavable object, to be saved
     * @return true, if successful; false, else
     */
    public boolean saveInDB(DBSavable object) {
        if (client == null) {
            try {
                init();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        String collName = getCollectionName(object.getClass().getSimpleName());
        DBObject dbObject = object.toDBObject();
        long OldDocID = (long) dbObject.get("id");
        DBCollectionFindAndModifyOptions options = new DBCollectionFindAndModifyOptions();
        options.upsert(true);
        options.update(dbObject);
        database.getCollection(collName).findAndModify(new BasicDBObject("id", OldDocID), options);
        return true;
    }

    /**
     * Deletes database documents in the given collection with the given attributes
     *
     * @param coll The java class, to which the documents belong
     * @param attr The attributes the documents to find should have. (String => attribute name; Object => attribute value)
     * @return the number of deleted documents; if -1, something before the database search went wrong
     */
    public <T> int deleteFromDB(Class<T> coll, Map<String, Object> attr) {
        if (client == null) {
            try {
                init();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        String collName = getCollectionName(coll.getSimpleName());
        if (collName == null) return -1;
        BasicDBObject query = transformToQuery(attr);
        int queryResult = database.getCollection(collName).find(query).count();
        WriteResult writeResult = database.getCollection(collName).remove(query);
        if (writeResult.wasAcknowledged() && writeResult.getN() == queryResult) {
            return writeResult.getN();
        }
        return 0;
    }

    /**
     * Transforms a map to a DBObject needed for a DB query
     *
     * @param attr the String-Object Map containing the needed attributes/query params
     * @return a BasicDBObject for the DB query
     */
    private BasicDBObject transformToQuery(Map<String, Object> attr) {
        BasicDBObject query = new BasicDBObject();
        if (attr != null) {
            for (Map.Entry<String, Object> entry : attr.entrySet()) {
                query.append(entry.getKey(), entry.getValue());
            }
        }
        return query;
    }

    /**
     * Gets MongoDB collection name for the given basic java class name
     *
     * @param className java class name in basic format" (without package , i.e. "MongoHelper")
     * @return name of the respective MongoDB collection name; null, if no collection could not be found
     */
    private String getCollectionName(String className) {
        if (className.equals("TestContainer")) return "TestContainer";
        else if (className.equals("Account")) return "Account";
        else if (className.equals("Game")) return "Game";
        else if (className.equals("Admin")) return "Admin";
        else return null;
    }

    /**
     * Makes a DBObject out of a List of DBSavables
     *
     * @param list list
     * @return DBOject
     */
    public static DBObject listToDB(List<? extends DBSavable> list) {
        List<DBObject> objects = new ArrayList<>();
        for (DBSavable dbSavable : list) {
            objects.add(dbSavable.toDBObject());
        }
        return new BasicDBObject().append("list", objects);
    }

    /**
     * Makes a DBObject out of a Map of DBSavables
     *
     * @param map map
     * @return DBOject
     */
    public static DBObject mapToDB(Map<? extends DBSavable, ? extends DBSavable> map) {
        List<BasicDBObject> objects = new ArrayList<>();
        for (Map.Entry<? extends DBSavable, ? extends DBSavable> entry : map.entrySet()) {
            BasicDBObject basicDBObject = new BasicDBObject()
                    .append("key", entry.getKey().toDBObject());
            if (entry.getValue() != null) basicDBObject.append("value", entry.getValue().toDBObject());
            objects.add(basicDBObject);
        }
        return new BasicDBObject()
                .append("map", objects);
    }

    /**
     * Makes a DBObject out of a Map of Longs and DBSavables
     *
     * @param map map
     * @return DBOject
     */
    public static DBObject longmapToDB(Map<Long, Long> map) {
        List<BasicDBObject> objects = new ArrayList<>();
        for (Map.Entry<Long, Long> entry : map.entrySet()) {
            BasicDBObject basicDBObject = new BasicDBObject()
                    .append("key", entry.getKey())
                    .append("value", entry.getValue());
            objects.add(basicDBObject);
        }
        return new BasicDBObject()
                .append("map", objects);
    }
}