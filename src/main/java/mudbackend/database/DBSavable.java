package mudbackend.database;

import com.mongodb.DBObject;

public interface DBSavable {
    public DBObject toDBObject();

    public void toJavaObject(DBObject dbObject);

}
