package mudbackend.entity.events;

import lombok.Getter;
import mudbackend.entity.items.ItemForm;

/**
 * Form for creation of a Roomevent
 */
@Getter
public class Eventform {

    private long id;
    private String name;
    private String textResponse;
    private ItemForm itemResponse;

    /**
     * Creates a RoomEvent out of the form
     *
     * @return the RoomEvent
     */
    public RoomEvent toEvent() {
        if (textResponse == null && itemResponse == null) return null;
        if (textResponse == null) {
            ItemEvent itemEvent = new ItemEvent(name, itemResponse.toItem());
            itemEvent.setId(id);
            return itemEvent;
        }
        if (itemResponse == null) {
            TextEvent textEvent = new TextEvent(name, textResponse);
            textEvent.setId(id);
            return textEvent;
        }
        return null;
    }
}
