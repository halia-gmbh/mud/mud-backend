package mudbackend.entity.events;

import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;

@Getter
@Setter
public abstract class RoomEvent implements DBSavable {
    private long id;
    protected String name;
    private EventType eventType;

    /**
     * Function called when a player interacts with a room event.
     *
     * @return Item if the event is an item event | String if the event is a text event
     */
    public abstract Object interact();
}
