package mudbackend.entity.events;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextEvent extends RoomEvent {
    private String textResponse;

    /**
     * Constructor.
     *
     * @param name         name of the text event
     * @param textResponse response the event gives when interacting with it
     */
    public TextEvent(String name, String textResponse) {
        this.name = name;
        this.textResponse = textResponse;
        setEventType(EventType.TextEvent);
    }

    /**
     * Function called when a player interacts with the text event.
     *
     * @return response given by the text event
     */
    @Override
    public Object interact() {
        return textResponse;
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject()
                .append("id", getId())
                .append("name", name)
                .append("eventType", getEventType().getValue())
                .append("textResponse", textResponse);
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        setId((long) dbObject.get("id"));
        name = (String) dbObject.get("name");
        setEventType(EventType.TextEvent);
        textResponse = (String) dbObject.get("textResponse");
    }
}
