package mudbackend.entity.events;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.entity.characterdesign.AttributeSet;
import mudbackend.entity.items.*;

import java.util.Objects;

@Getter
@Setter
public class ItemEvent extends RoomEvent {
    private Item itemResponse;

    /**
     * Constructor.
     *
     * @param name         name of the text event
     * @param itemResponse response the event gives when interacting with it
     */
    public ItemEvent(String name, Item itemResponse) {
        this.name = name;
        this.itemResponse = itemResponse;
        setEventType(EventType.ItemEvent);
    }

    /**
     * Function called when a player interacts with the item event.
     *
     * @return response given by the item event
     */
    @Override
    public Object interact() {
        return itemResponse;
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject()
                .append("id", getId())
                .append("name", name)
                .append("eventType", getEventType().getValue())
                .append("itemResponse", itemResponse.toDBObject());
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        setId((long) dbObject.get("id"));
        name = (String) dbObject.get("name");
        setEventType(EventType.ItemEvent);
        DBObject object = (DBObject) dbObject.get("itemResponse");
        ItemTypesEnum itemTypesEnum = ItemTypesEnum.valueToEnum((int) object.get("itemType"));
        Item item = null;
        switch (Objects.requireNonNull(itemTypesEnum)) {
            case ARMOUR:
                item = new Armour("", 0, new AttributeSet());
                break;
            case WEAPON:
                item = new Weapon("", 0, new AttributeSet());
                break;
            case ACCESSOIRE:
                item = new Accessoire("", 0, new AttributeSet());
                break;
            case CONSUMABLE:
                item = new Consumable("", 0, new AttributeSet());
                break;
        }
        item.toJavaObject(object);
        itemResponse = item;
    }

}
