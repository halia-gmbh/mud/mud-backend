package mudbackend.entity.events;

import lombok.Getter;

/**
 * Type of a event
 */
@Getter
public enum EventType {
    TextEvent(0),
    ItemEvent(1);

    private int value;

    private EventType(int value) {
        this.value = value;
    }

    /**
     * Returns the Enum connected to a value
     *
     * @param i the value
     * @return the Enum
     */
    public static EventType valueToEnum(int i) {
        switch (i) {
            case 0:
                return TextEvent;
            case 1:
                return ItemEvent;
            default:
                return null;
        }
    }
}
