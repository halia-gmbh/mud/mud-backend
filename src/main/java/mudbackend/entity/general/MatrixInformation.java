package mudbackend.entity.general;

import lombok.Getter;
import lombok.Setter;
import mudbackend.entity.dungeondesign.Room;

/**
 * Content of the Dungeon-Matrix
 */
@Getter
@Setter
public class MatrixInformation {
    public Room room;
    public boolean north = false;
    public boolean west = false;
    public boolean south = false;
    public boolean east = false;
    public boolean startRoom = false;

    public MatrixInformation(Room room) {
        this.room = room;
    }
}
