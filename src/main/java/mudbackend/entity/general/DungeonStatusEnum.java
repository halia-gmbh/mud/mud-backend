package mudbackend.entity.general;

import lombok.Getter;

@Getter
public enum DungeonStatusEnum {
    WIP(0),
    PUBLIC(1);

    private final int value;

    DungeonStatusEnum(int value) {
        this.value = value;
    }

    public static DungeonStatusEnum valueToStatus(int i) {
        if (i == 1) {
            return DungeonStatusEnum.PUBLIC;
        }
        return DungeonStatusEnum.WIP;
    }
}
