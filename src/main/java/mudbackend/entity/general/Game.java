package mudbackend.entity.general;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;
import mudbackend.database.MongoHelper;
import mudbackend.entity.characterdesign.AttributeSet;
import mudbackend.entity.characterdesign.CharacterClass;
import mudbackend.entity.characterdesign.CharacterRace;
import mudbackend.entity.characterdesign.PlayerCharacter;

import java.util.*;

@Getter
@Setter
public class Game implements DBSavable {
    private long id;
    private boolean running;
    private String name;
    private int builder;
    private int dungeonMaster;
    private int maxActivePlayers;
    private DungeonStatusEnum dungeonStatus;
    private final List<CharacterClass> classes = new ArrayList<>();
    private final List<CharacterRace> races = new ArrayList<>();
    private final Map<Long, List<PlayerCharacter>> playerCharacters = new HashMap<>();
    private Dungeon dungeon;
    private String password;

    /**
     * Constructor.
     *
     * @param maxPlayers maximum amount of players allowed inside the dungeon
     * @param name       name of the dungeon
     * @param builder    creator of the dungeon
     */
    public Game(int maxPlayers, String name, int builder) {
        this.id = UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE;  // generate unique ID
        this.maxActivePlayers = maxPlayers;
        this.name = name;
        this.builder = builder;
        this.dungeonStatus = DungeonStatusEnum.WIP;
        this.dungeon = new Dungeon();
        this.password = null;
    }

    /**
     * Constructor for DBObject
     *
     * @param dbObject DBObject
     */
    public Game(DBObject dbObject) {
        toJavaObject(dbObject);
    }

    /**
     * Changes status of the dungeon.
     *
     * @param dungeonStatus status to which the dungeon will be set
     * @return true if change was sucessful, otherwise false
     */
    public boolean changeDungeonStatus(DungeonStatusEnum dungeonStatus) {
        this.dungeonStatus = dungeonStatus;
        return true;
    }

    public boolean addCharacter(long accountID, PlayerCharacter character) {
        List<PlayerCharacter> characters = playerCharacters.get(accountID);
        if (characters == null) {
            this.playerCharacters.put(accountID, new ArrayList<>());
            characters = playerCharacters.get(accountID);
        }
        character.setId(UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE);
        return characters.add(character);
    }

    public boolean deleteCharacter(long characterID) {
        for (List<PlayerCharacter> playerList : playerCharacters.values()) {
            for (int i = 0; i < playerList.size(); i++) {
                if (playerList.get(i).getId() == characterID) {
                    playerList.remove(i);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean changeDungeonMaster(int accountID) {
        this.dungeonMaster = accountID;
        return true;
    }

    /**
     * Adds a race option to the dungeon.
     *
     * @param race option that will be added
     * @return true if addition was sucessful, otherwise false
     */
    public boolean addRace(CharacterRace race) {
        if (race.getId() == -1) {
            long id = race.createID();
            races.add(race);
            return true;
        }
        for (CharacterRace characterRace : races) {
            if (characterRace.getId() == race.getId()) {
                characterRace.setName(race.getName());
                characterRace.setDescription(race.getDescription());
                characterRace.setAttributes(race.getAttributes());
                return true;
            }
        }
        races.add(race);
        return true;
    }

    /**
     * Removes a race option from the dungeon.
     *
     * @param raceId id of the option that will be removed
     * @return true if deletion was sucessful, otherwise false
     */
    public boolean removeRace(long raceId) {
        return this.races.removeIf(race -> race.getId() == raceId);
    }

    /**
     * Adds a class option to the dungeon.
     *
     * @param characterClass option that will be added
     * @return true if addition was sucessful, otherwise false
     */
    public boolean addClass(CharacterClass characterClass) {
        if (characterClass.getId() == -1) {
            long id = characterClass.createID();
            classes.add(characterClass);
            return true;
        }
        for (CharacterClass c : classes) {
            if (c.getId() == characterClass.getId()) {
                c.setName(characterClass.getName());
                c.setDescription(characterClass.getDescription());
                c.setAttributes(characterClass.getAttributes());
                return true;
            }
        }
        classes.add(characterClass);
        return true;
    }

    /**
     * Removes a class option from the dungeon.
     *
     * @param classId id of the option that will be removed
     * @return true if deletion was sucessful, otherwise false
     */
    public boolean removeClass(long classId) {
        return this.classes.removeIf(characterClass -> characterClass.getId() == classId);
    }

    /**
     * Rolls back a dungeon. Removes all Character-Information
     */
    public void rollback() {
        dungeon.rollback();
        playerCharacters.clear();
    }

    public List<PlayerCharacter> getCharactersOfPlayer(long accountID) {
        return this.playerCharacters.get(accountID);
    }

    @Override
    public DBObject toDBObject() {

        return new BasicDBObject()
                .append("id", id)
                .append("running", running)
                .append("name", name)
                .append("builder", builder)
                .append("dungeonMaster", dungeonMaster)
                .append("maxActivePlayers", maxActivePlayers)
                .append("dungeonStatus", dungeonStatus.getValue())
                .append("classes", MongoHelper.listToDB(classes))
                .append("races", MongoHelper.listToDB(races))
                .append("playerCharacters", charactersToDB())
                .append("dungeon", dungeon.toDBObject())
                .append("password", password);
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        id = (long) dbObject.get("id");
        running = (boolean) dbObject.get("running");
        name = (String) dbObject.get("name");
        builder = (int) dbObject.get("builder");
        dungeonMaster = (int) dbObject.get("dungeonMaster");
        maxActivePlayers = (int) dbObject.get("maxActivePlayers");
        dungeonStatus = DungeonStatusEnum.valueToStatus((int) dbObject.get("dungeonStatus"));
        if(dbObject.get("password") != null) {
            password = (String) dbObject.get("password");
        }else{
            password = null;
        }

        DBObject objects = (DBObject) dbObject.get("classes");
        ArrayList<DBObject> list = (ArrayList<DBObject>) (objects.get("list"));
        for (DBObject object : list) {
            CharacterClass characterclass = new CharacterClass("", "", new AttributeSet());
            characterclass.toJavaObject(object);
            classes.add(characterclass);
        }
        objects = (DBObject) dbObject.get("races");
        list = (ArrayList<DBObject>) (objects.get("list"));
        for (DBObject object : list) {
            CharacterRace characterRace = new CharacterRace("", "", new AttributeSet());
            characterRace.toJavaObject(object);
            races.add(characterRace);
        }
        Dungeon newDungeon = new Dungeon();
        dungeon = newDungeon;
        dungeon.toJavaObject((DBObject) dbObject.get("dungeon"));
        objects = (DBObject) dbObject.get("playerCharacters");
        list = (ArrayList<DBObject>) (objects.get("map"));
        for (DBObject object : list) {
            Long id = (Long) (object.get("id"));
            DBObject valueObject = (DBObject) (object.get("value"));
            List<DBObject> list2 = ((ArrayList<DBObject>) (valueObject.get("list")));
            List<PlayerCharacter> listCharacters = new ArrayList<>();
            for (DBObject dbObject1 : list2) {
                PlayerCharacter playerCharacter = new PlayerCharacter(new CharacterRace("", "", new AttributeSet()), new CharacterClass("", "", new AttributeSet()));
                playerCharacter.toJavaObject(dbObject1, dungeon);
                listCharacters.add(playerCharacter);
            }
            playerCharacters.put(id, listCharacters);
        }

    }

    /**
     * Makes a DBObject out of the PlayerCharacter-Map
     *
     * @return DBObject
     */
    private BasicDBObject charactersToDB() {
        List<BasicDBObject> objects = new ArrayList<>();
        for (Map.Entry<Long, List<PlayerCharacter>> entry : playerCharacters.entrySet()) {
            BasicDBObject basicDBObject = new BasicDBObject()
                    .append("id", entry.getKey())
                    .append("value", MongoHelper.listToDB(entry.getValue()));
            objects.add(basicDBObject);
        }
        return new BasicDBObject()
                .append("map", objects);
    }
}
