package mudbackend.entity.general;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import mudbackend.database.DBSavable;
import mudbackend.database.MongoHelper;
import mudbackend.entity.characterdesign.AttributeSet;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.dungeondesign.DirectionEnum;
import mudbackend.entity.dungeondesign.GraphElement;
import mudbackend.entity.dungeondesign.NPC;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.events.EventType;
import mudbackend.entity.events.ItemEvent;
import mudbackend.entity.events.RoomEvent;
import mudbackend.entity.events.TextEvent;
import mudbackend.entity.items.*;

import java.util.*;

/**
 * Represents a mud in the game logic. Contains all information about the dungeon structure.
 */
@Getter
public class Dungeon implements DBSavable {
    private Room startRoom;
    private final Map<Room, GraphElement> rooms = new HashMap<>();
    private final List<PlayerCharacter> activeCharacters = new ArrayList<>();
    private final List<Item> itemTemplates = new ArrayList<>();
    private final List<RoomEvent> eventTemplates = new ArrayList<>();
    private final List<NPC> npcTemplates = new ArrayList<>();


    public Dungeon() {
    }

    /**
     * Adds a room to a dungeon.
     *
     * @param room room that will be added to the dungeon
     * @return true if the addition was successful
     */
    public boolean addRoom(Room room) {
        if (this.startRoom == null) {
            this.startRoom = room;
        }
        this.rooms.put(room, null);
        return true;
    }

    /**
     * Deletes a room of a dungeon.
     *
     * @param roomID identifier of the room
     * @return true if the addition was successful
     */
    public boolean deleteRoom(long roomID) {
        // Handles deletion in room map
        for (Map.Entry<Room, GraphElement> entry : this.rooms.entrySet()) {
            if (entry.getValue() != null) {
                entry.setValue(entry.getValue().deletePathway(getRoomByID(roomID)));
            }
        }
        this.rooms.keySet().removeIf(entry -> entry.getId() == roomID);

        // Deletes start room if need be
        if (startRoom.getId() == roomID) {
            if (rooms.isEmpty()) {
                startRoom = null;
            } else {
                startRoom = rooms.entrySet().iterator().next().getKey();  // startRoom = first map entry
            }
        }
        return true;
    }

    /**
     * Returns the room object corresponding to the selected room ID.
     *
     * @param RoomID identifier of the room
     * @return the room identified by the ID
     */
    public Room getRoomByID(long RoomID) {
        if (this.startRoom.getId() == RoomID) {
            return this.startRoom;
        }
        for (Room room : this.rooms.keySet()) {
            if (room.getId() == RoomID) {
                return room;
            }
        }
        return null;
    }

    /**
     * Returns the PlayerCharacter object corresponding to the selected player ID.
     *
     * @param playerID identifier of the player
     * @return the player character identified by the ID
     */
    public PlayerCharacter getPlayerCharacterByID(long playerID) {
        return this.activeCharacters.stream().filter(character -> character.getId() == playerID).findFirst().orElse(null);
    }

    /**
     * Adds an item to a given room.
     *
     * @param roomID identifier of the room
     * @param item   the item that will be added to the selected room
     * @return true if the addition was successful
     */
    public boolean addItem(long roomID, Item item) {
        return this.getRoomByID(roomID).addItem(item);
    }

    /**
     * Adds an NPC to a given room.
     *
     * @param roomID identifier of the room
     * @param npc    NPC that will be added to the room
     * @return true if the addition was successful
     */
    public boolean addNPC(long roomID, NPC npc) {
        return this.getRoomByID(roomID).addNPC(npc);
    }

    /**
     * Adds a room event to a selected room.
     *
     * @param roomID    identifier of the room
     * @param roomEvent event that will be added to the room
     * @return true if the addition was successful
     */
    public boolean addRoomEvent(long roomID, RoomEvent roomEvent) {
        return this.getRoomByID(roomID).addEvent(roomEvent);
    }

    /**
     * Deletes a given item from a given room.
     *
     * @param roomID identifier of the room
     * @param itemID identifier of the item
     * @return true if the removal was successful
     */
    public boolean deleteItem(long roomID, long itemID) {
        return this.getRoomByID(roomID).deleteItem(itemID);
    }

    /**
     * Deletes a given NPC from a given room.
     *
     * @param roomID identifier of the room
     * @param npcID  identifier of the NPC
     * @return true if removal was successful
     */
    public boolean deleteNPC(long roomID, long npcID) {
        return this.getRoomByID(roomID).deleteNPC(npcID);
    }

    /**
     * Deletes an event from a given room
     *
     * @param roomID  identifier of the room
     * @param eventID identifier of the event
     * @return true if the removal was successful
     */
    public boolean deleteRoomEvent(long roomID, long eventID) {
        return this.getRoomByID(roomID).deleteEvent(eventID);
    }

    /**
     * Adds a pathway from one room to another
     *
     * @param from      roomID where the pathway starts
     * @param to        roomID where the pathway ends
     * @param direction direction at which the pathway will be placed relative to the starting room
     * @return true if the addition was successful
     */
    public boolean addPathway(long from, long to, DirectionEnum direction) {
        DirectionEnum oppositeDirection;
        switch (direction) {
            case NORTH:
                oppositeDirection = DirectionEnum.SOUTH;
                break;
            case EAST:
                oppositeDirection = DirectionEnum.WEST;
                break;
            case SOUTH:
                oppositeDirection = DirectionEnum.NORTH;
                break;
            default:
                oppositeDirection = DirectionEnum.EAST;
                break;
        }
        return addOneWay(from, to, direction) && addOneWay(to, from, oppositeDirection);
    }

    /**
     * Helper function adding half of a pathway
     *
     * @param from      room the path originates from
     * @param to        room the path leads to
     * @param direction direction in which the path leaves the 'from' room
     * @return true if the addition was successful
     */
    private boolean addOneWay(long from, long to, DirectionEnum direction) {
        Room toRoom = getRoomByID(to);
        for (Map.Entry<Room, GraphElement> entry : this.rooms.entrySet()) {
            if (entry.getKey().getId() == from) {
                GraphElement currentGraphElement = entry.getValue();
                if (currentGraphElement == null) {
                    entry.setValue(new GraphElement(toRoom, direction));
                } else {
                    entry.getValue().addPathway(toRoom, direction);
                }
            }
        }
        return true;
    }

    /**
     * Deletes Pathway between two rooms
     *
     * @param room1 identifier of the first room
     * @param room2 identifier of the second room
     * @return true if pathway is deleted
     */
    public boolean deletePathway(long room1, long room2) {
        if (this.isConnected(room1, room2)) {
            return (this.deleteOneDirection(room1, room2) && this.deleteOneDirection(room2, room1));
        }
        return false;
    }


    /**
     * Checks if room is connected to another room
     *
     * @param room1ID identifier of the first room
     * @param room2ID identifier of the second room
     * @return true if connected
     */
    public boolean isConnected(long room1ID, long room2ID) {
        Room room1 = this.getRoomByID(room1ID);
        GraphElement room1GraphElement = this.rooms.get(room1);
        if (room1GraphElement == null) {
            return false;
        }
        return room1GraphElement.isConnected(room2ID);
    }

    /**
     * Moves a character from one room to another.
     *
     * @param characterID identifier of the character
     * @param direction   compass direction the character moves in
     * @return room the character was moved to
     */
    public Room moveCharacter(long characterID, DirectionEnum direction) {
        Room startRoom = getRoomWithPlayer(characterID);
        Room endRoom = this.getAdjacentRoom(startRoom.getId(), direction);
        endRoom.getPresentCharacters().add(characterID);
        startRoom.getPresentCharacters().remove(characterID);
        getPlayerCharacterByID(characterID).updateLocation(endRoom);
        return endRoom;
    }

    /**
     * Moves a character from one room to another.
     *
     * @param characterID identifier of the character
     * @param roomID      identifier of the room the character will be moved to
     * @return the player character inside the new room
     */
    public PlayerCharacter placeCharacter(long characterID, long roomID) {
        Room startRoom = getRoomWithPlayer(characterID);
        Room endRoom = getRoomByID(roomID);
        endRoom.getPresentCharacters().add(characterID);
        assert startRoom != null;
        startRoom.getPresentCharacters().remove(characterID);
        getPlayerCharacterByID(characterID).updateLocation(endRoom);
        return getPlayerCharacterByID(characterID);
    }

    /**
     * Adds an item template to the dungeon that can be reused multiple times.
     *
     * @param itemTemplate template that will be added to the respective dungeon list
     */
    public void addItemTemplate(Item itemTemplate) {
        if (itemTemplate.getId() == -1) {
            itemTemplate.setId(UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE);
            itemTemplates.add(itemTemplate);
        } else {
            for (Item template : itemTemplates) {
                if (template.getId() == itemTemplate.getId()) {
                    template.setName(itemTemplate.getName());
                    template.setWeight(itemTemplate.getWeight());
                    template.setAttributes(itemTemplate.getAttributes());
                }
            }
        }
    }

    /**
     * Adds an NPC template to the dungeon that can be reused multiple times.
     *
     * @param npcTemplate template that will be added to the respective dungeon list
     */
    public void addNPCTemplate(NPC npcTemplate) {
        if (npcTemplate.getId() == -1) {
            npcTemplates.add(new NPC(npcTemplate.getName()));
        } else {
            for (NPC template : npcTemplates) {
                if (template.getId() == npcTemplate.getId()) {
                    template.setName(npcTemplate.getName());
                    if (npcTemplate.getActions() != null) template.setActions(npcTemplate.getActions());
                }
            }
        }
    }

    /**
     * Adds a room event template to the dungeon that can be reused multiple times.
     *
     * @param eventTemplate template that will be added to the respective dungeon list
     */
    public void addRoomEventTemplate(RoomEvent eventTemplate) {
        if (eventTemplate.getId() == -1) {
            eventTemplate.setId(UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE);
            eventTemplates.add(eventTemplate);
        } else {
            EventType type = eventTemplate.getEventType();
            for (RoomEvent template : eventTemplates) {
                if (template.getId() == eventTemplate.getId() && type.equals(template.getEventType())) {
                    template.setName(eventTemplate.getName());
                    switch (type) {
                        case ItemEvent:
                            ((ItemEvent) template).setItemResponse(((ItemEvent) eventTemplate).getItemResponse());
                            break;
                        case TextEvent:
                            ((TextEvent) template).setTextResponse(((TextEvent) eventTemplate).getTextResponse());
                            break;
                    }
                }
            }
        }
    }

    /**
     * Deletes a previously added item template.
     *
     * @param itemID identifier of the template that will be removed from the respective dungeon list
     * @return true if the removal was successful
     */
    public boolean deleteItemTemplate(long itemID) {
        return this.itemTemplates.removeIf(itemTemplate -> itemTemplate.getId() == itemID);
    }

    /**
     * Deletes a previously added NPC template.
     *
     * @param npcID identifier of the template that will be removed from the respective dungeon list
     * @return true if the removal was successful
     */
    public boolean deleteNPCTemplate(long npcID) {
        return this.npcTemplates.removeIf(npcTemplate -> npcTemplate.getId() == npcID);
    }

    /**
     * Deletes a previously added room event template.
     *
     * @param eventID identifier of the template that will be removed from the respective dungeon list
     * @return true if the removal was successful
     */
    public boolean deleteRoomEventTemplate(long eventID) {
        return this.eventTemplates.removeIf(eventTemplate -> eventTemplate.getId() == eventID);
    }

    /**
     * Removes an item from a room and adds it to a player's inventory.
     *
     * @param characterID identifier of the player character
     * @param itemName    name of the item
     * @return true if picking up the item was successful
     */
    public boolean pickUpItem(long characterID, String itemName) {
        PlayerCharacter character = getPlayerCharacterByID(characterID);
        Room room = getRoomWithPlayer(characterID);
        assert room != null;
        Item item = room.getItems().stream().filter(itemIter -> itemIter.getName().equals(itemName)).findFirst().orElse(null);
        if (!character.addItem(item)) return false;
        assert item != null;
        return room.deleteItem(item.getId());
    }

    /**
     * Removes an item from a player's inventory and adds it to a room.
     *
     * @param characterID identifier of the player character
     * @param itemID      identifier of the item
     * @return true if discarding the item was successful
     */
    public boolean discardItem(long characterID, long itemID) {
        PlayerCharacter character = getPlayerCharacterByID(characterID);
        Room room = getRoomWithPlayer(characterID);
        assert room != null;
        Item item =
                character.getInventory().stream().filter(itemIter -> itemIter.getId() == itemID).findFirst().orElse(null);
        assert item != null;
        return character.deleteItem(item.getId()) && room.addItem(item);
    }

    /**
     * Function used to determine the room a player is in.
     *
     * @param characterID identifier of the player character
     * @return the room containing the player character
     */
    public Room getRoomWithPlayer(long characterID) {
        return getPlayerCharacterByID(characterID).getLocation();
    }

    /**
     * Starts the Matrix-Generation-Process at the Coordinates of the startroom (8,8)
     *
     * @return the matrix
     */
    public MatrixInformation[][] generateMatrix() {
        MatrixInformation[][] matrix = new MatrixInformation[15][15];
        return fillMatrixCell(7, 7, matrix, startRoom);
    }

    /**
     * Fills a cell of the Dungeonmatrix with the correct information. Then handles all adjacent rooms.
     *
     * @param x      x-Coordinate of the cell
     * @param y      y-Coordinate of the cell
     * @param matrix the current matrix
     * @param room   the room on that cell
     * @return the filled matrix
     */
    private MatrixInformation[][] fillMatrixCell(int x, int y, MatrixInformation[][] matrix, Room room) {
        MatrixInformation information = createMatrixInformation(room);
        matrix[x][y] = information;
        if (information.isNorth() && matrix[x][y - 1] == null) {
            Room neighbor = rooms.get(room).getRoomInDirection(DirectionEnum.NORTH);
            matrix = fillMatrixCell(x, y - 1, matrix, neighbor);
        }
        if (information.isEast() && matrix[x + 1][y] == null) {
            Room neighbor = rooms.get(room).getRoomInDirection(DirectionEnum.EAST);
            matrix = fillMatrixCell(x + 1, y, matrix, neighbor);
        }
        if (information.isSouth() && matrix[x][y + 1] == null) {
            Room neighbor = rooms.get(room).getRoomInDirection(DirectionEnum.SOUTH);
            matrix = fillMatrixCell(x, y + 1, matrix, neighbor);
        }
        if (information.isWest() && matrix[x - 1][y] == null) {
            Room neighbor = rooms.get(room).getRoomInDirection(DirectionEnum.WEST);
            matrix = fillMatrixCell(x - 1, y, matrix, neighbor);
        }
        return matrix;
    }

    /**
     * Creates the Matrixinformation for a room
     *
     * @param room room
     * @return the Matrixinformation
     */
    private MatrixInformation createMatrixInformation(Room room) {
        MatrixInformation matrixInformation = new MatrixInformation(room);
        if (startRoom == room) {
            matrixInformation.setStartRoom(true);
        }
        if (rooms.get(room) != null) {
            return rooms.get(room).createMatrixInformation(matrixInformation);
        }
        return matrixInformation;
    }

    /**
     * Deletes one Direction of a Pathway
     *
     * @param from identifier of the first room
     * @param to   identifier of the second room
     * @return true if pathway was deleted
     */
    private boolean deleteOneDirection(long from, long to) {
        Room toRoom = getRoomByID(to);
        for (Map.Entry<Room, GraphElement> entry : rooms.entrySet()) {
            if (entry.getKey().getId() == from) {
                if (entry.getValue() == null) {
                    return false;
                } else {
                    entry.setValue(entry.getValue().deletePathway(toRoom));
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Rolls back a dungeon. Removes all Character-Information
     */
    public void rollback() {
        activeCharacters.clear();
        for (Room room : this.rooms.keySet()) {
            room.getPresentCharacters().clear();
        }
    }

    @Override
    public DBObject toDBObject() {
        BasicDBObject basicDBObject = new BasicDBObject().append("rooms", MongoHelper.mapToDB(rooms)).append(
                "itemTemplates", MongoHelper.listToDB(itemTemplates)).append("npcTemplates",
                MongoHelper.listToDB(npcTemplates)).append("eventTemplates", MongoHelper.listToDB(eventTemplates));
        if (startRoom != null) {
            basicDBObject.append("startRoom", startRoom.getId());
        }
        return basicDBObject;
    }

    @Override
    public void toJavaObject(DBObject dbObject) {

        DBObject objects = (DBObject) dbObject.get("rooms");
        ArrayList<DBObject> list = (ArrayList<DBObject>) (objects.get("map"));
        for (DBObject object : list) {
            Room room = new Room("", "");
            room.toJavaObject((DBObject) object.get("key"));
            rooms.put(room, null);
        }
        for (Room room : rooms.keySet()) {
            if (room.getId() == (Long) dbObject.get("startRoom")) startRoom = room;
        }
        for (DBObject object : list) {
            GraphElement graphElement = null;
            if (object.get("value") != null) {
                graphElement = new GraphElement(new Room("", ""), DirectionEnum.NORTH);
                graphElement.toJavaObject((DBObject) object.get("value"), this);
            }
            Room room = getRoomByID((Long) ((DBObject) object.get("key")).get("id"));
            rooms.replace(room, graphElement);
        }

        objects = (DBObject) dbObject.get("itemTemplates");
        list = (ArrayList<DBObject>) (objects.get("list"));
        for (DBObject object : list) {
            ItemTypesEnum typesEnum = ItemTypesEnum.valueToEnum((int) object.get("itemType"));
            Item item = null;
            switch (Objects.requireNonNull(typesEnum)) {
                case ARMOUR:
                    item = new Armour("", 0, new AttributeSet());
                    break;
                case WEAPON:
                    item = new Weapon("", 0, new AttributeSet());
                    break;
                case ACCESSOIRE:
                    item = new Accessoire("", 0, new AttributeSet());
                    break;
                case CONSUMABLE:
                    item = new Consumable("", 0, new AttributeSet());
                    break;
            }
            item.toJavaObject(object);
            itemTemplates.add(item);
        }

        objects = (DBObject) dbObject.get("npcTemplates");
        list = (ArrayList<DBObject>) (objects.get("list"));
        for (DBObject object : list) {
            NPC npc = new NPC("");
            npc.toJavaObject(object);
            npcTemplates.add(npc);
        }

        objects = (DBObject) dbObject.get("eventTemplates");
        list = (ArrayList<DBObject>) (objects.get("list"));
        for (DBObject object : list) {
            EventType eventType = EventType.valueToEnum((int) object.get("eventType"));
            RoomEvent roomEvent = null;
            switch (Objects.requireNonNull(eventType)) {
                case TextEvent:
                    roomEvent = new TextEvent("", "");
                    roomEvent.toJavaObject(object);
                    break;
                case ItemEvent:
                    roomEvent = new ItemEvent("", new Weapon("", 0, new AttributeSet()));
                    roomEvent.toJavaObject(object);
                    break;
            }
            eventTemplates.add(roomEvent);
        }
    }

    /**
     * Fetches an NPC identified by the given ID.
     *
     * @param npcID identifier of the npc
     * @return the NPC corresponding to the given ID
     */
    public NPC getNPCbyID(long npcID) {
        for (Room room : this.rooms.keySet()) {
            NPC npc = room.getNPCByID(npcID);
            if (npc != null) {
                return npc;
            }
        }
        return null;
    }

    /**
     * Fetches the adjacent (connected) room in a given direction.
     *
     * @param roomID    identifier of the start room
     * @param direction direction of the pathway
     * @return room the pathway leads to
     */
    public Room getAdjacentRoom(long roomID, DirectionEnum direction) {
        return this.rooms.get(this.getRoomByID(roomID)).getRoomInDirection(direction);
    }

    /**
     * Adds a character to the active characters.
     *
     * @param playerCharacter character that will be added to the active list
     */
    public void joinGame(PlayerCharacter playerCharacter) {

    }

    /**
     * Runs interaction with a Roomevent.
     *
     * @param characterID characterID
     * @param eventName   name of the event
     * @return Response-String if TextEvent, else null
     */
    public RoomEvent interactRoomevent(long characterID, String eventName) {
        Room room = getRoomWithPlayer(characterID);
        for (RoomEvent event : room.getRoomEvents()) {
            if (eventName.equals(event.getName())) {
                switch (event.getEventType()) {
                    case ItemEvent:
                        room.addItem(((ItemEvent) event).getItemResponse());
                        room.getRoomEvents().remove(event);
                        return event;
                    case TextEvent:
                        return event;
                }
            }
        }
        return null;
    }

    /**
     * Checks if a room is connected to the startroom of a dungeon
     *
     * @param roomID identifier of the room
     * @return true if they are connected
     */
    public boolean isConnectedToStartroom(long roomID, List<Long> markedRooms) {
        if (roomID == startRoom.getId()) return true;
        markedRooms.add(roomID);
        Room room = getRoomByID(roomID);
        if (isConnected(roomID, startRoom.getId())) return true;
        if (rooms.get(room) == null) return false;
        return rooms.get(room).isConnectedToStartroom(this, markedRooms);
    }

    /**
     * Returns the Direction of a Connection between 2 rooms
     * @param from roomId of first room
     * @param to roomId of second room
     * @return the direction
     */
    public DirectionEnum getDirectionOfPathway(long from, long to) {
        return rooms.get(getRoomByID(from)).getDirectionOfPathway(to);
    }
}
