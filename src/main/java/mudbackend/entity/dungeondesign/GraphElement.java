package mudbackend.entity.dungeondesign;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;
import mudbackend.entity.general.Dungeon;
import mudbackend.entity.general.MatrixInformation;

import java.util.List;

/**
 * Represents a graph element in the adjacency list of rooms in a dungeon.
 */
@Getter
@Setter
public class GraphElement implements DBSavable {
    private DirectionEnum direction;
    private Room room;
    private GraphElement next;

    /**
     * Constructor.
     *
     * @param to        adjacent room to the current one
     * @param direction compass direction of adjacent room
     */
    public GraphElement(Room to, DirectionEnum direction) {
        this.direction = direction;
        this.room = to;
    }

    /**
     * Adds a path to another room.
     *
     * @param to        adjacent room to the current one
     * @param direction compass direction of adjacent room
     * @return true if the addition was successful
     */
    public boolean addPathway(Room to, DirectionEnum direction) {
        if (this.next == null) {
            this.next = new GraphElement(to, direction);
            return true;
        } else {
            return this.next.addPathway(to, direction);
        }
    }

    /**
     * Deletes the graph element path leading to a room.
     *
     * @param to room the path is leading to
     * @return a graph element where no path leads to the selected room
     */
    public GraphElement deletePathway(Room to) {
        if (room == to) {
            return next;
        }
        if (next != null) {
            next = next.deletePathway(to);
        }
        return this;
    }

    /**
     * Checks if room is connected to another room
     *
     * @param to identifier of the adjacent room
     * @return true if both rooms are connected
     */
    public boolean isConnected(long to) {
        if (room.getId() == to) {
            return true;
        }
        if (next != null) {
            return next.isConnected(to);
        }
        return false;
    }

    /**
     * Generates MatrixInformation for a room
     *
     * @param matrixInformation the current MatrixInformation
     * @return the current MatrixInformation
     */
    public MatrixInformation createMatrixInformation(MatrixInformation matrixInformation) {
        switch (direction) {
            case NORTH:
                matrixInformation.setNorth(true);
                break;

            case EAST:
                matrixInformation.setEast(true);
                break;

            case SOUTH:
                matrixInformation.setSouth(true);
                break;

            case WEST:
                matrixInformation.setWest(true);
                break;
        }
        if (next != null) {
            return next.createMatrixInformation(matrixInformation);
        }
        return matrixInformation;
    }

    /**
     * Returns the room in a specific direction
     *
     * @param d direction
     * @return the room
     */
    public Room getRoomInDirection(DirectionEnum d) {
        if (direction == d) {
            return room;
        }
        if (next == null) return null;
        return next.getRoomInDirection(d);

    }

    @Override
    public DBObject toDBObject() {
        BasicDBObject basicDBObject = new BasicDBObject()
                .append("direction", direction.getValue())
                .append("room", room.getId());
        if (next != null)
            basicDBObject.append("next", next.toDBObject());
        return basicDBObject;
    }

    @Override
    public void toJavaObject(DBObject dbObject) {

    }

    public void toJavaObject(DBObject dbObject, Dungeon dungeon) {
        direction = DirectionEnum.getByValue((int) dbObject.get("direction"));
        for (Room room : dungeon.getRooms().keySet()) {
            if (room.getId() == (Long) dbObject.get("room")) this.room = room;
        }
        if (dbObject.get("next") != null) {
            GraphElement graphElement = new GraphElement(room, direction);
            graphElement.toJavaObject((DBObject) dbObject.get("next"), dungeon);
            next = graphElement;
        }
    }

    /**
     * Checks if a room is connected to the startroom of a dungeon
     *
     * @param dungeon the dungeon
     * @return true if they are connected
     */
    public boolean isConnectedToStartroom(Dungeon dungeon, List<Long> markedRooms) {

        if (!markedRooms.contains(room.getId()) && dungeon.isConnectedToStartroom(room.getId(), markedRooms))
            return true;
        markedRooms.add(room.getId());
        if (next == null) return false;
        return next.isConnectedToStartroom(dungeon, markedRooms);
    }

    /**
     * Returns the Direction of a Connection to another room
     * @param to roomId of the other room
     * @return the direction
     */
    public DirectionEnum getDirectionOfPathway(long to) {
        if (room.getId() == to) return direction;
        if (next == null) return null;
        return next.getDirectionOfPathway(to);
    }
}
