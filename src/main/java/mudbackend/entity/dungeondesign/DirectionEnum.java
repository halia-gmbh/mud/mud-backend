package mudbackend.entity.dungeondesign;

import lombok.Getter;

@Getter
public enum DirectionEnum {
    NORTH(1),
    EAST(2),
    SOUTH(3),
    WEST(4);

    private int value;

    private DirectionEnum(int value) {
        this.value = value;
    }

    /**
     * returns the correct enum
     *
     * @param i value
     * @return the enum
     */
    public static DirectionEnum getByValue(int i) {
        switch (i) {
            case 1:
                return NORTH;
            case 2:
                return EAST;
            case 3:
                return SOUTH;
            case 4:
                return WEST;
            default:
                return null;
        }
    }
}
