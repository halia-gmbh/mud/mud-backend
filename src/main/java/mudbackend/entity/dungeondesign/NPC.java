package mudbackend.entity.dungeondesign;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;
import mudbackend.database.MongoHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class NPC implements DBSavable {
    private String name;
    private List<NPCAction> actions = new ArrayList<>();
    private long id;

    /**
     * Constructor.
     *
     * @param name name of the npc
     */
    public NPC(String name) {
        this.id = UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE;  // generate random unique ID
        this.name = name;

    }

    public NPC(long id, String name, List<NPCAction> actions) {
        this.id = id;
        this.name = name;
        this.actions = actions;
    }

    /**
     * Adds an action which the npc then supports for interaction.
     *
     * @param action   what the player has to write to trigger the npc
     * @param reaction what the npc will respond with
     * @return true if the addition was successful
     */
    public boolean addAction(String action, String reaction) {
        this.actions.add(new NPCAction(action, reaction));
        return true;
    }

    /**
     * Retrieves a specific reaction of the npc.
     *
     * @param action what the player wrote to trigger the npc
     * @return the NPCs response
     */
    public String getReaction(String action) {
        for (NPCAction npcAction : actions) {
            if (action.equals(npcAction.getName())) {
                return npcAction.getResponse();
            }
        }
        return null;
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject()
                .append("id", id)
                .append("name", name)
                .append("actions", MongoHelper.listToDB(actions));
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        id = (long) dbObject.get("id");
        name = (String) dbObject.get("name");

        DBObject objects = (DBObject) dbObject.get("actions");
        List<DBObject> list = (ArrayList<DBObject>) objects.get("list");
        for (DBObject object : list) {
            NPCAction action = new NPCAction("", "");
            action.toJavaObject(object);
            actions.add(action);
        }
    }
}
