package mudbackend.entity.dungeondesign;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;

/**
 * Action of a NPC
 */
@Getter
@Setter
public class NPCAction implements DBSavable {

    private String name;
    private String response;

    /**
     * Constructor
     *
     * @param name     Name of the action
     * @param response Response of the Npc
     */
    public NPCAction(String name, String response) {
        this.name = name;
        this.response = response;
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject()
                .append("name", name)
                .append("response", response);
    }

    @Override
    public void toJavaObject(DBObject dbObject) {

        name = (String) dbObject.get("name");
        response = (String) dbObject.get("response");
    }
}
