package mudbackend.entity.dungeondesign;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;
import mudbackend.database.MongoHelper;
import mudbackend.entity.characterdesign.AttributeSet;
import mudbackend.entity.events.EventType;
import mudbackend.entity.events.ItemEvent;
import mudbackend.entity.events.RoomEvent;
import mudbackend.entity.events.TextEvent;
import mudbackend.entity.items.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
public class Room implements DBSavable {
    private long id;
    private String name;
    private String description;
    private final List<Item> items = new ArrayList<>();
    private final List<NPC> npcs = new ArrayList<>();
    private final List<RoomEvent> roomEvents = new ArrayList<>();
    private final List<Long> presentCharacters = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param name        name of the created room
     * @param description description which will be shown to the player
     */
    public Room(String name, String description) {
        this.id = UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE;  // generate unique ID
        this.name = name;
        this.description = description;
    }

    /**
     * Adds an item to the room.
     *
     * @param item item which will be added
     * @return true if the addition was successful
     */
    public boolean addItem(Item item) {
        if (item.getId() == -1) {
            item.setId(UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE);
            items.add(item);
            return true;
        } else {
            for (Item template : items) {
                if (template.getId() == item.getId()) {
                    template.setName(item.getName());
                    template.setWeight(item.getWeight());
                    template.setAttributes(item.getAttributes());
                    return true;
                }
            }
        }
        items.add(item);
        return true;
    }

    /**
     * Adds an NPC to the room.
     *
     * @param npc npc which will be added
     * @return true if item was added successfully, otherwise false
     */
    public boolean addNPC(NPC npc) {
        if (npc.getId() == -1) {
            npc.setId(UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE);
            npcs.add(npc);
            return true;
        } else {
            for (NPC current : npcs) {
                if (current.getId() == npc.getId()) {
                    current.setName(npc.getName());
                    if (npc.getActions() != null) current.setActions(npc.getActions());
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Adds an event to the room.
     *
     * @param event event which will be added
     * @return true if the addition was successful
     */
    public boolean addEvent(RoomEvent event) {
        if (event.getId() == -1) {
            event.setId(UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE);
            roomEvents.add(event);
            return true;
        } else {
            EventType type = event.getEventType();
            for (RoomEvent template : roomEvents) {
                if (template.getId() == event.getId() && type.equals(template.getEventType())) {
                    template.setName(event.getName());
                    switch (type) {
                        case ItemEvent:
                            ((ItemEvent) template).setItemResponse(((ItemEvent) event).getItemResponse());
                            break;
                        case TextEvent:
                            ((TextEvent) template).setTextResponse(((TextEvent) event).getTextResponse());
                            break;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Removes an item from the room.
     *
     * @param itemID identifier of the item
     * @return true if the removal was successful
     */
    public boolean deleteItem(long itemID) {
        return this.items.removeIf(item -> item.getId() == itemID);
    }

    /**
     * Removes an NPC from the room.
     *
     * @param npcID identifier of the NPC
     * @return true if the removal was successful
     */
    public boolean deleteNPC(long npcID) {
        return this.npcs.removeIf(npc -> npc.getId() == npcID);
    }

    /**
     * Removes an event from the room.
     *
     * @param eventID identifier of the event
     * @return true if the removal was successful
     */
    public boolean deleteEvent(long eventID) {
        return this.roomEvents.removeIf(event -> event.getId() == eventID);
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject()
                .append("id", id)
                .append("name", name)
                .append("description", description)
                .append("items", MongoHelper.listToDB(items))
                .append("npcs", MongoHelper.listToDB(npcs))
                .append("roomEvents", MongoHelper.listToDB(roomEvents));
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        id = (long) dbObject.get("id");
        name = (String) dbObject.get("name");
        description = (String) dbObject.get("description");

        DBObject objects = (DBObject) dbObject.get("items");
        List<DBObject> list = (ArrayList<DBObject>) (objects.get("list"));
        for (DBObject object : list) {
            ItemTypesEnum typesEnum = ItemTypesEnum.valueToEnum((int) object.get("itemType"));
            Item item = null;
            switch (Objects.requireNonNull(typesEnum)) {
                case ARMOUR:
                    item = new Armour("", 0, new AttributeSet());
                    break;
                case WEAPON:
                    item = new Weapon("", 0, new AttributeSet());
                    break;
                case ACCESSOIRE:
                    item = new Accessoire("", 0, new AttributeSet());
                    break;
                case CONSUMABLE:
                    item = new Consumable("", 0, new AttributeSet());
                    break;
            }
            item.toJavaObject(object);
            items.add(item);
        }

        objects = (DBObject) dbObject.get("npcs");
        list = (ArrayList<DBObject>) (objects.get("list"));
        for (DBObject object : list) {
            NPC npc = new NPC("");
            npc.toJavaObject(object);
            npcs.add(npc);
        }

        objects = (DBObject) dbObject.get("roomEvents");
        list = (ArrayList<DBObject>) (objects.get("list"));
        for (DBObject object : list) {
            EventType eventType = EventType.valueToEnum((int) object.get("eventType"));
            RoomEvent roomEvent = null;
            switch (Objects.requireNonNull(eventType)) {
                case TextEvent:
                    roomEvent = new TextEvent("", "");
                    roomEvent.toJavaObject(object);
                    break;
                case ItemEvent:
                    roomEvent = new ItemEvent("", new Weapon("", 0, new AttributeSet()));
                    roomEvent.toJavaObject(object);
                    break;
            }
            roomEvents.add(roomEvent);
        }

    }

    /**
     * Fetches an NPC identified by the given ID.
     *
     * @param npcID identifier of the npc
     * @return the NPC corresponding to the given ID
     */
    public NPC getNPCByID(long npcID) {
        return this.npcs.stream().filter(npc -> npc.getId() == npcID).findFirst().orElse(null);
    }
}
