package mudbackend.entity.items;

import lombok.Getter;
import lombok.Setter;
import mudbackend.entity.characterdesign.AttributeSet;

@Setter
@Getter
public class ItemForm {
    private ItemTypesEnum itemType;
    private long id;
    private String name;
    private int weight;
    private AttributeSet attributes;

    /**
     * Converts an item form received from the front end into an item object usable by the back end.
     *
     * @return either a consumable, armour, weapon or accessoire depending on the item type
     */
    public Item toItem() {
        Item item;
        switch (itemType) {
            case CONSUMABLE:
                item = new Consumable(this.name, this.weight, this.attributes);
                item.setId(id);
                break;
            case ARMOUR:
                item = new Armour(this.name, this.weight, this.attributes);
                item.setId(id);
                break;
            case WEAPON:
                item = new Weapon(this.name, this.weight, this.attributes);
                item.setId(id);
                break;
            case ACCESSOIRE:
                item = new Accessoire(this.name, this.weight, this.attributes);
                item.setId(id);
                break;
            default:
                return null;
        }
        return item;
    }
}
