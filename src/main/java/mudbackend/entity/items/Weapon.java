package mudbackend.entity.items;

import mudbackend.entity.characterdesign.AttributeSet;

public class Weapon extends Equipment {

    /**
     * Constructor.
     *
     * @param name       name of the item
     * @param weight     weight of the item
     * @param attributes attribute set containing all the characteristics of an item
     */
    public Weapon(String name, int weight, AttributeSet attributes) {
        super(name, weight, attributes);
        setItemType(ItemTypesEnum.WEAPON);
    }
}
