package mudbackend.entity.items;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;
import mudbackend.entity.characterdesign.AttributeSet;

@Getter
@Setter
public abstract class Item implements DBSavable {
    protected long id;
    protected String name;
    protected int weight;
    protected AttributeSet attributes;
    protected ItemTypesEnum itemType;

    /**
     * Constructor.
     *
     * @param name       name of the item
     * @param weight     weight of the item
     * @param attributes attribute set containing all the characteristics of an item
     */
    public Item(String name, int weight, AttributeSet attributes) {
        this.name = name;
        this.weight = weight;
        this.attributes = attributes;
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject()
                .append("id", id)
                .append("name", name)
                .append("weight", weight)
                .append("attributes", attributes.toDBObject())
                .append("itemType", itemType.getValue());
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        id = (long) dbObject.get("id");
        name = (String) dbObject.get("name");
        weight = (int) dbObject.get("weight");
        itemType = ItemTypesEnum.valueToEnum((int) dbObject.get("itemType"));
        attributes = new AttributeSet();
        attributes.toJavaObject((DBObject) dbObject.get("attributes"));
    }
}
