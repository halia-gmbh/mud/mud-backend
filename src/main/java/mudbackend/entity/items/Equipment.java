package mudbackend.entity.items;

import mudbackend.entity.characterdesign.AttributeSet;

public abstract class Equipment extends Item {

    /**
     * Constructor.
     *
     * @param name       name of the item
     * @param weight     weight of the item
     * @param attributes attribute set containing all the characteristics of an item
     */
    public Equipment(String name, int weight, AttributeSet attributes) {
        super(name, weight, attributes);
    }
}
