package mudbackend.entity.items;

import mudbackend.entity.characterdesign.AttributeSet;
import mudbackend.entity.characterdesign.PlayerCharacter;

public class Consumable extends Item {


    /**
     * Constructor.
     *
     * @param name       name of the item
     * @param weight     weight of the item
     * @param attributes attribute set containing all the characteristics of an item
     */
    public Consumable(String name, int weight, AttributeSet attributes) {
        super(name, weight, attributes);
        setItemType(ItemTypesEnum.CONSUMABLE);
    }

    /**
     * Method for consuming a Consumable. Adds AttributeSet of Consumable to Player-Attributes
     *
     * @param character the Consumer
     * @return true
     */
    public boolean useConsumable(PlayerCharacter character) {
        character.getAttributes().addAttributeSet(attributes);
        return true;
    }
}
