package mudbackend.entity.items;

import lombok.Getter;

@Getter
public enum ItemTypesEnum {
    CONSUMABLE(0),
    ARMOUR(1),
    WEAPON(2),
    ACCESSOIRE(3);

    private final int value;

    private ItemTypesEnum(int value) {
        this.value = value;
    }

    /**
     * Returns the Enum connected to a value
     *
     * @param i the value
     * @return the Enum
     */
    public static ItemTypesEnum valueToEnum(int i) {
        switch (i) {
            case 0:
                return CONSUMABLE;
            case 1:
                return ARMOUR;
            case 2:
                return WEAPON;
            case 3:
                return ACCESSOIRE;
            default:
                return null;
        }
    }
}