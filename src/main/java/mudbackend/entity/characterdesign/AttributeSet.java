package mudbackend.entity.characterdesign;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import mudbackend.database.DBSavable;

import java.util.ArrayList;
import java.util.List;

@Getter
public class AttributeSet implements DBSavable {
    private final List<Attribute> attributes = new ArrayList<>();

    /**
     * Adds an attribute to the set.
     *
     * @param attribute identifier of the attribute
     * @param value     value of the attribute
     */
    public void addAttribute(String attribute, int value) {
        if (containsAttribute(attribute)) {
            int currentValue = getAttribute(attribute);
            setAttribute(attribute, currentValue + value);
        } else {
            attributes.add(new Attribute(attribute, value));
        }
    }


    /**
     * Adds another attributeSet to the Set
     *
     * @param attributeSet the other AttributeSet
     */
    public void addAttributeSet(AttributeSet attributeSet) {
        for (Attribute attribute : attributeSet.getAttributes()) {
            addAttribute(attribute.getName(), attribute.getValue());
        }
    }

    /**
     * Changes the value of the given attribute in the set.
     *
     * @param attribute identifier of the attribute
     * @param value     value of the attribute
     */
    public void setAttribute(String attribute, int value) {
        for (Attribute a : attributes) {
            if (a.getName().equals(attribute)) {
                a.setValue(value);
            }
        }
    }

    /**
     * Retrieves the value of the given attribute.
     *
     * @param attribute identifier of the attribute
     * @return value of the attribute
     */
    public int getAttribute(String attribute) {
        for (Attribute a : attributes) {
            if (a.getName().equals(attribute)) {
                return a.getValue();
            }
        }
        return 0;
    }

    /**
     * Deletes the given attribute from the set.
     *
     * @param attribute identifier of the attribute
     * @return true if the item was deleted successfully
     */
    public boolean deleteAttribute(String attribute) {
        for (Attribute a : attributes) {
            if (a.getName().equals(attribute)) {
                attributes.remove(a);
                return true;
            }
        }
        return false;
    }

    /**
     * Subtracts a value from an Attribute. If the value is 0 afterwards the Attribute is removed.
     *
     * @param attribute Attribute-Name
     * @param value     value
     * @return true if attribute was found
     */
    private boolean minusAttribute(String attribute, int value) {
        for (Attribute a : attributes) {
            if (a.getName().equals(attribute)) {
                a.setValue(a.getValue() - value);
                if (a.getValue() <= 0) attributes.remove(a);
                return true;
            }
        }
        return false;
    }

    /**
     * Subtracts values of an AttributeSet from the set
     *
     * @param attributeSet the subtracted AttributeSet
     */
    public void minusAttributeSet(AttributeSet attributeSet) {
        for (Attribute attribute : attributeSet.getAttributes()) {
            minusAttribute(attribute.getName(), attribute.getValue());
        }
    }

    @Override
    public DBObject toDBObject() {
        List<DBObject> objects = new ArrayList<>();
        for (Attribute attribute : attributes) {
            objects.add(attribute.toDBObject());
        }
        return new BasicDBObject().append("attributes", objects);
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        if (dbObject.get("attributes") != null) {
            ArrayList<DBObject> list = (ArrayList<DBObject>) (dbObject.get("attributes"));
            for (DBObject object : list) {
                Attribute attribute = new Attribute("", 0);
                attribute.toJavaObject(object);
                attributes.add(attribute);
            }
        }
    }


    /**
     * Checks if AttributeSet contains a specific attribute
     *
     * @param attributeName name of the attribute
     * @return true if set contains the attribute
     */
    public boolean containsAttribute(String attributeName) {
        for (Attribute attribute : attributes) {
            if (attribute.getName().equals(attributeName)) {
                return true;
            }
        }
        return false;
    }
}
