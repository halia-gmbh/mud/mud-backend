package mudbackend.entity.characterdesign;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;

@Getter
@Setter
public class Attribute implements DBSavable {

    private String name;
    private int value;

    public Attribute(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject()
                .append("name", name)
                .append("value", value);
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        name = (String) dbObject.get("name");
        value = (int) dbObject.get("value");
    }
}
