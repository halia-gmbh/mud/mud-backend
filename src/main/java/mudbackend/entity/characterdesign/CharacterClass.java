package mudbackend.entity.characterdesign;

import mudbackend.database.DBSavable;

public class CharacterClass extends CharacterOption implements DBSavable {

    /**
     * Constructor.
     *
     * @param name        name of the class
     * @param description brief description of the class
     * @param attributes  attributes which belong to the class
     */
    public CharacterClass(String name, String description, AttributeSet attributes) {
        super(name, description, attributes);
    }
}
