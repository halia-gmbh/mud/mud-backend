package mudbackend.entity.characterdesign;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.general.Dungeon;
import mudbackend.entity.items.*;

import java.util.*;

@Getter
@Setter
public class PlayerCharacter implements DBSavable {
    private long id;
    private String name;
    private boolean active;
    private AttributeSet attributes = new AttributeSet();
    private Weapon weapon;
    private Armour armour;
    private Accessoire accessoire;
    private final List<Item> inventory = new ArrayList<>();
    private CharacterRace characterRace;
    private CharacterClass characterClass;
    private Room location;

    /**
     * Constructor.
     *
     * @param characterRace  determines the race of the created character
     * @param characterClass determines the class of the created character
     */
    public PlayerCharacter(CharacterRace characterRace, CharacterClass characterClass) {
        this.characterRace = characterRace;
        this.characterClass = characterClass;
        attributes.addAttributeSet(characterRace.getAttributes());
        attributes.addAttributeSet(characterClass.getAttributes());
    }

    public void setName(String name) {
        this.name = name;
    }

    public void startAttributes() {
        attributes.addAttributeSet(characterClass.getAttributes());
        attributes.addAttributeSet(characterRace.getAttributes());
    }

    @Override
    public DBObject toDBObject() {
        List<DBObject> dbInventory = new ArrayList<>();
        for (Item item : inventory) {
            dbInventory.add(item.toDBObject());
        }
        BasicDBObject basicDBObject = new BasicDBObject();
        basicDBObject.append("id", id);
        basicDBObject.append("name", name);
        basicDBObject.append("attributes", attributes.toDBObject());
        if (weapon != null)
            basicDBObject.append("weapon", weapon.toDBObject());
        if (armour != null)
            basicDBObject.append("armour", armour.toDBObject());
        if (accessoire != null)
            basicDBObject.append("accessoire", accessoire.toDBObject());
        basicDBObject.append("inventory", dbInventory);
        basicDBObject.append("characterRace", characterRace.toDBObject());
        basicDBObject.append("characterClass", characterClass.toDBObject());
        if (location != null)
            basicDBObject.append("location", location.getId());
        else
            basicDBObject.append("location", null);
        return basicDBObject;
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
    }

    public void setId(long id) {
        this.id = id;
    }

    /**
     * Adds an item to a player's inventory.
     *
     * @param item item that will be added
     * @return true if the addition was successful
     */
    public boolean addItem(Item item) {
        if (weapon != null && weapon.getId() == item.getId()) {
            attributes.minusAttributeSet(weapon.getAttributes());
            weapon.setAttributes(item.getAttributes());
            weapon.setWeight(item.getWeight());
            weapon.setName(item.getName());
            attributes.addAttributeSet(weapon.getAttributes());
            return true;
        }
        if (armour != null && armour.getId() == item.getId()) {
            attributes.minusAttributeSet(armour.getAttributes());
            armour.setAttributes(item.getAttributes());
            armour.setWeight(item.getWeight());
            armour.setName(item.getName());
            attributes.addAttributeSet(armour.getAttributes());
            return true;
        }
        if (accessoire != null && accessoire.getId() == item.getId()) {
            attributes.minusAttributeSet(accessoire.getAttributes());
            accessoire.setAttributes(item.getAttributes());
            accessoire.setWeight(item.getWeight());
            accessoire.setName(item.getName());
            attributes.addAttributeSet(accessoire.getAttributes());
            return true;
        }
        if (item.getId() == -1) {
            item.setId(UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE);
            inventory.add(item);
            return true;
        }
        for (Item inventoryItem : inventory) {
            if (item.getId() == inventoryItem.getId() && item.getItemType().equals(inventoryItem.getItemType())) {
                inventoryItem.setAttributes(item.getAttributes());
                inventoryItem.setWeight(item.getWeight());
                inventoryItem.setName(item.getName());
                return true;
            }
        }
        return this.inventory.add(item);
    }

    /**
     * Removes an item from a player's inventory.
     *
     * @param itemID identifier of the item that will be removed
     * @return true if the removal was successful
     */
    public boolean deleteItem(long itemID) {
        if (weapon != null && weapon.getId() == itemID) {
            attributes.minusAttributeSet(weapon.getAttributes());
            weapon = null;
            return true;
        }
        if (armour != null && armour.getId() == itemID) {
            attributes.minusAttributeSet(armour.getAttributes());
            armour = null;
            return true;
        }
        if (accessoire != null && accessoire.getId() == itemID) {
            attributes.minusAttributeSet(accessoire.getAttributes());
            accessoire = null;
            return true;
        }
        for (Item item : inventory) {
            if (item.getId() == itemID) {
                inventory.remove(item);
                return true;
            }
        }
        return false;
    }

    /**
     * Uses an item: Either consumes it, or equips it.
     *
     * @param itemID identifier of the item that will be used
     * @return true if the item was successfully equipped or consumed
     */
    public boolean useItem(long itemID) {
        Item item = this.inventory.stream().filter(itemIter -> itemIter.getId() == itemID).findFirst().orElse(null);
        if (item instanceof Weapon) {
            return equipWeapon(itemID);
        } else if (item instanceof Armour) {
            return equipArmour(itemID);
        } else if (item instanceof Accessoire) {
            return equipAccessoire(itemID);
        } else if (item instanceof Consumable) {
            ((Consumable) item).useConsumable(this);
            return deleteItem(itemID);
        } else {
            return false;
        }
    }

    /**
     * Unequips the selected item and places it in the player's inventory.
     *
     * @param itemID identifier of the item that will be unequipped
     * @return true if the item was successfully unequipped
     */
    public boolean unequipItem(long itemID) {
        if (this.weapon != null && this.weapon.getId() == itemID) {
            this.attributes.minusAttributeSet(this.weapon.getAttributes());
            this.getInventory().add(this.weapon);
            this.weapon = null;
            return true;
        } else if (this.armour != null && this.armour.getId() == itemID) {
            this.attributes.minusAttributeSet(this.armour.getAttributes());
            this.getInventory().add(this.armour);
            this.armour = null;
            return true;
        } else if (this.accessoire != null && this.accessoire.getId() == itemID) {
            this.attributes.minusAttributeSet(this.accessoire.getAttributes());
            this.getInventory().add(this.accessoire);
            this.accessoire = null;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Searches a player's inventory for a weapon and equips it if found, removing the weapon from the inventory in
     * the process.
     *
     * @param weaponID identifier of the weapon that will be equipped
     * @return true if the weapon was found and the equipping was successful
     */
    public boolean equipWeapon(long weaponID) {
        final Iterator<Item> itemIt = this.inventory.iterator();
        while (itemIt.hasNext()) {
            Item item = itemIt.next();
            if (item.getId() == weaponID) {
                if (weapon != null) {
                    addItem(weapon);
                    attributes.minusAttributeSet(weapon.getAttributes());
                    weapon = null;
                }
                this.weapon = (Weapon) item;
                itemIt.remove();
                attributes.addAttributeSet(weapon.getAttributes());
                return true;
            }
        }
        return false;
    }

    /**
     * Searches a player's inventory for a armour and equips it if found, removing the armour from the inventory in
     * the process.
     *
     * @param armourID identifier of the armour that will be equipped
     * @return true if the armour was found and the equipping was successful
     */
    public boolean equipArmour(long armourID) {
        final Iterator<Item> itemIt = this.inventory.iterator();
        while (itemIt.hasNext()) {
            Item item = itemIt.next();
            if (item.getId() == armourID) {
                if (armour != null) {
                    addItem(armour);
                    attributes.minusAttributeSet(armour.getAttributes());
                    armour = null;
                }
                this.armour = (Armour) item;
                itemIt.remove();
                attributes.addAttributeSet(armour.getAttributes());
                return true;
            }
        }
        return false;
    }

    /**
     * Searches a player's inventory for a accessoire and equips it if found, removing the accessoire from the
     * inventory in the process.
     *
     * @param accessoireID identifier of the accessoire that will be equipped
     * @return true if the accessoire was found and the equipping was successful
     */
    public boolean equipAccessoire(long accessoireID) {
        final Iterator<Item> itemIt = this.inventory.iterator();
        while (itemIt.hasNext()) {
            Item item = itemIt.next();
            if (item.getId() == accessoireID) {
                if (accessoire != null) {
                    addItem(accessoire);
                    attributes.minusAttributeSet(accessoire.getAttributes());
                    accessoire = null;
                }
                this.accessoire = (Accessoire) item;
                itemIt.remove();
                attributes.addAttributeSet(accessoire.getAttributes());
                return true;
            }
        }
        return false;
    }

    /**
     * Updates the location of the player to a new room.
     *
     * @param room the player's new location
     */
    public void updateLocation(Room room) {
        this.location = room;
    }

    public void toJavaObject(DBObject dbObject, Dungeon dungeon) {
        id = (long) dbObject.get("id");
        name = (String) dbObject.get("name");
        active = false;
        DBObject weaponObject = (DBObject) dbObject.get("weapon");
        if (weaponObject != null) {
            weapon = new Weapon("", 0, new AttributeSet());
            weapon.toJavaObject(weaponObject);
        }
        DBObject armourObject = (DBObject) dbObject.get("armour");
        if (armourObject != null) {
            armour = new Armour("", 0, new AttributeSet());
            armour.toJavaObject(armourObject);
        }
        DBObject accessoiresObject = (DBObject) dbObject.get("weapon");
        if (weaponObject != null) {
            accessoire = new Accessoire("", 0, new AttributeSet());
            accessoire.toJavaObject(accessoiresObject);
        }

        attributes = new AttributeSet();
        if (dbObject.get("attributes") != null)
            attributes.toJavaObject((DBObject) dbObject.get("attributes"));

        ArrayList<DBObject> list = (ArrayList<DBObject>) (dbObject.get("inventory"));
        for (DBObject object : list) {
            ItemTypesEnum typesEnum = ItemTypesEnum.valueToEnum((int) object.get("itemType"));
            Item item = null;
            switch (Objects.requireNonNull(typesEnum)) {
                case ARMOUR:
                    item = new Armour("", 0, new AttributeSet());
                    break;
                case WEAPON:
                    item = new Weapon("", 0, new AttributeSet());
                    break;
                case ACCESSOIRE:
                    item = new Accessoire("", 0, new AttributeSet());
                    break;
                case CONSUMABLE:
                    item = new Consumable("", 0, new AttributeSet());
                    break;
            }
            item.toJavaObject(object);
            inventory.add(item);
        }

        characterRace = new CharacterRace("", "", new AttributeSet());
        characterRace.toJavaObject((DBObject) dbObject.get("characterRace"));

        characterClass = new CharacterClass("", "", new AttributeSet());
        characterClass.toJavaObject((DBObject) dbObject.get("characterClass"));

        Object locationID = dbObject.get("location");
        if (locationID != null)
            location = dungeon.getRoomByID((long) locationID);
        else
            location = null;
    }

    /**
     * Sets an attribute of a player to a specific value
     *
     * @param index     index of the changed Attribute. -1 if new Attribute
     * @param attribute the attribute
     * @param value     the value
     * @return this PlayerCharacter
     */
    public PlayerCharacter setPlayerAttribute(int index, String attribute, int value) {
        if (index == -1) {
            attributes.addAttribute(attribute, value);
            return this;
        }
        Attribute a = attributes.getAttributes().get(index);
        if (a != null) {
            a.setValue(value);
            a.setName(attribute);
        }
        return this;
    }

    /**
     * Deletes an Attribute from a Characters AttributeSet
     *
     * @param attribute Attribute-Name
     * @return the Character
     */
    public PlayerCharacter deletePlayerAttribute(String attribute) {
        attributes.deleteAttribute(attribute);
        if (weapon != null && weapon.getAttributes().containsAttribute(attribute)) {
            attributes.addAttribute(attribute, weapon.getAttributes().getAttribute(attribute));
        }
        if (armour != null && armour.getAttributes().containsAttribute(attribute)) {
            attributes.addAttribute(attribute, armour.getAttributes().getAttribute(attribute));
        }
        if (accessoire != null && accessoire.getAttributes().containsAttribute(attribute)) {
            attributes.addAttribute(attribute, accessoire.getAttributes().getAttribute(attribute));
        }
        return this;
    }
}
