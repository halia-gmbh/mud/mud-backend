package mudbackend.entity.characterdesign;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import lombok.Setter;
import mudbackend.database.DBSavable;

import java.util.UUID;

@Getter
@Setter
public abstract class CharacterOption implements DBSavable {

    protected long id;
    protected String name;
    protected AttributeSet attributes;
    protected String description;

    /**
     * Constructor.
     *
     * @param name        name of the option
     * @param description description of the option
     * @param attributes  attribute set containing all the characteristics of an item
     */
    public CharacterOption(String name, String description, AttributeSet attributes) {
        this.id = UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE;  // generate unique ID
        this.name = name;
        this.description = description;
        this.attributes = attributes;
    }

    public long createID() {
        this.id = UUID.randomUUID().getMostSignificantBits() & Integer.MAX_VALUE;
        return id;
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject()
                .append("id", id)
                .append("name", name)
                .append("description", description)
                .append("attributes", attributes.toDBObject());
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        id = (long) dbObject.get("id");
        name = (String) dbObject.get("name");
        description = (String) dbObject.get("description");
        attributes = new AttributeSet();
        attributes.toJavaObject((DBObject) dbObject.get("attributes"));
    }
}
