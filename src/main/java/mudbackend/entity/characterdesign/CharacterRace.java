package mudbackend.entity.characterdesign;

import mudbackend.database.DBSavable;

public class CharacterRace extends CharacterOption implements DBSavable {

    /**
     * Constructor.
     *
     * @param name        name of the race
     * @param description brief description of the race
     * @param attributes  attributes which belong to the race
     */
    public CharacterRace(String name, String description, AttributeSet attributes) {
        super(name, description, attributes);
    }
}
