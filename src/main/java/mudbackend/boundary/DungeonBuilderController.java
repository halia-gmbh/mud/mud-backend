package mudbackend.boundary;

import mudbackend.accountservice.AuthenticationHelper;
import mudbackend.entity.characterdesign.CharacterClass;
import mudbackend.entity.characterdesign.CharacterRace;
import mudbackend.entity.dungeondesign.DirectionEnum;
import mudbackend.entity.dungeondesign.NPC;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.events.Eventform;
import mudbackend.entity.general.Game;
import mudbackend.entity.general.MatrixInformation;
import mudbackend.entity.items.ItemForm;
import mudbackend.services.DungeonBuilderService;
import mudbackend.services.DungeonMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DungeonBuilderController {

    private final DungeonBuilderService dungeonBuilderService;
    private final DungeonMasterService dungeonMasterService;
    private final AuthenticationHelper authenticationHelper;

    /**
     * Constructor.
     *
     * @param dungeonBuilderService service connecting to the front end
     * @param dungeonMasterService
     */
    @Autowired
    public DungeonBuilderController(DungeonBuilderService dungeonBuilderService,
                                    DungeonMasterService dungeonMasterService,
                                    AuthenticationHelper authenticationHelper) {
        this.dungeonBuilderService = dungeonBuilderService;
        this.dungeonMasterService = dungeonMasterService;
        this.authenticationHelper = authenticationHelper;
    }

    /**
     * Creates a new Game and adds id to dungeonsWIP
     *
     * @param name       name of the Game
     * @param maxPlayers max Amount of Players
     * @param builderID  userID of the builder
     * @return the game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/createGame/name/maxPlayers/builderID/{name}/{maxPlayers}/{builderID}")
    public long createGame(@RequestHeader(value = "authorization", required = false) String auth,
                           @PathVariable String name, @PathVariable int maxPlayers, @PathVariable int builderID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.createGame(name, maxPlayers, builderID).getId();
    }

    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/alterDungeon/game/{gameID}")
    public long alterGame(@RequestHeader(value = "authorization", required = false) String auth,
                          @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.alterGame(gameID);
    }

    /**
     * Returns a game by ID
     *
     * @param gameId gameID
     * @return game
     */
    @CrossOrigin
    @GetMapping(value = "/dungeonBuilder/getGame/{gameId}")
    public Game getGame(@RequestHeader(value = "authorization", required = false) String auth,
                        @PathVariable long gameId) {
        authenticationHelper.doFilterInternal(auth);
        return getGame(gameId);
    }

    /**
     * Adds a room to the dungeon.
     *
     * @param gameID      identifier of the game that will be modified
     * @param name        name of the room
     * @param description description of the room
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addRoom/game/roomName/roomDescription/{gameID}/{name}/{description}")
    public Room addRoom(@RequestHeader(value = "authorization", required = false) String auth,
                        @PathVariable long gameID, @PathVariable String name, @PathVariable String description) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getRoomService().addRoom(getGame(gameID), name, description);
    }

    /**
     * Adds an item template to the dungeon that can be reused multiple times.
     *
     * @param gameID   identifier of the game that will be modified
     * @param itemForm item in specialised front end class
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addItemTemplate/game/{gameID}")
    public Game addItemTemplate(@RequestHeader(value = "authorization", required = false) String auth,
                                @PathVariable long gameID, @RequestBody ItemForm itemForm) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getItemService().addItemTemplate(getGame(gameID), itemForm);
    }

    /**
     * Adds an item to a given room.
     *
     * @param gameID   identifier of the game that will be modified
     * @param roomID   identifier of the room
     * @param itemForm item in specialised front end class
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addItem/game/room/{gameID}/{roomID}")
    public Game addItem(@RequestHeader(value = "authorization", required = false) String auth,
                        @PathVariable long gameID, @PathVariable long roomID, @RequestBody ItemForm itemForm) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getItemService().addItem(getGame(gameID), roomID, itemForm);
    }

    /**
     * Adds a room event to a room in a dungeon.
     *
     * @param gameID    identifier of the game that will be modified
     * @param roomID    identifier of the room in which the room event will be set
     * @param roomEvent event that will be added
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addRoomEvent/game/room/{gameID}/{roomID}")
    public Game addRoomEvent(@RequestHeader(value = "authorization", required = false) String auth,
                             @PathVariable long gameID, @PathVariable long roomID, @RequestBody Eventform roomEvent) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getRoomEventService().addRoomEvent(getGame(gameID), roomID, roomEvent.toEvent());
    }

    /**
     * Adds an NPC template to a dungeon.
     *
     * @param gameID identifier of the game that will be modified
     * @param npc    npc that will be added
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/createNPCTemplate/game/{gameID}")
    public Game createNPCTemplate(@RequestHeader(value = "authorization", required = false) String auth,
                                  @PathVariable long gameID, @RequestBody NPC npc) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getNpcService().addNPCTemplate(getGame(gameID), npc);
    }

    /**
     * Adds an NPC to a dungeon.
     *
     * @param gameID identifier of the game that will be modified
     * @param roomID identifier of the room in which the npc will be put
     * @param npc    npc that will be added
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addNPC/game/room/{gameID}/{roomID}")
    public Game addNPC(@RequestHeader(value = "authorization", required = false) String auth,
                       @PathVariable long gameID, @PathVariable long roomID, @RequestBody NPC npc) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getNpcService().addNPC(getGame(gameID), roomID, npc);
    }

    /**
     * Adds a template of a room event to a dungeon.
     *
     * @param gameID    identifier of the game that will be modified
     * @param roomEvent event that will be added
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addRoomEventTemplate/game/{gameID}")
    public Game addRoomEventTemplate(@RequestHeader(value = "authorization", required = false) String auth,
                                     @PathVariable long gameID, @RequestBody Eventform roomEvent) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getRoomEventService().addRoomEventTemplate(getGame(gameID), roomEvent.toEvent());
    }

    /**
     * Adds a pathway between two rooms in a dungeon.
     *
     * @param gameID    identifier of the game that will be modified
     * @param from      identifier of the first room
     * @param to        identifier of the second room
     * @param direction compass direction through the rooms will be connected
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addPathway/game/from/to/direction/{gameID}/{from}/{to}/{direction}")
    public MatrixInformation[][] addPathway(@RequestHeader(value = "authorization", required = false) String auth,
                                            @PathVariable long gameID, @PathVariable long from, @PathVariable long to
            , @PathVariable int direction) {
        authenticationHelper.doFilterInternal(auth);
        dungeonBuilderService.getRoomService().addPathway(getGame(gameID), from, to,
                DirectionEnum.getByValue(direction));
        return dungeonBuilderService.getDungeonMatrix(gameID);
    }

    /**
     * Deletes the path between two rooms in a dungeon.
     *
     * @param gameID identifier of the game that will be modified
     * @param from   identifier of the first room
     * @param to     identifier of the second room
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removePathway/game/from/to/{gameID}/{from}/{to}")
    public MatrixInformation[][] removePathway(@RequestHeader(value = "authorization", required = false) String auth,
                                               @PathVariable long gameID, @PathVariable long from,
                                               @PathVariable long to) {
        authenticationHelper.doFilterInternal(auth);
        if (dungeonBuilderService.getRoomService().deletePathway(getGame(gameID), from, to) != null) {
            return dungeonBuilderService.getDungeonMatrix(gameID);
        }
        else return null;
    }

    /**
     * Removes a room from the dungeon and deletes all associated pathways.
     *
     * @param gameID identifier of the game that will be modified
     * @param roomID identifier of the room that will be removed
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeRoom/game/room/{gameID}/{roomID}")
    public MatrixInformation[][] removeRoom(@RequestHeader(value = "authorization", required = false) String auth,
                                            @PathVariable long gameID, @PathVariable long roomID) {
        authenticationHelper.doFilterInternal(auth);
        if (dungeonBuilderService.getRoomService().removeRoom(getGame(gameID), roomID) != null) {
            return dungeonBuilderService.getDungeonMatrix(gameID);
        }
        else return null;
    }

    /**
     * Removes an NPC from a dungeon.
     *
     * @param gameID identifier of the game that will be modified
     * @param roomID identifier of the room in which the npc is
     * @param npcID  identifier of the npc that will be deleted
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeNPC/game/room/npc/{gameID}/{roomID}/{npcID}")
    public Game removeNPC(@RequestHeader(value = "authorization", required = false) String auth,
                          @PathVariable long gameID, @PathVariable long roomID, @PathVariable long npcID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getNpcService().removeNPC(getGame(gameID), roomID, npcID);
    }

    /**
     * Deletes a given item from a given room.
     *
     * @param gameID identifier of the game that will be modified
     * @param roomID identifier of the room
     * @param itemID identifier of the item
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeItem/game/room/item/{gameID}/{roomID}/{itemID}")
    public Game removeItem(@RequestHeader(value = "authorization", required = false) String auth,
                           @PathVariable long gameID, @PathVariable long roomID, @PathVariable long itemID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getItemService().removeItem(getGame(gameID), roomID, itemID);
    }

    /**
     * Deletes a room event from the dungeon.
     *
     * @param gameID  identifier of the game that will be modified
     * @param roomID  identifier of the room from which the event will be deleted
     * @param eventID identifier of the event that will be deleted
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeEvent/game/room/event/{gameID}/{roomID}/{eventID}")
    public Game removeEvent(@RequestHeader(value = "authorization", required = false) String auth,
                            @PathVariable long gameID, @PathVariable long roomID, @PathVariable long eventID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getRoomEventService().deleteRoomEvent(getGame(gameID), roomID, eventID);
    }

    /**
     * Removes an NPC template from a dungeon.
     *
     * @param gameID identifier of game that will be modified
     * @param npcID  identifier of the npc that will be deleted
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeNPCTemplate/game/npc/{gameID}/{npcID}")
    public Game removeNPCTemplate(@RequestHeader(value = "authorization", required = false) String auth,
                                  @PathVariable long gameID, @PathVariable long npcID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getNpcService().removeNPCTemplate(getGame(gameID), npcID);
    }

    /**
     * Deletes a previously added item template.
     *
     * @param gameID identifier of the game that will be modified
     * @param itemID identifier of the template that will be removed from the respective dungeon list
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeItemTemplate/game/item/{gameID}/{itemID}")
    public Game removeItemTemplate(@RequestHeader(value = "authorization", required = false) String auth,
                                   @PathVariable long gameID, @PathVariable long itemID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getItemService().removeItemTemplate(getGame(gameID), itemID);
    }

    /**
     * Deletes the template of a room event from the dungeon.
     *
     * @param gameID id of the game that will be modified
     * @param event  id of the event that will be deleted
     * @return modified game
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeEventTemplate/game/event/{gameID}/{event}")
    public Game removeEventTemplate(@RequestHeader(value = "authorization", required = false) String auth,
                                    @PathVariable long gameID, @PathVariable long event) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getRoomEventService().deleteRoomEventTemplate(getGame(gameID), event);
    }

    /**
     * Returns a Dungeonmatrix
     *
     * @param gameID gameID
     * @return the matrix
     */
    @CrossOrigin
    @GetMapping(value = "/dungeonBuilder/getMatrix/game/{gameID}")
    public MatrixInformation[][] getMatrix(@RequestHeader(value = "authorization", required = false) String auth,
                                           @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.getDungeonMatrix(gameID);
    }

    /**
     * Edits the name and description of a room
     *
     * @param gameID      gameID
     * @param roomID      roomID
     * @param name        new Name
     * @param description new Description
     * @return the room
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/editName/game/room/name/description/{gameID}/{roomID}/{name}/{description}")
    public MatrixInformation[][] editNameAndDescription(@RequestHeader(value = "authorization", required = false) String auth, @PathVariable long gameID, @PathVariable long roomID, @PathVariable String name, @PathVariable String description) {
        authenticationHelper.doFilterInternal(auth);
        dungeonBuilderService.getRoomService().editNameAndDescription(getGame(gameID), roomID, name, description);
        return dungeonBuilderService.getDungeonMatrix(gameID);
    }

    /**
     * Publishes the dungeon by setting its status to 'public'.
     *
     * @param gameID gameID
     * @return true if successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/publishDungeon/game/{gameID}")
    public boolean publishDungeon(@RequestHeader(value = "authorization", required = false) String auth,
                                  @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.publishDungeon(getGame(gameID));
    }

    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/saveDungeon/game/{gameID}")
    public boolean saveDungeon(@RequestHeader(value = "authorization", required = false) String auth,
                               @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.saveDungeon(gameID);
    }


    /**
     * Adds a class option to the dungeon.
     *
     * @param characterClass option that will be added
     * @param gameId         id of the game
     * @return true if addition was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addCharacterClass/game/characterClass/{gameId}")
    public Game addCharacterClass(@RequestHeader(value = "authorization", required = false) String auth,
                                  @PathVariable long gameId, @RequestBody CharacterClass characterClass) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.addCharacterClass(gameId, characterClass);
    }

    /**
     * Removes a class option from the dungeon.
     *
     * @param classId id of the option that will be removed
     * @param gameId  id of the game
     * @return true if deletion was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeCharacterClass/game/classId/{gameId}/{classId}")
    public Game removeCharacterClass(@RequestHeader(value = "authorization", required = false) String auth,
                                     @PathVariable long gameId, @PathVariable long classId) {
        authenticationHelper.doFilterInternal(auth);
        dungeonBuilderService.removeCharacterClass(gameId, classId);
        return getGame(gameId);
    }

    /**
     * Adds a race option to the dungeon.
     *
     * @param race   option that will be added
     * @param gameId id of the game
     * @return true if addition was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/addCharacterRace/game/race/{gameId}")
    public Game addCharacterRace(@RequestHeader(value = "authorization", required = false) String auth,
                                 @PathVariable long gameId, @RequestBody CharacterRace race) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonBuilderService.addCharacterRace(gameId, race);
    }

    /**
     * Removes a race option from the dungeon.
     *
     * @param raceId id of the option that will be removed
     * @param gameId id of the game
     * @return true if deletion was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonBuilder/removeCharacterRace/game/raceId/{gameId}/{raceId}")
    public Game removeCharacterRace(@RequestHeader(value = "authorization", required = false) String auth,
                                    @PathVariable long gameId, @PathVariable long raceId) {
        authenticationHelper.doFilterInternal(auth);
        dungeonBuilderService.removeCharacterRace(gameId, raceId);
        return getGame(gameId);
    }

    /**
     * Retrieves instance of a game.
     *
     * @param gameID game that will be retrieved
     * @return instance of game
     */
    private Game getGame(long gameID) {
        Game game = dungeonBuilderService.getDungeonsWIP().get(gameID);
        if (game == null) return dungeonMasterService.getGameFromID(gameID);
        return game;
    }

}
