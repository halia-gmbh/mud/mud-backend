package mudbackend.boundary;

import mudbackend.accountservice.Account;
import mudbackend.accountservice.Admin;
import mudbackend.accountservice.AuthenticationHelper;
import mudbackend.services.AccountService;
import mudbackend.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdminController {
    private final AdminService adminService;
    private final AuthenticationHelper authenticationHelper;
    private final AccountService accountService;

    @Autowired
    public AdminController(AccountService accountService, AdminService adminService, AuthenticationHelper authenticationHelper) {
        this.adminService = adminService;
        this.authenticationHelper = authenticationHelper;
        this.accountService = accountService;
    }

    /**
     * Tries to log in the given user to the server.
     *
     * @param loginForm the login elements (email and password), with which the user tries to login
     * @return the AccountID, if login is successful; -1 if not because of any reason
     */
    @CrossOrigin
    @PostMapping(value = "/api/admin/login")
    public Account adminLogin(@RequestBody String password) {
        return accountService.login("admin", password);
    }

    /**
     * Logs the given user out of the server.
     *
     * @return true if logout was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/api/admin/logout/{adminId}")
    public boolean adminLogout(int adminId) {
        return adminService.adminLogut(adminId);
    }

    /**
     * Simply checks, if authentication via token worked.
     *
     * @return true, if successful
     */
    @CrossOrigin
    @GetMapping(value = "api/check/admin")
    public boolean checkAuthState(@RequestHeader(value = "authorization", required = false) String auth) {
        authenticationHelper.doFilterInternal(auth);
        return true;
    }

    /**
     * Retrieves all accounts that are registered to the server.
     *
     * @return list of accounts
     */
    @CrossOrigin
    @PostMapping(value = "/api/admin/getAllAccounts")
    public List<Account> getAllAccounts(@RequestHeader(value = "authorization", required = false) String auth) {
        authenticationHelper.doFilterInternal(auth);
        return adminService.getAllAccounts();
    }

    /**
     * Retrieves all accounts that are currently active on the server.
     *
     * @return list of accounts
     */
    @CrossOrigin
    @PostMapping(value = "/api/admin/getAllPlayingAccounts")
    public List<Account> getAllPlayingAccounts(@RequestHeader(value = "authorization", required = false) String auth) {
        authenticationHelper.doFilterInternal(auth);
        return adminService.getAllPlayingAccounts();
    }

    /**
     * Verifies an account so that the user can use the login.
     *
     * @param accountID id of the account that will be verified
     * @return true if verification was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/api/admin/verify/{accountID}")
    public boolean verify(@RequestHeader(value = "authorization", required = false) String auth, @PathVariable long accountID) {
        authenticationHelper.doFilterInternal(auth);
        return adminService.verify(accountID);
    }

    /**
     * Deletes an account from the server.
     *
     * @param accountID id of the account that will be deleted
     * @return true of deletion was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/api/delete/{accountID}")
    public boolean delete(@RequestHeader(value = "authorization", required = false) String auth, @PathVariable int accountID) {
        authenticationHelper.doFilterInternal(auth);
        return adminService.delete(accountID);
    }

    /**
     * Changes the password of an account.
     *
     * @param accountID id of the account
     * @param password  password that will be set
     * @return true if change was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/api/changePassword/{accountID}/{password}")
    public boolean changePassword(@RequestHeader(value = "authorization", required = false) String auth, @PathVariable int accountID, @PathVariable String password) {
        authenticationHelper.doFilterInternal(auth);
        return adminService.changePassword(accountID, password);
    }

    /**
     * Changes the email of an account.
     *
     * @param accountID id of the account
     * @param email     email that will be set
     * @return true if change was successful, otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/api/changeEmail/{accountID}/{email}")
    public boolean changeEmail(@RequestHeader(value = "authorization", required = false) String auth, @PathVariable int accountID, @PathVariable String email) {
        authenticationHelper.doFilterInternal(auth);
        return adminService.changeEmail(accountID, email);
    }

    @CrossOrigin
    @PostMapping(value = "/api/adminMessage")
    public boolean adminMessage(@RequestHeader(value = "authorization", required = false) String auth,
                                @RequestBody String msg) {
        authenticationHelper.doFilterInternal(auth);
        return adminService.adminMessage(msg);
    }

    private static class LoginForm {
        public String email;
        public String password;
    }
}
