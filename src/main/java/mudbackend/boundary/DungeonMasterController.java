package mudbackend.boundary;

import mudbackend.accountservice.AuthenticationHelper;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.items.ItemForm;
import mudbackend.services.DungeonMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DungeonMasterController {
    private final DungeonMasterService dungeonMasterService;
    private final AuthenticationHelper authenticationHelper;

    /**
     * Constructor.
     *
     * @param dungeonMasterService service connecting to the front end
     */
    @Autowired
    public DungeonMasterController(DungeonMasterService dungeonMasterService, AuthenticationHelper authenticationHelper) {
        this.dungeonMasterService = dungeonMasterService;
        this.authenticationHelper = authenticationHelper;
    }

    /**
     * Gives a Player a new Item or replaces an old item
     *
     * @param gameID      gameID
     * @param characterID characterID
     * @param itemForm    the new item. If ID is -1 a new Item is created, else the item with the same id is replaced
     * @return true if successful
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonMaster/givePlayerItem/game/character/item/{gameID}/{characterID}")
    public PlayerCharacter givePlayerItem(@RequestHeader(value = "authorization", required = false) String auth,
                                          @PathVariable long gameID, @PathVariable long characterID,
                                          @RequestBody ItemForm itemForm) {
        authenticationHelper.doFilterInternal(auth);
        return this.dungeonMasterService.givePlayerItem(gameID, characterID, itemForm);
    }

    /**
     * Removes an item from a player's inventory.
     *
     * @param gameID      identifier of the game
     * @param characterID identifier of the character whose inventory will be modified
     * @param itemID      identifier of the item that will be removed
     * @return the modified character
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonMaster/deletePlayerItem/game/character/item/{gameID}/{characterID}/{itemID}")
    public PlayerCharacter deletePlayerItem(@RequestHeader(value = "authorization", required = false) String auth,
                                            @PathVariable long gameID, @PathVariable long characterID,
                                            @PathVariable long itemID) {
        authenticationHelper.doFilterInternal(auth);
        return this.dungeonMasterService.deletePlayerItem(gameID, characterID, itemID);
    }

    /**
     * Sets an attribute of a player to a specific value
     *
     * @param gameID      gameID
     * @param characterID characterID
     * @param index       index of the changed Attribute. -1 if new Attribute
     * @param attribute   attriuteName
     * @param value       new value
     * @return the Character
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonMaster/setPlayerAttribute/game/character/index/attribute/value/{gameID" +
            "}/{characterID}/{index}/{attribute}/{value}")
    public PlayerCharacter setPlayerAttribute(@RequestHeader(value = "authorization", required = false) String auth,
                                              @PathVariable long gameID, @PathVariable long characterID,
                                              @PathVariable int index, @PathVariable String attribute,
                                              @PathVariable int value) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonMasterService.setPlayerAttribute(gameID, characterID, index, attribute, value);
    }

    /**
     * Deletes an Attribute from a Characters AttributeSet
     *
     * @param gameID      gameID
     * @param characterID characterID
     * @param attribute   Attribute-Name
     * @return the Character
     */
    @CrossOrigin
    @PostMapping(value =
            "/dungeonMaster/deletePlayerAttribute/game/character/attribute/value/{gameID}/{characterID" +
                    "}/{attribute}")
    public PlayerCharacter deletePlayerAttribute(@RequestHeader(value = "authorization", required = false) String auth, @PathVariable long gameID, @PathVariable long characterID,
                                                 @PathVariable String attribute) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonMasterService.deletePlayerAttribute(gameID, characterID, attribute);
    }

    /**
     * Ends a GameSession. Kicks all Players and saves Game in DB
     *
     * @param gameID gameID
     * @return true, if game was ended
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonMaster/endGame/game/{gameID}")
    public boolean endGame(@RequestHeader(value = "authorization", required = false) String auth,
                           @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonMasterService.endGame(gameID);
    }

    /**
     * Places a player character inside a specified room.
     *
     * @param gameID      identifier of the game that contains the character
     * @param characterID identifier of the character that wil be moved
     * @param roomID      identifier of the room the character will be moved to
     * @return the player character inside the new room
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonMaster/placeCharacter/game/{gameID}/{characterID}/{roomID}")
    public PlayerCharacter placeCharacter(@RequestHeader(value = "authorization", required = false) String auth,
                                          @PathVariable long gameID, @PathVariable long characterID,
                                          @PathVariable long roomID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonMasterService.placeCharacter(gameID, characterID, roomID);
    }

    /**
     * Starts a death timer when a dungeon master leaves the webpage without properly closing the dungeon.
     * If the time runs out without the dungeon master rejoining the games is ended
     *
     * @param gameID the game for which the timer should be started
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonMaster/setDeathWatch/game/{gameID}")
    public void setDeathWatch(@PathVariable long gameID) {
        dungeonMasterService.setDeathWatch(gameID);
    }

    /**
     * Stops the death timer when the dungeon master rejoins again
     *
     * @param gameID the game for which the timer should be stopped
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonMaster/resetDeathWatch/game/{gameID}")
    public void resetDeathWatch(@PathVariable long gameID) {
        dungeonMasterService.resetDeathWatch(gameID);
    }

    @CrossOrigin
    @PostMapping(value = "/dungeonMaster/changeDungeonMaster/game/character/{gameID}/{characterID}")
    public boolean changeDungeonMaster(@PathVariable long gameID, @PathVariable long characterID) {
        return dungeonMasterService.changeDungeonMaster(gameID, characterID);
    }
}
