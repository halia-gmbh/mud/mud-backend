package mudbackend.boundary;

import mudbackend.accountservice.AuthenticationHelper;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.general.Game;
import mudbackend.services.OverviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OverviewController {

    private final OverviewService overviewService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public OverviewController(OverviewService overviewService, AuthenticationHelper authenticationHelper) {
        this.overviewService = overviewService;
        this.authenticationHelper = authenticationHelper;
    }

    /**
     * Retrieves all running games.
     *
     * @return list of all running games
     */
    @GetMapping(value = "/overview/getActiveGames/user/{userID}")
    @CrossOrigin
    public List<Game> getActiveGames(@RequestHeader(value = "authorization", required = false) String auth,
                                     @PathVariable int userID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.getActiveGames(userID);
    }

    /**
     * Retrieves all games that are not running at the moment.
     *
     * @return list of all inactive games
     */
    @GetMapping(value = "/overview/getInactiveGames/user/{userID}")
    @CrossOrigin
    public List<Game> getInactiveGames(@RequestHeader(value = "authorization", required = false) String auth,
                                       @PathVariable int userID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.getInactiveGames(userID);
    }

    /**
     * Retrieves all games built by a certain player.
     *
     * @param accountID id of the player for which all built games shall be retrieved
     * @return list of all built games
     */
    @GetMapping(value = "/overview/getOwnBuiltGames/user/{accountID}")
    @CrossOrigin
    public List<Game> getOwnBuiltGames(@RequestHeader(value = "authorization", required = false) String auth,
                                       @PathVariable int accountID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.getOwnBuiltGames(accountID);
    }

    /**
     * Retrieves all games in which the player is currently active.
     *
     * @param accountID id of the player
     * @return list of all games in which the player is active
     */
    @GetMapping(value = "/overview/getOwnPlayingGames/user/{accountID}")
    @CrossOrigin
    public List<Game> getOwnPlayingGames(@RequestHeader(value = "authorization", required = false) String auth,
                                         @PathVariable int accountID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.getOwnPlayingGames(accountID);
    }

    /**
     * Retrieves a dungeon by its id.
     *
     * @param gameID id of the dungeon
     * @return instance of game
     */
    @GetMapping(value = "/overview/searchDungeon/{gameID}")
    @CrossOrigin
    public Game searchDungeon(@RequestHeader(value = "authorization", required = false) String auth,
                              @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.searchDungeon(gameID);
    }

    /**
     * Rolls back a dungeon. Removes all Character-Information
     */
    @PostMapping(value = "/overview/rollbackDungeon/{gameID}")
    @CrossOrigin
    public Game rollbackDungeon(@RequestHeader(value = "authorization", required = false) String auth,
                                @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.rollBackDungeon(gameID);
    }

    /**
     * Deletes a game if its not public
     *
     * @param gameID gameID
     * @return true if deleted
     */
    @PostMapping(value = "/overview/deleteDungeon/{gameID}")
    @CrossOrigin
    public int deleteDungeon(@RequestHeader(value = "authorization", required = false) String auth,
                             @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.deleteDungeon(gameID);
    }

    /**
     * Starts an inactive Dungeon. The starting user becomes the DungeonMaster
     *
     * @param gameID    gameID
     * @param accountID accountID
     * @return true if game was started
     */
    @PostMapping(value = "/overview/startDungeon/game/user/{gameID}/{accountID}")
    @CrossOrigin
    public boolean startDungeon(@RequestHeader(value = "authorization", required = false) String auth,
                                @PathVariable long gameID, @PathVariable int accountID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.startDungeon(gameID, accountID);
    }


    /**
     * Creates a character and saves it in the given game
     *
     * @param gameID    the game to which the new character belongs
     * @param userID    the user to which the new character belongs
     * @param character the new character
     * @return true if successful
     */
    @PostMapping(value = "/overview/createCharacter/{userID}/{gameID}")
    @CrossOrigin
    public boolean createCharacter(@RequestHeader(value = "authorization", required = false) String auth,
                                   @PathVariable long userID, @PathVariable long gameID,
                                   @RequestBody PlayerCharacter character) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.createCharacter(userID, gameID, character);
    }

    /**
     * Deletes an existing character
     *
     * @param gameID      the game to which the character belongs
     * @param characterID the character to delete
     * @return true if successful
     */
    @PostMapping(value = "/overview/deleteCharacter/{gameID}/{characterID}/{userID}")
    @CrossOrigin
    public List<PlayerCharacter> deleteCharacter(@RequestHeader(value = "authorization", required = false) String auth,
                                                 @PathVariable long gameID,
                                                 @PathVariable long characterID,
                                                 @PathVariable long userID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.deleteCharacter(gameID, characterID, userID);
    }

    @GetMapping(value = "/overview/getCharacters/{userID}/{gameID}")
    @CrossOrigin
    public List<PlayerCharacter> getCharactersOfDungeon(@RequestHeader(value = "authorization", required = false) String auth, @PathVariable long userID, @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return overviewService.getCharactersOfDungeon(userID, gameID);
    }

    /**
     * Allows a player to join an active dungeon and updates their location
     *
     * @param gameID      identifier of the game
     * @param characterID identifier of the character
     * @return the modified game state
     */
    @PostMapping(value = "/overview/joinDungeon/{gameID}/{userID}/{characterID}")
    @CrossOrigin
    public Game joinDungeon(@RequestHeader(value = "authorization", required = false) String auth,
                            @PathVariable long gameID, @PathVariable long userID, @PathVariable long characterID) {
        authenticationHelper.doFilterInternal(auth);
        return this.overviewService.joinDungeon(gameID, userID, characterID);
    }

    /**
     * Sets the password of a game so when players try to enter, they have to authorize.
     *
     * @param gameID   identifier of the game
     * @param password password that will be set
     */
    @PostMapping(value = "/overview/setPassword/{gameID}")
    @CrossOrigin
    public void setPassword(@RequestHeader(value = "authorization", required = false) String auth,
                            @PathVariable long gameID, @RequestBody (required = false) String password) {
        authenticationHelper.doFilterInternal(auth);
        this.overviewService.setPassword(gameID, password);
    }

    /**
     * Checks if the password a player entered is correct.
     *
     * @param gameID   identifier of the game
     * @param password password that will be set
     */
    @PostMapping(value = "/overview/checkPassword/{gameID}/{password}")
    @CrossOrigin
    public boolean checkPassword(@RequestHeader(value = "authorization", required = false) String auth,
                                 @PathVariable long gameID, @PathVariable String password) {
        authenticationHelper.doFilterInternal(auth);
        return this.overviewService.checkPassword(gameID, password);
    }

    /**
     * Checks if the game has a password set or not
     *
     * @param gameID identifier of the game
     */
    @PostMapping(value = "/overview/getGamePassword/{gameID}/")
    @CrossOrigin
    public boolean getGamePassword(@RequestHeader(value = "authorization", required = false) String auth,
                                   @PathVariable long gameID) {
        authenticationHelper.doFilterInternal(auth);
        return this.overviewService.getGamePassword(gameID);
    }

    /**
     * Retrieves a list of all dungeons that are public.
     *
     * @return list of all public game instances
     */
    @PostMapping(value = "/overview/getPublicDungeons/")
    @CrossOrigin
    public List<Game> getPublicDungeons(@RequestHeader(value = "authorization", required = false) String auth) {
        authenticationHelper.doFilterInternal(auth);
        return this.overviewService.getPublicDungeons();
    }

}
