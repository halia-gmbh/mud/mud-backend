package mudbackend.boundary;

import mudbackend.accountservice.AuthenticationHelper;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.dungeondesign.DirectionEnum;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.events.RoomEvent;
import mudbackend.services.DungeonPlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DungeonPlayerController {
    private final DungeonPlayerService dungeonPlayerService;
    private final AuthenticationHelper authenticationHelper;

    /**
     * Constructor.
     *
     * @param dungeonPlayerService service connecting to the front end
     */
    @Autowired
    public DungeonPlayerController(DungeonPlayerService dungeonPlayerService,
                                   AuthenticationHelper authenticationHelper) {
        this.dungeonPlayerService = dungeonPlayerService;
        this.authenticationHelper = authenticationHelper;
    }

    /**
     * Uses an item: Either consumes it, or equips it.
     *
     * @param characterID identifier of the player character
     * @param gameID      identifier of the game
     * @param itemID      identifier of the item
     * @return the player character with a modified inventory
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/useItem/game/character/item/{gameID}/{characterID}/{itemID}")
    public PlayerCharacter useItem(@RequestHeader(value = "authorization", required = false) String auth,
                                   @PathVariable long characterID, @PathVariable long gameID,
                                   @PathVariable long itemID) {
        authenticationHelper.doFilterInternal(auth);
        return this.dungeonPlayerService.useItem(characterID, gameID, itemID);
    }

    /**
     * Unequips an item and places it in the player's inventory.
     *
     * @param characterID identifier of the player character
     * @param gameID      identifier of the game
     * @param itemID      identifier of the item
     * @return the player character with a modified inventory
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/unequip/game/character/item/{gameID}/{characterID}/{itemID}")
    public PlayerCharacter unequipItem(@RequestHeader(value = "authorization", required = false) String auth,
                                       @PathVariable long characterID, @PathVariable long gameID,
                                       @PathVariable long itemID) {
        authenticationHelper.doFilterInternal(auth);
        return this.dungeonPlayerService.unequipItem(characterID, gameID, itemID);
    }

    /**
     * Removes an item from a room and adds it to a player's inventory.
     *
     * @param characterID identifier of the player character
     * @param gameID      identifier of the game
     * @param itemName    name of the item
     * @return the player character with a modified inventory
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/pickUpItem/game/character/item/{gameID}/{characterID}/{itemName}")
    public PlayerCharacter pickUpItem(@RequestHeader(value = "authorization", required = false) String auth,
                                      @PathVariable long characterID, @PathVariable long gameID,
                                      @PathVariable String itemName) {
        authenticationHelper.doFilterInternal(auth);
        return this.dungeonPlayerService.pickUpItem(characterID, gameID, itemName);
    }

    /**
     * Removes an item from a player's inventory and adds it to a room.
     *
     * @param characterID identifier of the player character
     * @param gameID      identifier of the game
     * @param itemID      identifier of the item
     * @return the player character with a modified inventory
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/discardItem/game/character/item/{gameID}/{characterID}/{itemID}")
    public PlayerCharacter discardItem(@RequestHeader(value = "authorization", required = false) String auth,
                                       @PathVariable long characterID, @PathVariable long gameID,
                                       @PathVariable long itemID) {
        authenticationHelper.doFilterInternal(auth);
        return this.dungeonPlayerService.discardItem(characterID, gameID, itemID);
    }

    /**
     * Moves a player character from one room to another following a path in the specified direction.
     *
     * @param characterID identifier of the character that will be moved
     * @param gameID      identifier of the game
     * @param direction   direction of the pathway
     * @return the room the character has been moved to
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/move/game/character/direction/{gameID}/{characterID}/{direction}")
    public Room move(@RequestHeader(value = "authorization", required = false) String auth,
                     @PathVariable long characterID, @PathVariable long gameID, @PathVariable int direction) {
        authenticationHelper.doFilterInternal(auth);
        return this.dungeonPlayerService.move(characterID, gameID, DirectionEnum.getByValue(direction));
    }

    /**
     * Function fetching all the player characters in a given room.
     *
     * @param gameID identifier of the game that contains the room
     * @param roomID identifier of the room containing the player characters
     * @return List of player characters currently residing in that room
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/getCharactersInRoom/game/character/direction/{gameID}/{roomID}")
    public List<PlayerCharacter> getCharactersInRoom(@RequestHeader(value = "authorization", required = false) String auth, @PathVariable long gameID, @PathVariable long roomID) {
        authenticationHelper.doFilterInternal(auth);
        return this.dungeonPlayerService.getCharactersInRoom(gameID, roomID);
    }

    /**
     * Runs interaction with a Roomevent.
     *
     * @param gameID      gameID
     * @param characterID characterID
     * @param eventName   name of the event
     * @return Response-String if TextEvent, else null
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/interactRoomevent/game/character/event/{gameID}/{characterID}/{eventName}")
    public RoomEvent interactRoomevent(@RequestHeader(value = "authorization", required = false) String auth,
                                       @PathVariable long gameID, @PathVariable long characterID,
                                       @PathVariable String eventName) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonPlayerService.interactRoomevent(gameID, characterID, eventName);
    }


    /**
     * Removes a specified character from the active characters attribute within the dungeon class.
     *
     * @param gameID      identifier of the game containing the character
     * @param characterID identifier of the character leaving
     * @return true if the removal was successful
     */
    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/quitGame/game/character/{gameID}/{characterID}")
    public boolean quitGame(@RequestHeader(value = "authorization", required = false) String auth,
                            @PathVariable long gameID, @PathVariable long characterID) {
        authenticationHelper.doFilterInternal(auth);
        return dungeonPlayerService.quitGame(gameID, characterID);
    }

    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/setDeathWatch/{gameID}/{characterID}")
    public void setDeathWatch(@PathVariable long gameID, @PathVariable long characterID) {
        dungeonPlayerService.setDeathWatch(gameID, characterID);
    }

    @CrossOrigin
    @PostMapping(value = "/dungeonPlayer/resetDeathWatch/{gameID}/{characterID}")
    public void resetDeathWatch(@PathVariable long characterID, @PathVariable long gameID) {
        dungeonPlayerService.resetDeathWatch(gameID, characterID);
    }

}
