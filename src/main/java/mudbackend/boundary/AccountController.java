package mudbackend.boundary;

import mudbackend.accountservice.Account;
import mudbackend.accountservice.AuthenticationHelper;
import mudbackend.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {

    private final AccountService accountService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AccountController(AccountService accountService, AuthenticationHelper authenticationHelper) {
        this.accountService = accountService;
        this.authenticationHelper = authenticationHelper;
    }

    /**
     * Tries to log in the given user to the server.
     *
     * @param loginForm the login elements (email and password), with which the user tries to login
     * @return the AccountID, if login is successful; -1 if not because of any reason
     */
    @CrossOrigin
    @PostMapping(value = "/api/account/login")
    public Account login(@RequestBody LoginForm loginForm) {
        return accountService.login(loginForm.email, loginForm.password);
    }

    /**
     * Tries to log out the given user off the server.
     *
     * @return true if logout is successful; otherwise false
     */
    @CrossOrigin
    @PostMapping(value = "/api/account/logout/{accountID}")
    public boolean logout(@PathVariable int accountID) {
        return accountService.logout(accountID);
    }

    /**
     * Tries to sign up a user with the given credentials
     *
     * @param loginForm email address and password, with which the user wants to sign up
     * @return true, if registration is successful; false, if not because of any reason
     */
    @CrossOrigin
    @PostMapping(value = "/api/account/register")
    public boolean register(@RequestBody LoginForm loginForm) {
        return accountService.register(loginForm.email, loginForm.password);
    }

    /**
     * Simply checks, if authentication via token worked.
     *
     * @return true, if successful
     */
    @CrossOrigin
    @GetMapping(value = "api/check")
    public boolean checkAuthState(@RequestHeader(value = "authorization", required = false) String auth) {
        authenticationHelper.doFilterInternal(auth);
        return true;
    }

    /**
     * Resets the password to the given accounts and sends the new one per mail
     *
     * @param email the account email address
     * @return true, if the reset succeeded; false, if not
     */
    @CrossOrigin
    @PostMapping("/api/account/reset")
    public boolean resetPassword(@RequestBody String email) {
        return accountService.resetPassword(email);
    }


    /**
     * Confirms a newly created account with a token sent by mail
     *
     * @param token the token the user entered
     * @return true, if a corresponding account was found and confirmed; false, else
     */
    @CrossOrigin
    @PostMapping("/api/account/confirm")
    public boolean confirmAccount(@RequestBody String token) {
        return accountService.confirmAccount(token);
    }

    private static class LoginForm {
        public String email;
        public String password;
    }
}
