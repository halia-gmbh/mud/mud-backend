package mudbackend.entity.general;

import com.mongodb.DBObject;
import mudbackend.database.MongoHelper;
import mudbackend.entity.characterdesign.AttributeSet;
import mudbackend.entity.characterdesign.CharacterClass;
import mudbackend.entity.characterdesign.CharacterRace;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.dungeondesign.DirectionEnum;
import mudbackend.entity.dungeondesign.NPC;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.events.ItemEvent;
import mudbackend.entity.items.Consumable;
import mudbackend.entity.items.Weapon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class Game_Test {

    @Autowired
    private MongoHelper mongoHelper;

    @Test
    public void gameDBObject() {
        Game game = new Game(100, "testGame", 1234);

        Room room1 = new Room("TestRoom1", "This room is made for Testing purposes");
        Room room2 = new Room("TestRoom2", "This room is made for Testing purposes");
        Room room3 = new Room("TestRoom3", "This room is made for Testing purposes");
        game.getDungeon().addRoom(room1);
        game.getDungeon().addRoom(room2);
        game.getDungeon().addRoom(room3);
        game.getDungeon().addPathway(room1.getId(), room2.getId(), DirectionEnum.NORTH);
        game.getDungeon().addPathway(room2.getId(), room3.getId(), DirectionEnum.EAST);

        CharacterRace testrace = new CharacterRace("class", "hallo", new AttributeSet());
        CharacterClass testclass = new CharacterClass("class", "hallo", new AttributeSet());
        AttributeSet attributeSet = new AttributeSet();
        attributeSet.addAttribute("Strenght", 100);
        testclass.setAttributes(attributeSet);
        PlayerCharacter playerCharacter = new PlayerCharacter(testrace, testclass);
        playerCharacter.setId(10);
        playerCharacter.setName("name");
        playerCharacter.updateLocation(room1);
        List<PlayerCharacter> list = new ArrayList<>();
        list.add(playerCharacter);
        game.addClass(testclass);
        game.addRace(testrace);
        game.getPlayerCharacters().put(Long.parseLong("1"), list);


        Weapon weapon = new Weapon("weaponname", 120, attributeSet);
        weapon.setId(-1);
        NPC npc = new NPC("npc");
        npc.setId(-1);
        npc.addAction("hallo", "hallo zurück");
        ItemEvent itemEvent = new ItemEvent("Event", new Consumable("Apfel", 10, new AttributeSet()));
        itemEvent.setId(-1);
        game.getDungeon().addItemTemplate(weapon);
        game.getDungeon().addNPCTemplate(npc);
        game.getDungeon().addRoomEvent(room1.getId(), itemEvent);

        DBObject object = game.toDBObject();
        Game actual = new Game(object);

        Assertions.assertEquals(game.getId(), actual.getId());
        Assertions.assertEquals(game.getDungeon().getStartRoom().getId(), actual.getDungeon().getStartRoom().getId());
        Assertions.assertEquals(game.getDungeon().getStartRoom().getRoomEvents().get(0).getId()
                , actual.getDungeon().getStartRoom().getRoomEvents().get(0).getId());
    }

    @Test
    public void emptygameDBObject() {
        Game game = new Game(100, "testGame", 1234);

        Room room1 = new Room("TestRoom1", "This room is made for Testing purposes");
        Room room2 = new Room("TestRoom2", "This room is made for Testing purposes");
        Room room3 = new Room("TestRoom3", "This room is made for Testing purposes");
        game.getDungeon().addRoom(room1);
        game.getDungeon().addRoom(room2);
        game.getDungeon().addRoom(room3);
        game.getDungeon().addPathway(room1.getId(), room2.getId(), DirectionEnum.NORTH);
        game.getDungeon().addPathway(room2.getId(), room3.getId(), DirectionEnum.EAST);

        CharacterRace testrace = new CharacterRace("class", "hallo", new AttributeSet());
        CharacterClass testclass = new CharacterClass("class", "hallo", new AttributeSet());
        AttributeSet attributeSet = new AttributeSet();
        attributeSet.addAttribute("Strenght", 100);
        AttributeSet attributeSet2 = new AttributeSet();
        attributeSet2.addAttribute("Strenght", 150);
        attributeSet2.addAttribute("x", 3);
        testclass.setAttributes(attributeSet);
        testrace.setAttributes(attributeSet2);
        PlayerCharacter playerCharacter = new PlayerCharacter(testrace, testclass);
        playerCharacter.setId(10);
        playerCharacter.setName("name");
        playerCharacter.updateLocation(room1);
        List<PlayerCharacter> list = new ArrayList<>();
        list.add(playerCharacter);
        game.addClass(testclass);
        game.addRace(testrace);
        game.getPlayerCharacters().put(Long.parseLong("1"), list);


        Weapon weapon = new Weapon("weaponname", 120, attributeSet);
        weapon.setId(-1);
        NPC npc = new NPC("npc");
        npc.setId(-1);
        npc.addAction("hallo", "hallo zurück");
        ItemEvent itemEvent = new ItemEvent("Event", new Consumable("Apfel", 10, new AttributeSet()));
        itemEvent.setId(-1);
        game.getDungeon().addItemTemplate(weapon);
        game.getDungeon().addNPCTemplate(npc);
        game.getDungeon().addRoomEvent(room1.getId(), itemEvent);

        mongoHelper.saveInDB(game);
        Map<String, Object> query = new HashMap<>();
        query.put("id", game.getId());
        Game actual = mongoHelper.getFromDB(Game.class, query).get(0);

        Assertions.assertEquals(game.getId(), actual.getId());
        Assertions.assertEquals(game.getDungeon().getStartRoom().getId(), actual.getDungeon().getStartRoom().getId());
        Assertions.assertEquals(game.getDungeon().getStartRoom().getRoomEvents().get(0).getId()
                , actual.getDungeon().getStartRoom().getRoomEvents().get(0).getId());
    }
}
