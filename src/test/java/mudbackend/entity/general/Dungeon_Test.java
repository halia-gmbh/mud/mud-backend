package mudbackend.entity.general;

import mudbackend.entity.characterdesign.AttributeSet;
import mudbackend.entity.characterdesign.CharacterClass;
import mudbackend.entity.characterdesign.CharacterRace;
import mudbackend.entity.characterdesign.PlayerCharacter;
import mudbackend.entity.dungeondesign.DirectionEnum;
import mudbackend.entity.dungeondesign.NPC;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.events.ItemEvent;
import mudbackend.entity.events.RoomEvent;
import mudbackend.entity.events.TextEvent;
import mudbackend.entity.items.Consumable;
import mudbackend.entity.items.Item;
import mudbackend.entity.items.Weapon;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class Dungeon_Test {
    Dungeon testDungeon;

    /**
     * Sets up the test fixture.
     * (Called before every test case method.)
     */
    @BeforeEach
    public void setUp() {
        testDungeon = new Dungeon();
    }

    /**
     * Tears down the test fixture.
     * (Called after every test case method.)
     */
    @AfterEach
    public void tearDown() {
        testDungeon = null;
    }

    /**
     * Tests if the first room added to the dungeon will be set as start room.
     */
    @Test
    public void addRoom_isStartRoom_True() {
        // Arrange
        String testName = "test room";
        String testDescription = "this room is used for unit testing";
        Room testRoom = new Room(testName, testDescription);

        // Act
        testDungeon.addRoom(testRoom);

        // Assert
        Assertions.assertEquals(testDungeon.getStartRoom().getName(), testName);
    }

    /**
     * Tests if the every room after the first one is added to the dungeon and does not override the start room.
     */
    @Test
    public void addRoom_roomAdded_True() {
        // Arrange
        String testName_start = "test start room";
        String testDescription_start = "this start room is used for unit testing";
        Room testRoom_start = new Room(testName_start, testDescription_start);

        String testName_normal = "test room";
        String testDescription_normal = "this room is used for unit testing";
        Room testRoom_normal = new Room(testName_normal, testDescription_normal);

        int numberOfExpectedRooms = 2;

        // Act
        testDungeon.addRoom(testRoom_start);
        testDungeon.addRoom(testRoom_normal);

        // Assert
        Assertions.assertTrue((!testName_normal.equals(testDungeon.getStartRoom().getName()) && (numberOfExpectedRooms == testDungeon.getRooms().size())));
    }

    /**
     * Tests if the same room can't be added twice.
     */
    @Test
    public void addRoom_roomAlreadyExists_False() {
        // Arrange
        String testName_normal = "test room";
        String testDescription_normal = "this room is used for unit testing";
        Room testRoom_normal = new Room(testName_normal, testDescription_normal);

        int numberOfExpectedRooms = 1;

        // Act
        testDungeon.addRoom(testRoom_normal);
        testDungeon.addRoom(testRoom_normal);

        // Assert
        Assertions.assertEquals(numberOfExpectedRooms, testDungeon.getRooms().size());
    }

    /**
     * Tests if deletion of room in a dungeon is successful.
     */
    @Test
    public void deleteRoom_dungeonDoesNotContainRoom_True() {
        // Arrange
        String testName_normal = "test room";
        String testDescription_normal = "this room is used for unit testing";
        Room testRoom_normal = new Room(testName_normal, testDescription_normal);

        int numberOfExpectedRooms = 0;

        // Act
        testDungeon.addRoom(testRoom_normal);
        testDungeon.deleteRoom(testRoom_normal.getId());

        // Assert
        Assertions.assertEquals(numberOfExpectedRooms, testDungeon.getRooms().size());
    }

    /**
     * Tests if an item added to the room in a dungeon is in that room.
     */
    @Test
    public void addItem_itemAddedToRoom_True() {
        // Arrange
        Weapon testWeapon = new Weapon("test weapon", 1, new AttributeSet());
        testWeapon.setId(-1);
        Room testRoom = new Room("test room", "this room is used for unit testing");

        // Act
        testDungeon.addRoom(testRoom);
        testDungeon.addItem(testRoom.getId(), testWeapon);

        // Assert
        Assertions.assertTrue(testRoom.getItems().contains(testWeapon));
    }

    /**
     * Tests if a npc added to the room in a dungeon is in that room.
     */
    @Test
    public void addNPC_npcAddedToRoom_True() {
        // Arrange
        NPC testNpc = new NPC("test npc");
        testNpc.setId(-1);
        Room testRoom = new Room("test room", "this room is used for unit testing");

        // Act
        testDungeon.addRoom(testRoom);
        testDungeon.addNPC(testRoom.getId(), testNpc);

        // Assert
        Assertions.assertTrue(testRoom.getNpcs().contains(testNpc));
    }

    // No tests for addItemEvent() and addRoomEvent(), works just like addItem() and addNPC()

    /**
     * Tests if the item deleted in a specific room in a dungeons is gone.
     */
    @Test
    public void deleteItem_itemDeleted_True() {
        // Arrange
        Weapon testWeapon = new Weapon("test weapon", 1, new AttributeSet());
        Room testRoom = new Room("test room", "this room is used for unit testing");

        // Act
        testDungeon.addRoom(testRoom);
        testDungeon.addItem(testRoom.getId(), testWeapon);
        testDungeon.deleteItem(testRoom.getId(), testWeapon.getId());

        // Assert
        Assertions.assertFalse(testRoom.getItems().contains(testWeapon));
    }

    // No tests for other deleteObject functions, work just like deleteItem()

    /**
     * Tests if the connection between two rooms was made.
     */
    @Test
    public void addPathway_roomsConnected_true() {
        // Arrange
        String testName_start = "test start room";
        String testDescription_start = "this start room is used for unit testing";
        Room testRoom_start = new Room(testName_start, testDescription_start);

        String testName_normal = "test room";
        String testDescription_normal = "this room is used for unit testing";
        Room testRoom_normal = new Room(testName_normal, testDescription_normal);

        testDungeon.addRoom(testRoom_start);
        testDungeon.addRoom(testRoom_normal);

        // Act
        testDungeon.addPathway(testRoom_start.getId(), testRoom_normal.getId(), DirectionEnum.NORTH);

        // Assert
        Assertions.assertTrue(testDungeon.isConnected(testRoom_start.getId(), testRoom_normal.getId()));

    }

    /**
     * Tests if the connection between three rooms was made.
     */
    @Test
    public void addPathway_connection_threeRooms() {
        // Arrange
        String testName_start = "test start room";
        String testDescription_start = "this start room is used for unit testing";
        Room testRoom_start = new Room(testName_start, testDescription_start);

        String testName_normal_1 = "test room";
        String testDescription_normal_1 = "this room is used for unit testing";
        Room testRoom_normal_1 = new Room(testName_normal_1, testDescription_normal_1);

        String testName_normal_2 = "test room";
        String testDescription_normal_2 = "this room is used for unit testing";
        Room testRoom_normal_2 = new Room(testName_normal_2, testDescription_normal_2);

        testDungeon.addRoom(testRoom_start);
        testDungeon.addRoom(testRoom_normal_1);
        testDungeon.addRoom(testRoom_normal_2);
        testDungeon.addPathway(testRoom_start.getId(), testRoom_normal_1.getId(), DirectionEnum.NORTH);

        // Act
        testDungeon.addPathway(testRoom_start.getId(), testRoom_normal_2.getId(), DirectionEnum.SOUTH);

        // Assert
        Assertions.assertTrue((testDungeon.isConnected(testRoom_start.getId(), testRoom_normal_1.getId())) && testDungeon.isConnected(testRoom_start.getId(), testRoom_normal_2.getId()));
    }

    /**
     * Tests if the connection between two rooms was deleted in both ways.
     */
    @Test
    public void deletePathway_roomsNotConnected_True() {
        // Arrange
        String testName_start = "test start room";
        String testDescription_start = "this start room is used for unit testing";
        Room testRoom_start = new Room(testName_start, testDescription_start);

        String testName_normal = "test room";
        String testDescription_normal = "this room is used for unit testing";
        Room testRoom_normal = new Room(testName_normal, testDescription_normal);

        testDungeon.addRoom(testRoom_start);
        testDungeon.addRoom(testRoom_normal);
        testDungeon.addPathway(testRoom_start.getId(), testRoom_normal.getId(), DirectionEnum.NORTH);

        // Act
        testDungeon.deletePathway(testRoom_start.getId(), testRoom_normal.getId());
        // Assert
        Assertions.assertFalse(testDungeon.isConnected(testRoom_start.getId(), testRoom_normal.getId()));

    }

    /**
     * Tests if a player can pick up an item from a room.
     */
    @Test
    public void pickUpItem() {
        // Arrange
        Weapon testWeapon = new Weapon("test weapon", 1, new AttributeSet());
        testWeapon.setId(-1);
        PlayerCharacter playerCharacter = new PlayerCharacter(new CharacterRace("", "", new AttributeSet()), new CharacterClass("", "", new AttributeSet()));
        Room testRoom = new Room("test room", "this room is used for unit testing");
        testRoom.getPresentCharacters().add(playerCharacter.getId());
        this.testDungeon.getActiveCharacters().add(playerCharacter);
        playerCharacter.updateLocation(testRoom);
        this.testDungeon.addRoom(testRoom);
        this.testDungeon.addItem(testRoom.getId(), testWeapon);

        // Act
        this.testDungeon.pickUpItem(playerCharacter.getId(), testWeapon.getName());

        // Assert
        Assertions.assertTrue(playerCharacter.getInventory().contains(testWeapon) && !testRoom.getItems().contains(testWeapon));
    }

    /**
     * Tests if a player can discard an item into a room.
     */
    @Test
    public void discardItem() {
        // Arrange
        Weapon testWeapon = new Weapon("test weapon", 1, new AttributeSet());
        testWeapon.setId(-1);
        PlayerCharacter playerCharacter = new PlayerCharacter(new CharacterRace("", "", new AttributeSet()), new CharacterClass("", "", new AttributeSet()));
        Room testRoom = new Room("test room", "this room is used for unit testing");
        testRoom.getPresentCharacters().add(playerCharacter.getId());
        this.testDungeon.getActiveCharacters().add(playerCharacter);
        playerCharacter.updateLocation(testRoom);
        this.testDungeon.addRoom(testRoom);
        playerCharacter.addItem(testWeapon);

        // Act
        this.testDungeon.discardItem(playerCharacter.getId(), testWeapon.getId());

        // Assert
        Assertions.assertTrue(!playerCharacter.getInventory().contains(testWeapon) && testRoom.getItems().contains(testWeapon));
    }

    /**
     * Tests the Generation of a Dungeonmatrix
     */
    @Test
    public void generateMatrix() {
        String roomName = "test start room";
        String roomDescription = "this start room is used for unit testing";
        Room room1 = new Room(roomName, roomDescription);
        Room room2 = new Room(roomName, roomDescription);
        Room room3 = new Room(roomName, roomDescription);
        Room room4 = new Room(roomName, roomDescription);
        Room room5 = new Room(roomName, roomDescription);
        Room room6 = new Room(roomName, roomDescription);
        testDungeon.addRoom(room1);
        testDungeon.addRoom(room2);
        testDungeon.addRoom(room3);
        testDungeon.addRoom(room4);
        testDungeon.addRoom(room5);
        testDungeon.addRoom(room6);
        testDungeon.addPathway(room1.getId(), room2.getId(), DirectionEnum.NORTH);
        testDungeon.addPathway(room2.getId(), room3.getId(), DirectionEnum.EAST);
        testDungeon.addPathway(room1.getId(), room4.getId(), DirectionEnum.EAST);
        testDungeon.addPathway(room4.getId(), room3.getId(), DirectionEnum.NORTH);
        testDungeon.addPathway(room4.getId(), room5.getId(), DirectionEnum.EAST);
        testDungeon.addPathway(room4.getId(), room6.getId(), DirectionEnum.SOUTH);
        MatrixInformation info1 = new MatrixInformation(room1);
        info1.setNorth(true);
        info1.setEast(true);
        info1.setStartRoom(true);
        MatrixInformation info2 = new MatrixInformation(room2);
        info2.setEast(true);
        info2.setSouth(true);
        MatrixInformation info3 = new MatrixInformation(room3);
        info3.setSouth(true);
        info3.setWest(true);
        MatrixInformation info4 = new MatrixInformation(room4);
        info4.setNorth(true);
        info4.setEast(true);
        info4.setSouth(true);
        info4.setWest(true);
        MatrixInformation info5 = new MatrixInformation(room5);
        info5.setWest(true);
        MatrixInformation info6 = new MatrixInformation(room6);
        info6.setNorth(true);
        MatrixInformation[][] expectedMatrix = new MatrixInformation[15][15];
        expectedMatrix[7][7] = info1;
        expectedMatrix[7][6] = info2;
        expectedMatrix[8][6] = info3;
        expectedMatrix[8][7] = info4;
        expectedMatrix[9][7] = info5;
        expectedMatrix[8][8] = info6;

        MatrixInformation[][] actualMatrix = testDungeon.generateMatrix();

        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                if (actualMatrix[i][j] != null) {
                    MatrixInformation actual = actualMatrix[i][j];
                    MatrixInformation expected = expectedMatrix[i][j];
                    Assertions.assertEquals(actual.isNorth(), expected.isNorth());
                    Assertions.assertEquals(actual.isEast(), expected.isEast());
                    Assertions.assertEquals(actual.isWest(), expected.isWest());
                    Assertions.assertEquals(actual.isSouth(), expected.isSouth());
                    Assertions.assertEquals(actual.isStartRoom(), expected.isStartRoom());
                    Assertions.assertEquals(actual.getRoom(), expected.getRoom());
                } else {
                    Assertions.assertNull(expectedMatrix[i][j]);
                }
            }
        }
    }

    @Test
    public void rollbackDungeon() {
        Game game = new Game(10, "testGame", 0);
        PlayerCharacter playerCharacter = new PlayerCharacter(new CharacterRace("", "", new AttributeSet()), new CharacterClass("", "", new AttributeSet()));
        List<PlayerCharacter> characters = new ArrayList<>();
        characters.add(playerCharacter);
        game.getPlayerCharacters().put(Long.parseLong("1"), characters);

        Room room = new Room("", "");
        game.getDungeon().getActiveCharacters().add(playerCharacter);
        playerCharacter.updateLocation(room);

        game.rollback();
        Assertions.assertEquals(0, game.getPlayerCharacters().size());
        Assertions.assertEquals(0, game.getDungeon().getActiveCharacters().size());
    }

    @Test
    public void moveCharacter_works_true() {
    }

    @Test
    public void interactTextEvent() {
        Room testRoom = new Room("TestRoom", "For Testing");
        String eventName = "testEvent";
        String response = "testResponse";
        TextEvent event = new TextEvent(eventName, response);
        event.setId(-1);
        testRoom.addEvent(event);
        testDungeon.addRoom(testRoom);

        PlayerCharacter playerCharacter = new PlayerCharacter(new CharacterRace("", "", new AttributeSet()), new CharacterClass("", "", new AttributeSet()));
        playerCharacter.setId(1);
        playerCharacter.setLocation(testRoom);
        testDungeon.getActiveCharacters().add(playerCharacter);

        RoomEvent actual = testDungeon.interactRoomevent(1, eventName);
        Assertions.assertEquals(event, actual);
    }

    @Test
    public void interactItemEvent() {
        Room testRoom = new Room("TestRoom", "For Testing");
        String eventName = "testEvent";
        Item response = new Consumable("testItem", 100, new AttributeSet());
        ItemEvent event = new ItemEvent(eventName, response);
        event.setId(-1);
        testRoom.addEvent(event);
        testDungeon.addRoom(testRoom);

        PlayerCharacter playerCharacter = new PlayerCharacter(new CharacterRace("", "", new AttributeSet()), new CharacterClass("", "", new AttributeSet()));
        playerCharacter.setId(1);
        playerCharacter.setLocation(testRoom);
        testDungeon.getActiveCharacters().add(playerCharacter);

        RoomEvent actual = testDungeon.interactRoomevent(1, eventName);
        Assertions.assertEquals(actual, event);
        Assertions.assertFalse(testRoom.getRoomEvents().contains(event));
    }

    @Test
    public void isConnectedToStartroom_True() {
        String roomName = "test start room";
        String roomDescription = "this start room is used for unit testing";
        Room room1 = new Room(roomName, roomDescription);
        Room room2 = new Room(roomName, roomDescription);
        Room room3 = new Room(roomName, roomDescription);
        testDungeon.addRoom(room1);
        testDungeon.addRoom(room2);
        testDungeon.addRoom(room3);
        testDungeon.addPathway(room1.getId(), room2.getId(), DirectionEnum.NORTH);
        testDungeon.addPathway(room2.getId(), room3.getId(), DirectionEnum.EAST);

        Assertions.assertTrue(testDungeon.isConnectedToStartroom(room3.getId(), new ArrayList<>()));
    }

    @Test
    public void isConnectedToStartroom_False() {
        String roomName = "test start room";
        String roomDescription = "this start room is used for unit testing";
        Room room1 = new Room(roomName, roomDescription);
        Room room2 = new Room(roomName, roomDescription);
        Room room3 = new Room(roomName, roomDescription);
        testDungeon.addRoom(room1);
        testDungeon.addRoom(room2);
        testDungeon.addRoom(room3);
        testDungeon.addPathway(room2.getId(), room3.getId(), DirectionEnum.EAST);

        Assertions.assertFalse(testDungeon.isConnectedToStartroom(room3.getId(), new ArrayList<>()));
    }
}
