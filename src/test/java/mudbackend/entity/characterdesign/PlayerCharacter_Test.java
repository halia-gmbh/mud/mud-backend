package mudbackend.entity.characterdesign;

import mudbackend.entity.items.Armour;
import mudbackend.entity.items.Weapon;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerCharacter_Test {
    private PlayerCharacter playerCharacter;

    /**
     * Sets up the test fixture.
     * (Called before every test case method.)
     */
    @BeforeEach
    public void setUp() {
        CharacterRace race = new CharacterRace("Human", "A simple human", new AttributeSet());
        CharacterClass characterClass = new CharacterClass("Warrior", "Simple minded people with an axe and a shield", new AttributeSet());
        this.playerCharacter = new PlayerCharacter(race, characterClass);
    }

    /**
     * Tears down the test fixture.
     * (Called after every test case method.)
     */
    @AfterEach
    public void tearDown() {
        this.playerCharacter = null;
    }

    /**
     * Checks if a weapon can be added to a player's inventory.
     */
    @Test
    public void addWeaponToInventory() {
        // Arrange
        Weapon testWeapon = new Weapon("Axe", 3, new AttributeSet());
        testWeapon.setId(-1);

        // Act
        this.playerCharacter.addItem(testWeapon);

        // Assert
        Assertions.assertTrue(this.playerCharacter.getInventory().contains(testWeapon));
    }

    /**
     * Checks if a piece of armour can be added to a player's inventory.
     */
    @Test
    public void addArmourToInventory() {
        // Arrange
        Armour testArmour = new Armour("Chest plate", 15, new AttributeSet());
        testArmour.setId(-1);

        // Act
        this.playerCharacter.addItem(testArmour);

        // Assert
        Assertions.assertTrue(this.playerCharacter.getInventory().contains(testArmour));
    }

    /**
     * Checks if a piece of armour can be equipped and is removed from a player's inventory.
     */
    @Test
    public void equipArmourRemoveFromInventory() {
        // Arrange
        Armour testArmour = new Armour("Chest plate", 15, new AttributeSet());
        testArmour.setId(-1);
        this.playerCharacter.addItem(testArmour);
        long armourID = testArmour.getId();

        // Act
        this.playerCharacter.equipArmour(armourID);

        // Assert
        Assertions.assertFalse(this.playerCharacter.getInventory().contains(testArmour));
    }

    /**
     * Checks if a piece of armour can be equipped.
     */
    @Test
    public void equipArmour() {
        // Arrange
        Armour testArmour = new Armour("Chest plate", 15, new AttributeSet());
        testArmour.setId(-1);
        this.playerCharacter.addItem(testArmour);
        long armourID = testArmour.getId();

        // Act
        this.playerCharacter.equipArmour(armourID);

        // Assert
        Assertions.assertEquals(testArmour, this.playerCharacter.getArmour());
    }

    /**
     * Checks if a piece of weapon can be equipped and is removed from a player's inventory.
     */
    @Test
    public void equipWeaponRemoveFromInventory() {
        // Arrange
        Weapon testWeapon = new Weapon("Axe", 3, new AttributeSet());
        testWeapon.setId(-1);
        this.playerCharacter.addItem(testWeapon);
        long weaponID = testWeapon.getId();

        // Act
        this.playerCharacter.equipWeapon(weaponID);

        // Assert
        Assertions.assertFalse(this.playerCharacter.getInventory().contains(testWeapon));
    }

    /**
     * Checks if a piece of weapon can be equipped.
     */
    @Test
    public void equipWeapon() {
        // Arrange
        Weapon testWeapon = new Weapon("Axe", 3, new AttributeSet());
        testWeapon.setId(-1);
        this.playerCharacter.addItem(testWeapon);
        long weaponID = testWeapon.getId();

        // Act
        this.playerCharacter.equipWeapon(weaponID);

        // Assert
        Assertions.assertEquals(testWeapon, this.playerCharacter.getWeapon());
    }
}
