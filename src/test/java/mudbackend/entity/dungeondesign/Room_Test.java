package mudbackend.entity.dungeondesign;

import mudbackend.entity.characterdesign.AttributeSet;
import mudbackend.entity.events.ItemEvent;
import mudbackend.entity.events.TextEvent;
import mudbackend.entity.items.Accessoire;
import mudbackend.entity.items.Armour;
import mudbackend.entity.items.Consumable;
import mudbackend.entity.items.Weapon;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Room_Test {
    Room testRoom;

    /**
     * Sets up the test fixture.
     * (Called before every test case method.)
     */
    @BeforeEach
    public void setUp() {
        testRoom = new Room("testName", "test description");
    }

    /**
     * Tears down the test fixture.
     * (Called after every test case method.)
     */
    @AfterEach
    public void tearDown() {
        testRoom = null;
    }

    /**
     * Checks if an accessoir item can be added to a room.
     */
    @Test
    public void addItem_accessoirAdded_True() {
        // Arrange
        String testName = "test name";
        int testWeight = 1;
        AttributeSet testAttributes = new AttributeSet();
        Accessoire testItem = new Accessoire(testName, testWeight, testAttributes);
        testItem.setId(-1);

        // Act
        testRoom.addItem(testItem);

        // Assert
        Assertions.assertTrue(testRoom.getItems().contains(testItem));
    }

    /**
     * Checks if an armour item can be added to a room.
     */
    @Test
    public void addItem_armourAdded_True() {
        // Arrange
        String testName = "test name";
        int testWeight = 1;
        AttributeSet testAttributes = new AttributeSet();
        Armour testItem = new Armour(testName, testWeight, testAttributes);
        testItem.setId(-1);

        // Act
        testRoom.addItem(testItem);

        // Assert
        Assertions.assertTrue(testRoom.getItems().contains(testItem));
    }

    /**
     * Checks if a consumable item can be added to a room.
     */
    @Test
    public void addItem_consumableAdded_True() {
        // Arrange
        String testName = "test name";
        int testWeight = 1;
        AttributeSet testAttributes = new AttributeSet();
        Consumable testItem = new Consumable(testName, testWeight, testAttributes);
        testItem.setId(-1);

        // Act
        testRoom.addItem(testItem);

        // Assert
        Assertions.assertTrue(testRoom.getItems().contains(testItem));
    }

    /**
     * Checks if a weapon item can be added to a room.
     */
    @Test
    public void addItem_weaponAdded_True() {
        // Arrange
        String testName = "test name";
        int testWeight = 1;
        AttributeSet testAttributes = new AttributeSet();
        Weapon testItem = new Weapon(testName, testWeight, testAttributes);
        testItem.setId(-1);

        // Act
        testRoom.addItem(testItem);

        // Assert
        Assertions.assertTrue(testRoom.getItems().contains(testItem));
    }

    /**
     * Checks if a non playable character can be added to a room.
     */
    @Test
    public void addNPC_npcAdded_True() {
        // Arrange
        NPC testNPC = new NPC("Max Mustermann");
        testNPC.setId(-1);

        // Act
        testRoom.addNPC(testNPC);

        // Assert
        Assertions.assertTrue(testRoom.getNpcs().contains(testNPC));
    }

    /**
     * Checks if a text event can be added to a room.
     */
    @Test
    public void addEvent_textEventAdded_True() {
        // Arrange
        TextEvent testTextEvent = new TextEvent("test text", "this is a test text event");
        testTextEvent.setId(-1);

        // Act
        testRoom.addEvent(testTextEvent);

        // Assert
        Assertions.assertTrue(testRoom.getRoomEvents().contains(testTextEvent));
    }

    /**
     * Checks if an item event can be added to a room.
     */
    @Test
    public void addEvent_itemEventAdded_True() {
        // Arrange
        String testName = "test name";
        int testWeight = 1;
        AttributeSet testAttributes = new AttributeSet();
        Weapon testItem = new Weapon(testName, testWeight, testAttributes);
        ItemEvent testItemEvent = new ItemEvent("test item event", testItem);
        testItemEvent.setId(-1);

        // Act
        testRoom.addEvent(testItemEvent);

        // Assert
        Assertions.assertTrue(testRoom.getRoomEvents().contains(testItemEvent));
    }

    /**
     * Checks if an item can be deleted from a room.
     */
    @Test
    public void deleteItem_itemAdded_True() {
        // Arrange
        String testName = "test name";
        int testWeight = 1;
        AttributeSet testAttributes = new AttributeSet();
        Weapon testItem = new Weapon(testName, testWeight, testAttributes);

        // Act
        testRoom.addItem(testItem);
        testRoom.deleteItem(testItem.getId());

        // Assert
        Assertions.assertFalse(testRoom.getItems().contains(testItem));
    }

    /**
     * Checks if a non playable character can be deleted from a room.
     */
    @Test
    public void deleteNPC_npcAdded_True() {
        // Arrange
        NPC testNPC = new NPC("Max Mustermann");

        // Act
        testRoom.addNPC(testNPC);
        long testId = testNPC.getId();
        testRoom.deleteNPC(testId);

        // Assert
        Assertions.assertFalse(testRoom.getNpcs().contains(testNPC));
    }

    /**
     * Checks if a text event can be deleted from a room.
     */
    @Test
    public void deleteTextEvent_textEventAdded_True() {
        // Arrange
        TextEvent testTextEvent = new TextEvent("test text event", "this is a test text event");

        // Act
        testRoom.addEvent(testTextEvent);
        testRoom.deleteEvent(testTextEvent.getId());

        // Assert
        Assertions.assertFalse(testRoom.getRoomEvents().contains(testTextEvent));
    }

    /**
     * Checks if an item event can be deleted from a room.
     */
    @Test
    public void deleteItemEvent_itemEventAdded_True() {
        // Arrange
        String testName = "test name";
        int testWeight = 1;
        AttributeSet testAttributes = new AttributeSet();
        Weapon testItem = new Weapon(testName, testWeight, testAttributes);
        ItemEvent testItemEvent = new ItemEvent("test item event", testItem);
        long testId = testItemEvent.getId();

        // Act
        testRoom.addEvent(testItemEvent);
        testRoom.deleteEvent(testId);

        // Assert
        Assertions.assertFalse(testRoom.getRoomEvents().contains(testItemEvent));
    }
}
