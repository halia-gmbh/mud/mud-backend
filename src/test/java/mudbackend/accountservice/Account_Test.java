package mudbackend.accountservice;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Account_Test {

    Account testAccount;

    /**
     * Sets up the test fixture.
     * (Called before every test case method.)
     */
    @BeforeEach
    public void setUp() {
        testAccount = new Account("test@halia.com", "123");
    }

    /**
     * Tears down the test fixture.
     * (Called after every test case method.)
     */
    @AfterEach
    public void tearDown() {
        testAccount = null;
    }

    /**
     * This test checks if gameId will be added to according list in account class through addCreatedDungeon().
     */
    @Test
    public void addCreatedDungeon_dungeonAdded_true() {
        // Arrange
        Long testGameId = 1337L;

        // Act
        testAccount.addCreatedDungeon(testGameId);

        // Assert
        Assertions.assertTrue(testAccount.getDungeonsBuilt().contains(testGameId));

    }

    /**
     * This test checks if gameId will be added to according list in account class through joinGame().
     */
    @Test
    public void joinGame_gameAdded_true() {
        // Arrange
        Long testGameId = 9779L;

        // Act
        testAccount.joinGame(testGameId);

        // Assert
        Assertions.assertTrue(testAccount.getJoinedGames().contains(testGameId));
    }

    /**
     * This test checks if the confirmation of an account works.
     */
    @Test
    public void confirmAccount_confirmationWorks_true() {
        // Arrange
        // all in setUp()

        // Act
        testAccount.confirmAccount();

        // Assert
        Assertions.assertTrue(testAccount.isUnlocked());
    }

    /**
     *
     */
    @Test
    public void resetPassword_passwordChanged_true() {
        // TODO: tested method yet to be implemented
    }

    /**
     *
     */
    @Test
    public void getUserDetails_returnsAll_true() {
        // TODO: tested method yet to be implemented
    }

}