package mudbackend.accountservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EmailHelper_Test {

    EmailHelper helper;

    @Test
    public void testConnection() {
        helper = new EmailHelper();
        helper.getMailer().testConnection();
    }

    @Test
    public void sendPassword_correctMailAddress_returnsTrue() {
        //Arrange
        helper = new EmailHelper();

        //Act
        boolean result = helper.sendPassword("dominik.sauerer@gmail.com", "test", false);

        //Assert
        Assertions.assertTrue(result);
    }

    @Test
    public void sendPassword_noEmailAddress_returnsFalse() {
        //Arrange
        helper = new EmailHelper();

        //Act
        boolean result = helper.sendPassword("dominik", "test", false);

        //Assert
        Assertions.assertFalse(result);
    }
}
