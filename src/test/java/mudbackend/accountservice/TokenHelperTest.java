package mudbackend.accountservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;

@SpringBootTest
public class TokenHelperTest {

    @Autowired
    private TokenHelper tokenHelper;

    @Test
    public void returnUsername_NotExisting() {
        String username = tokenHelper.getUsernameFromToken("NotAtoken");
        Assertions.assertNull(username);
    }

    @Test
    public void returnUsername_Existing() {
        String username = "user1";
        UserDetails userDetails = new User(username, "password", new ArrayList<>());
        String token = tokenHelper.generateToken(userDetails);
        String actualUsername = tokenHelper.getUsernameFromToken(token);
        Assertions.assertEquals(username, actualUsername);
    }

    @Test
    public void generateToken() {
        String username = "user2";
        UserDetails userDetails = new User(username, "password", new ArrayList<>());
        String token = tokenHelper.generateToken(userDetails);
        Assertions.assertNotNull(token);
    }

    @Test
    public void validateToken_Existing_False() {
        String username = "user3";
        UserDetails userDetails = new User(username, "password", new ArrayList<>());
        String token = tokenHelper.generateToken(userDetails);
        userDetails = new User("notAUser", "password", new ArrayList<>());
        boolean result = tokenHelper.validateToken(token, userDetails);
        Assertions.assertFalse(result);
    }

    @Test
    public void validateToken_Existing_True() {
        String username = "user4";
        UserDetails userDetails = new User(username, "password", new ArrayList<>());
        String token = tokenHelper.generateToken(userDetails);
        boolean result = tokenHelper.validateToken(token, userDetails);
        Assertions.assertTrue(result);
    }

    @Test
    public void validateToken_NotExisting() {
        String token = "notAToken";
        UserDetails userDetails = new User("notAUser", "password", new ArrayList<>());
        boolean result = tokenHelper.validateToken(token, userDetails);
        Assertions.assertFalse(result);
    }
}
