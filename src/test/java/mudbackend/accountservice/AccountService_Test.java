package mudbackend.accountservice;

import mudbackend.database.MongoHelper;
import mudbackend.services.AccountService;
import mudbackend.services.DungeonMasterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AccountService_Test {

    AccountService accountService;

    @Mock
    MongoHelper mongoHelper;

    @Mock
    Account testAccount;

    @Mock
    TokenHelper tokenHelper;

    @Mock
    DungeonMasterService dungeonMasterService;

    @Test
    public void login_getsCorrectCredentials_returnsCorrectAccount() {
        //Arrange
        Assertions.assertNotNull(mongoHelper);
        Assertions.assertNotNull(testAccount);
        String encryptedPassword = "2fae046dbb7ddc1d49b96f393a7a8269"; //md5 hash of "testpasswort"
        when(testAccount.getId()).thenReturn(123L);
        when(testAccount.getEmail()).thenReturn("test@test.test");
        when(testAccount.getPassword()).thenReturn(encryptedPassword);
        List<Account> result = new ArrayList<>();
        result.add(testAccount);
        when(mongoHelper.getFromDB(eq(Account.class), any())).thenReturn(result);

        when(tokenHelper.generateToken(any())).thenReturn("jslijvsilfjdl");
        accountService = new AccountService(tokenHelper, mongoHelper, dungeonMasterService);
        accountService.setMongoHelper(mongoHelper);
        when(testAccount.isUnlocked()).thenReturn(true);

        //Act
        Account idResult = accountService.login("test@test.test", "testpasswort");

        //Assert
        Assertions.assertEquals(idResult.getId(), 123);

    }

    @Test
    public void login_getsFalsePassword_returnsNull() {
        //Arrange
        Assertions.assertNotNull(mongoHelper);
        Assertions.assertNotNull(testAccount);
        String encryptedPassword = "2fae046dbb7ddc1d49b96f393a7a8269"; //md5 hash of "testpasswort"
        when(testAccount.getPassword()).thenReturn(encryptedPassword);
        List<Account> result = new ArrayList<>();
        result.add(testAccount);
        when(mongoHelper.getFromDB(eq(Account.class), any())).thenReturn(result);

        accountService = new AccountService(tokenHelper, mongoHelper, dungeonMasterService);
        accountService.setMongoHelper(mongoHelper);

        //Act
        Account idResult = accountService.login("test@test.test", "testpassworT");

        //Assert
        Assertions.assertNull(idResult);
    }

    @Test
    public void login_accountDoesNotExist_returnsNull() {
        //Arrange
        accountService = new AccountService(tokenHelper, mongoHelper, dungeonMasterService);
        List<Account> result = new ArrayList<>();
        when(mongoHelper.getFromDB(eq(Account.class), any())).thenReturn(result);
        accountService.setMongoHelper(mongoHelper);

        //Act
        Account idResult = accountService.login("test@test.test", "testpasswort");

        //Assert
        Assertions.assertEquals(accountService.getLoggedInNumber(), 0);
        Assertions.assertNull(idResult);
    }

    @Test
    public void register_emailAlreadyRegistered_returnsFalse() {
        //Arrange
        accountService = new AccountService(tokenHelper, mongoHelper, dungeonMasterService);
        List<Account> result = new ArrayList<>();
        result.add(testAccount);
        when(mongoHelper.getFromDB(eq(Account.class), any())).thenReturn(result);
        accountService.setMongoHelper(mongoHelper);

        //Act
        boolean actResult = accountService.register("test@test.test", "testpasswort");

        //Assert
        Assertions.assertFalse(actResult);
    }

    @Test
    public void register_RegistrationSuccessful_returnsTrue() {
        //Arrange
        accountService = new AccountService(tokenHelper, mongoHelper, dungeonMasterService);
        List<Account> result = new ArrayList<>();
        when(mongoHelper.getFromDB(eq(Account.class), any())).thenReturn(result);
        accountService.setMongoHelper(mongoHelper);
        when(mongoHelper.saveInDB(any())).thenReturn(true);

        //Act
        boolean actResult = accountService.register("test2@test.test", "testpasswort");

        //Assert
        Assertions.assertTrue(actResult);
    }

}
