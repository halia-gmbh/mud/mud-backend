package mudbackend.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DatabaseConnectionTest {

    @Value("${mongoDB.IP}")
    private String mongoIP;

    @Value("${mongoDB.Port}")
    private int mongoPort;

    @Test
    public void tryToConnectToDB() {
        MongoClientOptions.Builder o = MongoClientOptions.builder().connectTimeout(3000);
        ServerAddress adress;
        try {
            MongoClient mongo = new MongoClient(new ServerAddress(mongoIP, mongoPort), o.build());
            adress = mongo.getAddress();
            Assertions.assertEquals(adress.getHost(), mongoIP);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    public void tryToConnectToFalseDB() {
        MongoClientOptions.Builder o = MongoClientOptions.builder().connectTimeout(3000);
        ServerAddress adress;
        try {
            MongoClient mongo = new MongoClient(new ServerAddress("193.196.55.122", 27021), o.build());
            mongo.getAddress();
            Assertions.fail();
        } catch (Exception ignored) {

        }
    }
}