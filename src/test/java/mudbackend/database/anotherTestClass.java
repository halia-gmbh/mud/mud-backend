package mudbackend.database;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class anotherTestClass implements DBSavable {
    int number;

    public anotherTestClass(int number) {
        this.number = number;
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject().append("number", number);
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        number = (int) dbObject.get("number");
    }
}
