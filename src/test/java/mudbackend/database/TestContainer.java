package mudbackend.database;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.util.ArrayList;
import java.util.List;

public class TestContainer implements DBSavable {
    int number;
    String text;
    List<Character> testChars;

    public TestContainer(int number, String text, List<Character> testChars) {
        this.number = number;
        this.text = text;
        this.testChars = testChars;
    }

    public TestContainer(DBObject object) {
        this.toJavaObject(object);
    }

    @Override
    public DBObject toDBObject() {
        return new BasicDBObject("number", number)
                .append("text", text)
                .append("testChars", testChars);
    }

    @Override
    public void toJavaObject(DBObject dbObject) {
        number = (int) dbObject.get("number");
        text = (String) dbObject.get("text");
        testChars = new ArrayList<>();
        List<String> charList = (List<String>) dbObject.get("testChars");
        for (String x : charList) {
            testChars.add(x.charAt(0));
        }
    }
}
