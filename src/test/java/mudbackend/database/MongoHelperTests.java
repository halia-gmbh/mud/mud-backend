package mudbackend.database;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class MongoHelperTests {

    @Value("${mongoDB.IP}")
    private String mongoIP;

    @Value("${mongoDB.Port}")
    private int mongoPort;

    @Autowired
    private MongoHelper helper;


    MongoClient client;
    DBCollection testContainerCollection;
    DBCollection anotherTestClassCollection;

    TestContainer testContainer;
    TestContainer otherTestContainer;

    @BeforeEach
    public void init() {
        try {
            this.client = new MongoClient(new ServerAddress(mongoIP, mongoPort));
        } catch (Exception ignored) {
        }
        this.testContainerCollection = client.getDB("HaliaTest").getCollection("TestContainer");
        testContainerCollection.drop();
        this.anotherTestClassCollection = client.getDB("HaliaTest").getCollection("anotherTestClass");
        anotherTestClassCollection.drop();

        ArrayList<Character> newCharList = new ArrayList<>();
        newCharList.add('a');
        newCharList.add('7');
        testContainer = new TestContainer(23, "Hallo", newCharList);
        otherTestContainer = new TestContainer(24, "Hallo", newCharList);

        testContainerCollection.insert(new BasicDBObject("number", 23)
                .append("text", "Hallo")
                .append("testChars", newCharList));

        testContainerCollection.insert(new BasicDBObject("number", 24)
                .append("text", "Hallo")
                .append("testChars", newCharList));
    }

    @Test
    public void getFromDB_getSavedObjects_correctObjects() throws UnknownHostException {
        //Arrange
        helper.switchToTest();

        Map<String, Object> query = new HashMap<>();
        query.put("number", 23);

        //Act
        List<TestContainer> result = helper.getFromDB(TestContainer.class, query);

        //Assert
        Assertions.assertEquals(result.size(), 1);
        Assertions.assertEquals(testContainer.text, result.get(0).text);
        Assertions.assertEquals(testContainer.number, result.get(0).number);
        for (int i = 0; i < testContainer.testChars.size(); i++) {
            Assertions.assertEquals(testContainer.testChars.get(i), result.get(0).testChars.get(i));
        }
    }

    @Test
    public void getFromDB_getNonExistingObjects_emptyList() throws UnknownHostException {
        //Arrange
        helper.switchToTest();

        Map<String, Object> query = new HashMap<>();
        query.put("number", 25);

        //Act
        List<TestContainer> result = helper.getFromDB(TestContainer.class, query);

        //Assert
        Assertions.assertEquals(result.size(), 0);
    }

    @Test
    public void getFromDB_queryMapIsNull_getAllObjectsFromCollection() throws UnknownHostException {
        //Arrange
        helper.switchToTest();

        //Act
        List<TestContainer> result = helper.getFromDB(TestContainer.class, null);

        //Assert
        Assertions.assertEquals(result.size(), 2);
    }

    @Test
    public void getFromDB_WithUnsupportedClass_null() throws UnknownHostException {
        //Arrange
        helper.switchToTest();

        Map<String, Object> query = new HashMap<>();
        query.put("number", 23);

        //Act
        List<anotherTestClass> result = helper.getFromDB(anotherTestClass.class, query);

        //Assert
        Assertions.assertNull(result);
    }

    @Test
    public void deleteFromDB_WithUnsupportedClass_returnMinus1() throws UnknownHostException {
        //Arrange
        helper.switchToTest();

        Map<String, Object> query = new HashMap<>();
        query.put("number", 23);

        //Act
        int result = helper.deleteFromDB(anotherTestClass.class, query);

        //Assert
        Assertions.assertEquals(result, -1);
        Assertions.assertEquals(testContainerCollection.find().count(), 2);
    }

    @Test
    public void deleteFromDB_queryMapIsNull_deleteAllObjectsFromCollection() throws UnknownHostException {
        //Arrange
        helper.switchToTest();

        //Act
        int result = helper.deleteFromDB(TestContainer.class, null);

        //Assert
        Assertions.assertEquals(result, 2);
        Assertions.assertEquals(testContainerCollection.find().count(), 0);
    }

    @Test
    public void deleteFromDB_deleteSavedObjects_correctNumber() throws UnknownHostException {
        //Arrange
        helper.switchToTest();

        Map<String, Object> query = new HashMap<>();
        query.put("number", 23);

        //Act
        int result = helper.deleteFromDB(TestContainer.class, query);

        //Assert
        Assertions.assertEquals(result, 1);
        Assertions.assertEquals(testContainerCollection.find().count(), 1);
    }

    @AfterEach
    public void teardown() {
        testContainerCollection.drop();
        anotherTestClassCollection.drop();
        client.close();
    }

}
