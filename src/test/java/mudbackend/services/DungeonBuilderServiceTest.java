package mudbackend.services;

import mudbackend.entity.dungeondesign.DirectionEnum;
import mudbackend.entity.dungeondesign.Room;
import mudbackend.entity.general.Game;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@SpringBootTest
public class DungeonBuilderServiceTest {

    @Mock
    SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    DungeonBuilderRoomService dungeonBuilderRoomService;

    Game testGame;

    /**
     * Sets up the test fixture.
     * (Called before every test case method.)
     */
    @BeforeEach
    public void setUp() {
        testGame = new Game(10, "TestGame", 10);
        Mockito.doNothing().when(simpMessagingTemplate).convertAndSend("/topic/game/" + testGame.getId() + "/roomsWM", "");
    }

    /**
     * Tears down the test fixture.
     * (Called after every test case method.)
     */
    @AfterEach
    public void tearDown() {
        testGame = null;
    }

    @Test
    public void deletePathway_True() {
        long[] ids = new long[4];
        dungeonBuilderRoomService.addRoom(testGame, "room1", "0");
        dungeonBuilderRoomService.addRoom(testGame, "room2", "1");
        dungeonBuilderRoomService.addRoom(testGame, "room3", "2");
        dungeonBuilderRoomService.addRoom(testGame, "room4", "3");
        for (Room room : testGame.getDungeon().getRooms().keySet()) {
            ids[Integer.parseInt(room.getDescription())] = room.getId();
        }
        dungeonBuilderRoomService.addPathway(testGame, ids[0], ids[1], DirectionEnum.NORTH);
        dungeonBuilderRoomService.addPathway(testGame, ids[1], ids[2], DirectionEnum.EAST);
        dungeonBuilderRoomService.addPathway(testGame, ids[2], ids[3], DirectionEnum.SOUTH);
        dungeonBuilderRoomService.addPathway(testGame, ids[3], ids[0], DirectionEnum.WEST);
        dungeonBuilderRoomService.deletePathway(testGame, ids[2], ids[3]);

        Assertions.assertFalse(testGame.getDungeon().isConnected(ids[2], ids[3]));
    }

    @Test
    public void deletePathway_False() {
        long[] ids = new long[3];
        dungeonBuilderRoomService.addRoom(testGame, "room1", "0");
        dungeonBuilderRoomService.addRoom(testGame, "room2", "1");
        dungeonBuilderRoomService.addRoom(testGame, "room3", "2");
        for (Room room : testGame.getDungeon().getRooms().keySet()) {
            ids[Integer.parseInt(room.getDescription())] = room.getId();
        }
        dungeonBuilderRoomService.addPathway(testGame, ids[0], ids[1], DirectionEnum.NORTH);
        dungeonBuilderRoomService.addPathway(testGame, ids[1], ids[2], DirectionEnum.EAST);
        dungeonBuilderRoomService.deletePathway(testGame, ids[1], ids[2]);

        Assertions.assertTrue(testGame.getDungeon().isConnected(ids[1], ids[2]));
    }

    @Test
    public void deleteRoom_True() {
        long[] ids = new long[4];
        dungeonBuilderRoomService.addRoom(testGame, "room1", "0");
        dungeonBuilderRoomService.addRoom(testGame, "room2", "1");
        dungeonBuilderRoomService.addRoom(testGame, "room3", "2");
        dungeonBuilderRoomService.addRoom(testGame, "room4", "3");
        for (Room room : testGame.getDungeon().getRooms().keySet()) {
            ids[Integer.parseInt(room.getDescription())] = room.getId();
        }
        dungeonBuilderRoomService.addPathway(testGame, ids[0], ids[1], DirectionEnum.NORTH);
        dungeonBuilderRoomService.addPathway(testGame, ids[1], ids[2], DirectionEnum.EAST);
        dungeonBuilderRoomService.addPathway(testGame, ids[2], ids[3], DirectionEnum.SOUTH);
        dungeonBuilderRoomService.addPathway(testGame, ids[3], ids[0], DirectionEnum.WEST);
        dungeonBuilderRoomService.removeRoom(testGame, ids[3]);

        Assertions.assertEquals(3, testGame.getDungeon().getRooms().size());
    }

    @Test
    public void deleteRoom_False() {
        long[] ids = new long[3];
        dungeonBuilderRoomService.addRoom(testGame, "room1", "0");
        dungeonBuilderRoomService.addRoom(testGame, "room2", "1");
        dungeonBuilderRoomService.addRoom(testGame, "room3", "2");
        for (Room room : testGame.getDungeon().getRooms().keySet()) {
            ids[Integer.parseInt(room.getDescription())] = room.getId();
        }
        dungeonBuilderRoomService.addPathway(testGame, ids[0], ids[1], DirectionEnum.NORTH);
        dungeonBuilderRoomService.addPathway(testGame, ids[1], ids[2], DirectionEnum.EAST);
        dungeonBuilderRoomService.removeRoom(testGame, ids[1]);

        Assertions.assertEquals(3, testGame.getDungeon().getRooms().size());
        Assertions.assertTrue(testGame.getDungeon().isConnected(ids[1], ids[2]));
        Assertions.assertTrue(testGame.getDungeon().isConnected(ids[0], ids[1]));
    }
}
